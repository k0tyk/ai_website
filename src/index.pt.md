title: Bem-vinde ao A/I
----

Bem-vinde ao A/I
================

A/I (Autistici/Inventati, pronunciado [au’tistiʧi]-[iŋ’vɛntati], ou [iŋvɛn’tati])
nasceu em 2001, de um encontro de ativistas e coletivos do movimento autônomo
anticapitalista com interesse em tecnologia e na luta por direitos digitais.

Nós acreditamos que este muito está longe de ser o melhor mundo possível. 
Por isso, nós oferecemos plataformas para uma comunicação mais livre 
e ferramentas digitais para autodefesa da privacidade para ativistas, grupos e coletivos. 
Entre essas ferramentas estão: 
[e-mail](/services/mail), [blogs](/services/blog), 
[listas de e-mail](/services/lists), [mensageiros instantâneos](/services/chat) e [mais](/services).

Se você quer usar nossos serviços, veja se você concorda com nosso 
[manifesto](/who/manifesto) e comprometa-se em respeitar a nossa
[política](/who/policy).

**[Faça uma conta.](get_service)**

**Siga nosso [blog](https://cavallette.noblogs.org) para novidades e atualizações.**

