title: Solicita un servicio
----

# Solicita un servicio

Algunos de los servicios ofrecidos por A/I, como [IRC](/services/chat "IRC - Chat") y las herramientas de anonimidad, están
destinadas al uso libre por cualquiera en Internet, mientras que otros servicios de carácter “personal” requieren un registro y la creación
de una *cuenta* en A/I.
 En el proceso de registro, no solicitaremos ninguna información identificativa más que un correo electrónico para poder enviarte los datos
de activación (ya que todas las peticiones deben ser aprobadas) – pero esto también puede ayudarte a recordar cuán difícil es no dejar
rastros--- (No registraremos qué correo electrónico corresponde a qué cuenta, pero el proveedor de correo que estés utilizando actualmente
quizás sí lo haga...).

Lo que queremos de nuestros usuarios es que compartan nuestros principios de anti-fascismo, anti-sexismo, anti-racismo, anti-militarismo, y
uso no lucrativo. Por favor, lee nuestra [política](/who/policy) y [manifiesto](/who/manifesto) antes de solicitar tu
cuenta.

Lee tan bien [privacy policy](/who/privacy-policy)

Nos gustaría dejar claro que **tener un servicio alojado en nuestra red no significa que el mismo no tenga un costo**: significa que hemos
elegido un enfoque de auto-gestión y que queremos mantenernos alejados de intereses comerciales y juegos de poder. Más importante aún, es
que esto no sólo significa que puedes utilizar nuestros servicios, sino también que deberías preocuparte por la supervivencia de este sitio:
ya que mantener estos servidores implica más que simplemente sustentarlos y brindarles el seguimiento necesario a diario (lo que hacemos
voluntariamente y sin ganancias económicas de ningún tipo). Te estaríamos más que agradecidos si pudieras hacer [una pequeña
donación](/donate), para ayudarnos a sostener los significativos gastos de alojamiento, conexión, mantenimiento de hardware, etc.

Si deseas solicitar una cuenta y/o servicio, sigue estos simples pasos:

## 1 - Lee detenidamente nuestra [política](/who/policy)

Pedir uno de nuestros servicios implica automáticamente que estás de acuerdo con los principios expresados en la misma.

## 2 - Sigue este link: [https://services.autistici.org/](https://services.autistici.org/)

y rellena el formulario para la activación de un nuevo servicio: recuerda rellenar correctamente todos los campos y no te olvides de dejarnos una cuenta de correo electrónico para que podamos contactarte. También estaríamos encantados de saber cuáles son tus planes respecto a los servicios que estás solicitando. Nos preocupamos por nuestros usuarios y nos gusta pensar que ellos se preocupan por nosotros, a través del intercambio de ideas, planes y proyectos con A/I.

## 3 - Recibirás un correo de confirmación

(o la denegación del servicio, si creemos que tu petición no respeta nuestras políticas): en éste te enviaremos toda la información necesaria y las instrucciones de acceso a tu cuenta y al [panel de usuario](/docs/userpanel) (¡Donde encontrarás todos los servicios conectados a tu cuenta!). El colectivo de A/I hace lo posible para responder a todas las solicitudes, confía en nosotros: tarde o temprano recibirás una respuesta...

