title: Com contribuir amb el projecte
----

Com contribuir amb el projecte
==============================

Considerant que aquest projecte viu **EXCLUSIVAMENT** gràcies a les contribucions, estarem molt feliços amb cada gest i contribució que
puguen fer.
En aquesta pàgina es poden trobar moltes maneres de donar: no sigues tímid\*!.
Si vols saber quant costa mantenir el nostre servei funcionant, [mira aquí!](/who/costs)

- [Transferència Directa a Banca Ètica](#bonifico)
- [Donació on-line](#carta) (és necessària una targeta de crèdit)
- [Lliurament en mà](#life) (a qui coneguis de confiança del col·lectiu)

Detalls
-------

### Transferència Directa a Banca Ètica

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN: IT83P0501802800000016719213
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Agència de Milà
    V. Santa Tecla, 5
    Detall/Raó: Donació per Investici (any 202x)
    
### Donació on-line (PayPal)

<a name="carta"></a>

Pots efectuar la teva donació a través d'una targeta de crèdit. Per a la transacció usem PayPal (no necessites un compte específic), no ens
resulta molt simpàtic, però no hem trobat una millor alternativa fins ara. Si coneixes alguna, per favor fes-nos-la saber.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Subscripcions periòdiques (PayPal)

Açò és una donació periòdica.
Si estàs segur\* de subscriure't i donar al nostre projecte anualment, pots prémer el botó de "subscriu" del següent formulari i tots els
anys 25 euros (o la xifra que decideixis) seràn transferits a les nostres arques :).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>

### Donació on-line (Liberapay)

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Lliurament en mà

<a name="life"></a>

Si vius prop d'algú que forma part del projecte i en qui confies, pots donar-li la teva donació.  
 És important que intentis recol·lectar diferents donacions (de col·lectius, amic\*s o equips) abans de donar-li els diners, així
s'aconsegueix una reducció en el nombre de pagaments i transaccions realitzades.

