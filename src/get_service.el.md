title: Request a service
----

# Αίτημα υπηρεσίας

Θέλετε έναν λογαριασμό/λίστα ηλ. ταχυδρομείου, μία ιστοσελίδα που φιλοξενείται
από την A/I;

Αυτό είναι απολύτως λογικό, αλλά τα κίνητρά μας ξεπερνούν την ιδιωτικότητα και
την ανωνυμία -- Autistici/Inventati είναι μία συλλογικότητα που αναγνωρίζει και
προωθεί τον **αντικαπιταλισμό, αντιρατσισμό, αντιφασισμό, αντισεξισμό, αντιμιλιταρισμό**
και την **άρνηση του αυταρχισμού και της ιεραρχίας**. Εργαζόμαστε εθελοντικά σ' αυτό
το project και το κάνουμε επειδή πιστεύουμε ακράδαντα σ' αυτές τις αρχές. Έτσι λοιπόν,
οι άνθρωποι που θέλουμε να προσφέρουμε αυτές τις υπηρεσίες, είναι άνθρωποι που
μοιράζονται αυτές τις αρχές.
Επομένως, όταν εξηγείτε τον λόγο που θέλετε να κάνετε χρήση των υπηρεσιών στην φόρμα
Αίτησης υπηρεσίας, μην γράφετε για την ιδιωτικότητα ή/και την ανωνυμία - το γνωρίζουμε
ήδη αυτό, αλλιώς δεν θα είχατε προσγειωθεί εδώ..
Πείτε μας, αντ' αυτού, τους λόγους για τους οποίους μοιράζεστε αυτές τις αρχές
και τι είδους άτομο είστε. Δεν χρειάζεται να περιγράψετε λεπτομερώς την ιδιωτική σας
ζωή - πρέπει απλώς να πάρουμε μία ιδέα σχετικά με τον λόγο που σας παρέχουμε υπηρεσία.

Εάν θα θέλατε να ξέρετε περισσότερα για μας, διαβάστε σχετικά με το [μανιφέστο](/who/policy).

Κάθε αίτημα πρόκειται να διαβαστεί από ένα πραγματικό πρόσωπο, όχι από ρομπότ.
Έτσι, εκτιμούμε κάθε πληροφορία που αποφασίζετε να μοιραστείτε μαζί μας κι ελπίζουμε
ότι μπορούμε να δημιουργήσουμε μία σχέση εμπιστοσύνης, ξεκινώντας από το γεγονός
ότι το Αίτημά σας πρόκειται να 'καταστραφεί'.

Άλλα η εμπιστοσύνη είναι αμοιβαία: όπως ακριβώς πρόκειται να πιστέψουμε σ' αυτά
που λέτε χωρίς να ζητάμε τα προσωπικά σας δεδομένα, αν διαπιστώσουμε ότι παραβιάζετε
δημοσίως τις αρχές μας ενώ χρησιμοποιείτε τις υπηρεσίες μας, δεν θα διστάσουμε να
διαγράψουμε τον λογαριασμό σας χωρίς προηγούμενη ειδοποίηση. Εάν θέλετε να χρησιμοποιήσετε
τις υπηρεσίες μας, βεβαιωθείτε ότι βρίσκεστε στο σωστό μέρος.

Λαμβάνουμε επίσης αρκετά αιτήματα από άτομα που θέλουν ν' απαλλαχθούν από τον
'έλεγχο των πολυεθνικών εταιριών και της NSA'. Οι υπηρεσίες μας διαθέτουν χαρακτηριστικά
που **είναι δυνατόν** να επιτευχθεί ένα επίπεδο ιδιωτικότητας ώστε να ικανοποιεί
αυτήν την ανάγκη, αλλά όπως έχει, κανένα πολιτικό πρόβλημα δεν λύνεται απλώς με
μιά τεχνική λύση.
Πρώτα απ' όλα, λάβετε υπόψη, ότι **ο τρόπος με τον οποίο χρησιμοποιείτε τις υπηρεσίες**
**μας, είναι αυτό που πραγματικά μετρά για το απόρρητό σας** - με άλλα λόγια μην
περιμένετε ότι επειδή χρησιμοποιείτε τις υπηρεσίες μας, είναι σαν να πατάτε ένα μαγικό
κουμπί που σας κάνει μη ανιχνεύσιμο. Εάν, για παράδειγμα, χρησιμοποιείτε τον
λογαριασμό σας A/I για να εγγραφείτε σε υπηρεσίες που καταγράφουν την διεύθυνση IP σας,
όπου συνδέεται με τον αριθμό τηλεφώνου σας ή συστηματικά καταγράφει τις κοινωνικές σας
αλληλεπιδράσεις, τότε έχοντας έναν ανώνυμο λογαριασμό ηλ. ταχυδρομείου, δεν θα 
βοηθήσει, ειλικρινά.

**Συνοψίζοντας, εσείς είστε αυτός που είναι υπέυθυνος για την ιδιωτικότητά σας**,
εμείς απλώς φτιάχνουμε αυτήν την διαδικασία τεχνικά δυνατή, αλλά μόνο όταν και αν
ο τρόπος που χρησιμοποιείτε αυτά τα εργαλεία το επιτρέπουν.

Ελπίζουμε να χρησιμοποιήσετε τα τεχνολογικά κι επικοινωνιακά εργαλεία που σας προσφέρουμε
προσεκτικά κι εκτός απ' αυτό, ότι είστε επίσης έτοιμοι να διεκδικήσετε, **στην**
**πραγματική ζωή**, την ελευθερία της έκφρασης και της επικοινωνίας.

Τέλος, θυμηθείτε ότι **οι υπηρεσίες μας είναι ελεύθερες για χρήση αλλά δεν είναι δωρεάν**.
Η διατήρηση και λειτουργία των διακομιστών απαιτεί κάτι περισσότερο από απλή ρουτίνα
για μας (το οποίο συμβαίνει εθελοντικά και χωρίς αμοιβή), επομένως παρακαλούμε να
φροντίσετε αυτό το project στέλνοντας τουλάχιστον μία μικρή δωρεά, ώστε να καλυφθούνε
τα έξοδα.

[Μάθετε πως μπορείτε να συνεισφέρετε στην Autistici/Inventati](/donate "donate").

**[Αίτημα υπηρεσίας](https://services.autistici.org/)**
