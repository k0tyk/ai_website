title: Como contribuir con el proyecto 
----

Como contribuir con el proyecto
================================

Considerando que este proyecto vive **EXCLUSIVAMENTE** gracias a las contribuciones, estaremos muy felices con cada gesto y contribución que
puedan hacer.  En ésta página se pueden encontrar muchas maneras de donar: ¡no seas tímid\*!. Si quieres saber cuanto cuesta mantener
nuestro servicio funcionando, mira[aquí!](/who/costs)

- [Transferencia Directa a Banca Etica](#bonifico)
- [Donación on-line](#carta) (es necesaria una tarjeta de crédito)
- [Entrega en mano](#life) (a quien conozcas de confianza del colectivo)

Detalles
--------

### Transferencia Directa a Banca Etica

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN: IT83P0501802800000016719213 
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Agencia de Milán
    V. Santa Tecla, 5
    Detalle/Razón: Donación para Investici (año 202x)
    
### Donación on-line (PayPal)

<a name="carta"></a>

Puedes efectuar tu donación a través de una tarjeta de crédito. Para
la transacción usamos PayPal (no necesitas una cuenta específica), no
nos resulta muy simpático, pero no hemos encontrado una mejor
alternativa hasta ahora. Si conoces alguna, por favor háznosla saber.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Suscripciones periódicas (PayPal)

Esto es una donación periódica. Si estás segur\* de suscribirte y
donar a nuestro proyecto anualmente, puedes pulsar el botón de
"suscribe" del siguiente formulario y todos los años 50 euros (o la
cifra que decidas) será transferida a nuestras arcas :).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>

### Donación on-line (Liberapay)

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Entrega en mano

<a name="life"></a>

Si vives cerca de alguien que forma parte del proyecto y en quien confías, pueeds darle tu donación.
Es importante que intentes recolectar diferentes donaciones (de colectivos, amig\*s u equipos) antes de darle el
dinero, así se logra una reducción en el número de pagos y transacciones realizadas.
