title: Como contribuir con el proyecto 
----

Como doar para o projeto?
=========================

Nosso projeto vive **EXCLUSIVAMENTE** graças às contribuições das pessoas,
então ficaremos muito felizes e agradecidos por qualquer valor com que
você possa nos ajudar.

Nesta página você poderá encontrar variadas maneiras de doar, não seja
tímid\*! Se você quer saber quanto custa para manter tudo rodando e funcionando,
por favor *dê uma [olhada](/who/costs)!*

Desculpa, mas não aceitamos Bitcoin. *[Este
post](http://cavallette.noblogs.org/2013/07/8333#eng)* explica o motivo.

- [Transferência Bancária para o a Banca Etica](#bonifico)
- [Doação Online](#carta) ((Você precisará de um cartão de crédito)
- [Vida real](#life) (se você conhece e confia em alguém deste projeto)

Detalhes
--------

### Transferência Direta a Banca Etica

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN: IT83P0501802800000016719213 
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Agencia de Milán
    V. Santa Tecla, 5
    Reason/Razão: doação para Investici (ano 202x)
    
### Doação Online (PayPal)

<a name="carta"></a>

Você pode fazer sua doação usando um cartão de crédito. Para a transação
usamos o PayPal (você não precisa criar uma conta). Nós não gostamos
muito do PayPal, mas ainda não encontramos nenhuma alternativa melhor.
Se você tem alguma sugestão, por favor, sinta-se livre em recomendar
para nós.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Doação Recorrente (PayPal)

Essa é uma doação recorrente.
Se você quer ter certeza em assinar para nosso projeto anualmente, você
poderá usar o formulário seguinte.
A cada ano, 25 euros (ou o valor que você quiser) serão creditados em
nossa conta off-shore  (nós estamos brincando sobre isso, claro).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>

### Doação Online (Liberapay)

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Vida Real

<a name="life"></a>

Se você reside próximo à alguém do projeto que você conhece e confia,
você pode dar a ele/ela sua doação.
É importante que você tente coletar diferentes doações (de coletivos,
amigos ou equipes) antes de doar o dinheiro, assim a pobre pessoa que
receber o dinheiro poderá fazer como uma simples transação (ao invés de
múltiplas).
