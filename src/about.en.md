title: Who we are
----

Who we are
==========

A/I was born almost 20 years ago to provide internet support (through website hosting, e-mail accounts, mailing lists, chat, instant
messaging, anonymous remailing, blogs, and much more) to activists and collectives coming from the world of grassroot and social movements.
Our principles are fairly straightforward: the world should not be run on money, but it should be rooted in solidarity, community, mutual
help, equal rights and freedoms, and social justice. Easy, isn't it? We support individuals, collectives, communities, groups and so on
whose political and social activities fit within this worldview and who share with us some fundamental principles: **anti-fascism,
anti-racism, anti-sexism, anti-militarism**. And on top of that, one has to share our basic attitude towards money and the capitalistic
world: a deep feeling of **uneasyness and unrest**.

Our resources live thanks to the donations of the community who uses them. The work the collective does to keep it all running is strictly
**voluntary**. We don't host commercial projects, that we are sure will find more suitable servers than ours. We don't host institutions and
political parties that should be able to run their own servers with the money they have instead of preying a self-managed and voluntary-run
network like ours. Our services are thought and setup to **protect the freedom of speech and the privacy** of our users: that's why we do
not keep sensitive information about them (ie logs, IPs, IDs, ecc.) nor we do any sort of commercial data mining.

Learn more about us and our goals:

* Our [services](/services)
* Our [policy](/who/policy)
* Our [privacy policy](/who/privacy-policy)
* A short [short tale](/who/telltale) about why we are who we are and why we do what we do.
* The [manifesto](/who/manifesto) we wrote in 2002
* Some words about our [history as a collective](/who/collective) (less poetry and more facts)
* [Propaganda, flyers, banners and graphics](/who/propaganda)
* The [R* Plan](/who/rplan/)
* Infrastructure [costs](/who/costs)

Our servers are legally managed by [Associazione AI-ODV](mailto:associazione@ai-odv.org). 
If you encounter technical problems, write to [helpdesk](mailto:help@autistici.org)
other issues should be addressed to our [e-mail contact](mailto:info@autistici.org).
