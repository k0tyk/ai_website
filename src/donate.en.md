title: How to donate to the project 
----

How to donate to the project
=============================

Our project lives on **EXCLUSIVELY** thanks to people's contribution, so we are very happy and grateful for any sum you can help us with.
To avoid any misunderstandings, we want to remind you that a donation does not correspond to a payment, but to an unconditional form of support to our project.
It's neither necessary or sufficient to effect a donation in order to get access to an account on our servers. We don't intend to either sell our services or to allow their commercial use.
Our services are provided exclusively and for free to any person sharing our principles of **anti-fascism, anti-racism, anti-sexism, anti-homophobia, anti-transphobia, and
anti-militarism**.

Said that, on this page you can find a wide range of ways to donate, don't be shy!  If you want to know how much it costs to keep it all up and
running, please [take a look!](/who/costs) Sorry, but we don't accept
Bitcoin. [This post](https://cavallette.noblogs.org/2013/07/8333#eng) explains why.

- [Direct Transfer to Banca Etica](#bonifico)
- [On-line donation](#carta) (you need a credit card)
- [Real life](#life) (if you know and trust someone from the project)

Details
-------

### Direct Transfer

<a name="bonifico"></a>

    Associazione AI ODV
    EU IBAN: IT83P0501802800000016719213
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Milan Agency
    V. Santa Tecla, 5
    Reason: donation to investici (year 202x)
    
### On-line donation (PayPal)

<a name="carta"></a>

You can make your donation with a credit card. For the transaction we use PayPal (you don't need a specific account). We don't like PayPal
too much, but we haven't been able to find a better alternative so far. If you have any suggestions, please feel free to let us know.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Recurrent donation (PayPal)

This is a recurrent donation.  If you want to be sure to subscribe to our project annually, you can use the following form. Every year 50€
(or the sum of your choice) will be credited to our off-shore banking accounts ;) (we are joking of course).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>

### On-line donation (Liberapay)

We can also receive one-off or recurrent donations via Liberapay:

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Real life

<a name="life"></a>

If you live near someone from the project who you know and trust, you can give him/her your donation.
It's important that you try to collect different donations (from collectives, friends or crews) before giving the money in, so that the
poor soul who gets it can make a single transaction.
