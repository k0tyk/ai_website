title:  frequently asked questions about noblogs
----

Frequently Asked Questions
==========================


I can't register on Noblogs.
----------------------------

You have to use an email address registered on our server or other privacy related servers.
Commercial mailbox are not allowed.

Can you add a plugin/template to Noblogs ?
------------------------------------------

No, we are not going to install other plugins/templates at the moment.

How can I delete my blog ?
--------------------------

From the administrative interface, "Tools" -> "Delete Site".
A mail will be sent to your address to confirm
