title:  frequently asked questions about mailboxes
----

Frequently Asked Questions
==========================


What is the capacity of my mailbox?
-----------------------------------

Theoretically it is unlimited: so far, we have trusted our users common sense, and we expect a certain degree of
self-management and responsibility by our users, who should know that our project is totally managed on a
volunteering basis and is only supported by our users donations.

Are there any limits for attachment sending?
--------------------------------------------

Yes: about 10Mb incoming and outgoing.

I've received this error message:
---------------------------------

**4.7.1 <END-OF-MESSAGE>: End-of-data rejected: Recipient limit exceeded. Please check the message and try again.**
what does it mean ?

It means that you sent too many messages, so your account is temporary blocked.
Limits are:

100 recipients in a day

or

10 messages sent in an hour

If you are using your mailbox to send mails to a lot of recipients
maybe you should ask us of a newsletter or a mailing list service.

What mail client do you recommend for windows?
----------------------------------------------

The tools we advice are all licensed as [FOSS](http://www.gnu.org/philosophy/free-sw.html),
usually under a [GNU/GPL license](http://www.gnu.org/licenses/gpl.html)
The mail client we most recommend is [Mozilla Thunderbird](http://www.mozilla.org/products/thunderbird/).

How shall I configure a mail client to receive and send mail?
-------------------------------------------------------------

You can find the connection parameters [here](/docs/mail/connectionparms).
For details about configuration, see out [mail howtos](/docs/mail).

I'm receiving a lot of spam. What's happening?
----------------------------------------------

We do use some anti-spam filters, but they are never exact. Apart from being patient, you could use the spam
filters available with several mail clients, for instance with Thunderbird.
And be sure that you have a filter that move the spam into the spam folder (see [filters](https://www.autistici.org/docs/mail/sieve))

Valid messages are marked as spam, why ?
----------------------------------------

Send us (write to [help@autistici.org](mailto:help@autistici.org) the complete headers of the message,
hidden headers X-Spam-Status and X-Spam-Report
explain why a message is tagged as spam.


How come do I only manage to send messages to inventati.org and/or autistici.org mail addresses?
------------------------------------------------------------------------------------------------

If you're using a mail client, probably you aren't using SSL correctly to connect to our SMTP server,
probably you are using port 25 for smtp server, which is wrong.

Check your client configuration with our [mail howtos](/docs/mail).


I'd like to open a mailbox on A/I's servers. What should I do?
--------------------------------------------------------------

Go to the dedicated web page and [sign in](/get_service).

Will you send me spam and/or marketing messages?
------------------------------------------------

No, we won't. Despite our antispam filters, you'll receive spam anyway.
The only thing we can assure is that we won't give away (either for free or for commercial purposes) your mail address.
But, as usual, we recommend you to be cautious yourself, when giving your address around.

May I use my mailbox for commercial purposes?
---------------------------------------------
If you use your private mailbox for your work, we have no problems (also because we never control the content
of e-mails); but if you want a free mail address for your job and you've bumped into this project by chance,
then our answer is No.

For further information, read our [policy](/who/policy) and our [manifesto](/who/manifesto).

Is my mailbox content safe, if I leave it in the server?
--------------------------------------------------------

No, it isn't. As any other server, also our servers can run into accidents that could undermine its security.

Besides, unlike other servers, our servers are potentially more under control. For instance, the content of your mailbox is
encrypted automatically with a personal key related to your user, that makes your mail unreadable even to us.

We do our best to protect our users' privacy, but this could be insufficient.

We therefore always invite everyone to download their mail and every other data and/or to encrypt their archives.

I can't login in the webmail, I got an error about wrong user or password.
--------------------------------------------------------------------------

Be sure that the username you are using is your complete mail address.

If you forgot your password, try using the recovery password that we recommended you to set up.

If you don't have any of those, there is no way we can help you (we never asked you your name or telephone number so it is
practically impossible for us to know if you are actually the owner of a certain mailbox).

I can login in my panel, but not in the webmail
-----------------------------------------------

Verify that:

* Your password isn't longer than 63 characters (a limit due to the authentication software we use).
* You have logged into your account in the last year (we automatically deactivate accounts that haven't been used for 365 days).
* You have changed your password after the 1st of June 2019.


I can't access my mailbox after I've not used it for some months
----------------------------------------------------------------
Probably it has been deactivated, send a mail to [help@autistici.org](mailto:help@autistici.org)
and ask for re-activation (remember to use a different email address in the request).


All my mails have disappeared !!!!
----------------------------------

Are you sure you have not download them with a client ?

Or maybe your mailbox has been moved to another server, and mail are syncing, [check our blog for news](https://cavallette.noblogs.org)

Anyway, remember that we do not keep backup of your emails, so if your emails are so important, download them with a client.


I can login in the panel but webmail link gives me an error. The password I'm using is correct, since I can read mails using a mail client.
-------------------------------------------------------------------------------------------------------------------------------------------

Probably you are using a password longer than 63 characters, or strange characters.
Choose a shorter password and avoid weird characters.

I've forgotten the password of the mailbox.
-------------------------------------------

We do not keep track of who request a service, so we can't know if you are the legitimate owner of the mailbox.
If you haven't set or don't remember the recovery question we can't help you.

I've forgotten my password but it's still in thunderbird.
---------------------------------------------------------

See here how to recover it [here](https://support.mozilla.org/en-US/questions/1005341).

I've lost my OTP code
---------------------

Try to login with a wrong code, and then click on the "Forgot your password ?" link answering the security
question you should have setup you can change your password and you will login into the user panel without OTP code,
and then you can re-enable 2FA if you want.

If you don't remember the security answer write to [help@autistici.org](mailto:help@autistici.org)

Can I change the address of my mailbox ?
----------------------------------------

No, but you can create an alias in your user panel and set it as default identity in the webmail/client.

Can I switch an alias to make it may main address ?
---------------------------------------------------

No, it's complicated and we can't help you moving messages/addressbook, so ask yourself if you really need this.

I want to close my account.
---------------------------

You can do it from your user panel, click on the "Account Management" button.

How can I set the recovery question and I can I change my password?
-------------------------------------------------------------------

In your user panel click on the "Account Management" button.

How many mails can I send every day ?
-------------------------------------

Actual limits are: 10 mail/hour for a maximum of 100 recipient/day.

Contact us if you need more or request a mailing list/newsletter.
