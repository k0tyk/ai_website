title:  frequently asked questions about supporting the project
----

Frequently Asked Questions
==========================


How can I support your project?
-------------------------------

If you want to support us, read the [Donate](/donate) section in our website.

How can I co-operate with this project?
---------------------------------------

One aspect we constantly need help with is translations. We would like this website and the how-tos and all the technical
documentation to be available in as many languages as possible. So if you master a language and you would like to help,
that's an awesome place to start (even if you only speak English but you are mother tongue).

You can clone our [Website repository](https://0xacab.org/AI/ai_website), and send us some pull requests.


Other than that the A/I collective gladly welcomes any new useful ideas (they don't have to be strictly technical
in order to be useful ;). If you have any proposals, [write to us](mailto:info@autistici.org).

I am skilled in system administration/scripting/coding.
-------------------------------------------------------

The A/I collective doesn't have any particular needs for the administration of its servers, however, we invite you to use
your skills and resources also to create small servers offering services for anonymity. If you want to do that using any of
the pieces of software we created and [made available to the public](https://git.autistici.org/public), you might
find interesting bugs or improvements you could send to us in the form of a Pull Request. This would be a great place to start.

Other than that we appreciate any software released under GPL and useful for the safety of the data contained in our servers.

I am skilled in design, communication and media strategies.
-----------------------------------------------------------

We appreciate any help for explicitating our intentions: from graphics to concept presentation,
from accessibility to mediascape impact, these are domains in which we have never been very good!

I'd like to organize a benefit event/party/meeting for this project. May I? What should I do?
---------------------------------------------------------------------------------------------

Sure! If you contact us, we'll appreciate any benefit event for our project. Of course, events mustn't be
commercial and must respect the principles in our [Manifesto](/who/manifesto). Other than that, we think that organizing
an event to support us is such a flattering compliment to us, our work, and really gives us good vibes to continue on.

I have a lot of unused bandwidth. Can this be helpful?
------------------------------------------------------

Yes, it can. Connectivity is very important, and unused bandwidth should always be used.
If you can and know how to do it ( [contact us](mailto:info@autistici.org) if you need help), you can install a server and offer any service you wish.

I know/own/work in a place which could house a project. Are you interested?
---------------------------------------------------------------------------
Sure :) [Contact us](mailto:info@autistici.org), and we'll think about it.


Can I link you in my site?
--------------------------
Sure. Especially if the description of the link is not insulting ;)

Who supports your project? Do you have any sponsors?
----------------------------------------------------

We don't have proper sponsors or supporters. To support our project, we annually ask our users for a donation,
and with their help we sometimes organize some benefit events.

How much do you pay for the services you offer?
-----------------------------------------------

Our services don't have a fixed price. Of course, since we have to provide for our servers infrastructure and
connection, we do need money, about 15000E/year.

We generally launch annual fundraising campaigns, asking for donations according to
each user's possibilities (hoping that groups and collectives manage to donate more).

For instance, 10 euros will be sufficient for mailboxes, 20 for mailing lists and if a group using a website
reaches 50 euros, it would be wonderful.

However, even if you donate few euros, we'll be very grateful! If you want to know more, read our page about
[donations](/donate).

Are donations tax-deductible?
-----------------------------

If you file taxes in Italy, you can donate to Autistici/Inventati via the so-called “5 per mille”, an option
to donate 0.5% of your taxes to a non-profit organization.

To do so, on your “Modello CU Unico” and Form 730, in the section regarding “5 per mille”, you will have to
sign your name ad insert the tax code (codice fiscale) “93090910501” where it says:

"SOSTEGNO DEL VOLONTARIATO E DELLE ALTRE ORGANIZZAZIONI NON LUCRATIVE DI UTILITA’ SOCIALE, DELLE ASSOCIAZIONI
DI PROMOZIONE SOCIALE E DELLE ASSOCIAZIONI E FONDAZIONI RICONOSCIUTE CHE OPERANO NEI SETTORI DI CUI ALL’ART. 10,
C. 1, LETT A), DEL D.LGS. N. 460 DEL 1997"


Do members (admins, designers, coders, lamers) earn money and/or get repayments for activities carried out within the project?
------------------------------------------------------------------------------------------------------------------------------

No: whoever co-operates with this project is a volunteer. We never receive payments/wages/repayments for the
time we spend for this project.

I'm totally broke, can I have a mailbox in your servers anyway?
---------------------------------------------------------------

Sure you can; but actually, considering that we never have enough money, you could perhaps give us 5 euros,
couldn't you? You can't? Alright: we hope we can survive anyway.

I can afford a small/large donation. What should I do?
------------------------------------------------------

Before you change your mind, since generosity is very flattering, read [donations](/donate) page now!

Are donations anonymous?
------------------------

It depends on the payment modality you choose: if you give us the money personally, or hide it in a CD you send
by mail, they are.

If you choose to pay by draft or by credit card, your bank will keep track of this transaction.

However, we don't keep a record, but in our hearts ;), of the people who contribute money to our project.
Unfortunately, we don't know yet how to receive online donations anonymously: can you suggest one?

Do you accept bitcoin ?
-----------------------

No, here we have explained why: [https://cavallette.noblogs.org/2013/07/8333](https://cavallette.noblogs.org/2013/07/8333)


Is it possible to find an agreement for a greater visibility of your web pages?
-------------------------------------------------------------------------------

If you are talking about exchanging a link or a banner with money, our answer is obviously **No**
We never accept advertising banners/links, from anyone. We accept donations from anybody, provided there's no
condition, wether explicit or implicit.
