title: Howto per il pannello utente
----

Howto per il pannello utente
============================

** ATTENZIONE - QUESTA PAGINA E' OBSOLETA, LE IMMAGINI NON CORRISPONDONO AL PANNELLO ATTUALE, STIAMO LAVORANDO PER AGGIORNARLA **

Per usare uno qualsiasi dei [servizi](/services/) che A/I offre dovete essere titolari di un utente presso i server di A/I e quindi
essere in grado di accedere al vostro **Pannello Utente** nel quale trovare tutte le informazioni sui vostri servizi attivi e su come
interagirvi.

Questo manuale vorrebbe essere una guida dettagliata a tutto quello che potete fare accedendo al vostro **Pannello Utente**

<a name="topsummary"></a>

Indice
------

- [Accesso](#login)
- [Cosa fare dopo il primo accesso](#firstlogin)
  1.  [Impostare la domanda per il recupero della propria password](#recoverquestion)
  2.  [Cambiare la propria password](#passwordchange)
- [Pagina principale del Pannello Utente](#main)
- [Come cambiare la lingua del proprio Pannello Utente](#languagechange)
- [Come leggere la propria mail e come cambiare le sue impostazioni dal Pannello Utente](#mailbox)
  1.  [Come aggiungere o cancellare un alias](#alias)
  2.  [Come scaricare la propria rubrica](#addressbook)
- [Come gestire un sito ospitato su A/I dal Pannello Utente](#website)
- [Come gestire il proprio account WebDAV dal Pannello Utente (e come cambiare la propria password WebDAV)](#webdav)
- [Come gestire il proprio database mysql dal Pannello Utente](#mysql)
- [Come gestire le proprie mailing list dal Pannello Utente](#mailinglist)
- [Come gestire i propri blog su Noblogs.org dal Pannello Utente](#blog)
- [Come chiudere il proprio account](#close)

<a name="login"></a>

Accesso
-------

Come è facilmente intuibile dall'immagine in basso per accedere al vostro **Pannello Utente** dovete cliccare sul grosso bottone rosso che
c'è in [homepage del sito di A/I](/), inserire il vostro utente (l'indirizzo mail completo!) e la vostra password e poi cliccare su *login*.

![](/static/img/man_userpanel/it/login_page01.png "Pagina di login del Pannello Utente")

Se il bottone rosso non funzioona potete accedere direttamente alla [pagina di login](https://accounts.autistici.org) del vostro **Pannello Utente** e
inserire il vostro utente (l'indirizzo mail completo!) e la vostra password. Poi cliccate su *login*.

![](/static/img/man_userpanel/it/login_page02.png "Pagina di login del Pannello Utente")

<a name="firstlogin"></a>

### Cosa fare dopo il primo accesso

<a name="recoverquestion"></a>

#### Impostare la domanda per il recupero della propria password

Se è la prima volta che accedete al vostro **Pannello Utente** ci sono alcune operazioni che dovete compiere prima di dimenticarvene!

Come potete notare dalla barra rosso scuro in cima al vostro **Pannello Utente** la prima cosa che dovete fare è impostare una domanda di
ripristino: se vi dimenticaste la vostra password questo sarebbe conoscere tale domanda e la sua risposta diventerebbe l'unico modo per
accedere nuovamente al vostro utente e alla vostra casella di posta.

![](/static/img/man_userpanel/it/first_login_set_recover_question.png "Avviso della necessità di impostare la domanda di ripristino")

Una volta che abbiate cliccato sul link nella barra rosso scuro verrete indirizzati alla nostra pagina dei *Gatti* (dato che di solito le
domande più gettonate sono quelle sui propri animali abbiamo pensato di dedicargli tutto il sistema di ripristino delle password
dimenticate). Da tale pagina potrete decidere la vostra domanda di ripristino e la sua risposta.

![](/static/img/man_userpanel/it/gatti01.png "Form per l'impostazione della domanda di ripristino, anche noto come Gatto")

Inserite una domanda e la relativa risposta, poi cliccate sul bottone *Salva*.

![](/static/img/man_userpanel/it/gatti02.png "Form per l'impostazione della domanda di ripristino, anche noto come Gatto")

**Attenzione**: dovete essere sicuri di ricordarvi l'esatta risposta alla domanda che avete inserito (incluse lettere maiuscole, simboli,
numeri e quant'altro) dato che fornirla sarà l'unico modo per recuperare la vostra password in caso di smarrimento.

Inoltre vogliamo ricordarvi che A/I **non conosce la risposta alla domanda che avete inserito** dato che essa viene conservata nei nostri
database in forma crittata. Siete e sarete l'unica persona a sapere come rispondere alla domanda e quindi ad essere in grado di recuperare
la password che vi siete dimenticati.

Una volta che avrete cliccato sul bottone *Salva* verrete riportati al vostro **Pannello Utente** dove un nuovo avviso nella barra rosso
scuro vi informerà che avete impostato la vostra domanda di ripristino correttamente. Adesso possiamo procedere al prossimo passo.

![](/static/img/man_userpanel/it/first_login_set_recover_question_done.png "Avete impostato correttamente la vostra domanda di ripristino per la password")

<a name="passwordchange"></a>

#### Cambiare la password

La seconda cosa che dovete **tassativamente** fare prima di procedere a
scoprire la vostra mail e gli altri servizi offerti da A/I è cambiare la
vostra password, soprattutto se ve la siete fatta spedire a una casella
di posta.

Per cambiare la password, cliccate sul vostro nome utente in alto a
destra e si aprirà un menu a tendina con l'opzione del cambio password.

![](/static/img/man_userpanel/it/pannello_cambio_password_it.png)

**Notate** che nello stesso menu a tendina potete anche trovare il link per cambiare la vostra
[domanda di recupero della password (se non lo avete ancora fatto)](#recoverquestion) e per impostare l'[autenticazione a due fattori](../2FA).

Una volta che avrete cliccato sul link *Cambia password* verrete indirizzati a una form dove potrete inserire **due volte** la vostra nuova
password. Dopodiché cliccate sul bottone *Salva* e avrete una nuova password per accedere al vostro **Pannello Utente** e **alla vostra
casella di posta**.

![](/static/img/man_userpanel/it/change_password01.png "Pagina per cambiare la vostra password")

![](/static/img/man_userpanel/it/change_password02.png "Inserite due volte la nuova password nella form e cliccate su Salva")

**Attenzione**: in questo passaggio state cambiando la password del vostro utente e della vostra casella di posta. Se volete cambiare la
vostra password WebDAV o la password per accedere al database MySQL dovrete riferirvi ad altre [parti di questo manuale](#topsummary).

Una volta che avrete cliccato sul bottone *Salva* verrete riportati automaticamente al vostro **Pannello Utente** dove un nuovo avviso nella
barra rosso scuro vi comunicherà l'avvenuto cambiamento di password. Ora siete pronti a familiarizzare con il **Pannello Utente**. Andiamo.

![](/static/img/man_userpanel/it/userpanel_main_password_changed.png "Password cambiata con successo")

<a name="main"></a>

Pagina principale del Pannello Utente
-------------------------------------

![](/static/img/man_userpanel/it/userpanel_main.png "Pagina principale del Pannello Utente")

Quello che vedete nell'immagine soprastante è una versione molto base di come potrebbe apparire il vostro **Pannello Utente**. E' il
**Pannello Utente** di un utente che ha solo una casella di posta sui server di A/I. Non ha indirizzi di posta aggiuntivi, né liste, né
blog, né newsletter, niente di niente. Più o meno come il 50% dei nostri utenti, quindi non c'è niente di cui vergognarsi :)

Ora vediamo di dargli un po' di colore e di capire come diavolo è fatto il **Pannello Utente**

![](/static/img/man_userpanel/it/userpanel_main_boxes.png "Le sezioni del Pannello Utente")

Potete immaginare il **Pannello Utente** diviso in diverse sezioni: alcune sono molto affollate ma i colori dovrebbero aiutarvi a
orientarvi.

-   I **rettangoli blu** sono le sezioni dove potete trovare notizie, informazioni e comunicazioni: nella colonna laterale destra ci sono
    gli ultimi articoli del [blog del collettivo di A/I](http://cavallette.noblogs.org), ovvero dove potreste trovare notizie che noi
    riteniamo importanti da comunicare al pubblico, inclusi problemi hardware o software sui server; nella parte inferiore della pagina del
    pannello potete trovare una sezione di news divisa in tre parti: gli ultimi articoli riguardo [noblogs.org](http://noblogs.org) e gli
    ultimi post della comunità di [noblogs.org](http://noblogs.org).
-   Sotto i rettangoli blu in basso trovate un singolo **rettangolo giallo**: in esso trovate il menù per cambiare la lingua con cui
    contattarvi e per la visualizzazione del **Pannello Utente**. Se volete sapere come fare a cambiarla, leggete [sotto](#languagechange).
-   Nella parte alta della pagina trovate un **rettangolo verde** che mostra dove trovare aiuto per risolvere problemi con i servizi di A/I:
    documentazione, manuali, e tutto quanto abbiamo messo a disposizione per voi. Le stesse risorse potete trovarle nella nostra
    [pagina di supporto](/get_help).
-   Appena sotto il rettangolo verde trovate un singolo **rettangolo viola** che vi indicherà il link per richiedere ulteriori servizi ad
    A/I: se avete già una casella di posta sui server di A/I e volete aprire una lista o ospitare un sito, quello è il link che dovrete
    seguire per farlo. Ovviamente prima di farlo assicuratevi di dare una rilettura la nostra [policy](/who/policy) e il nostro
    [manifesto](/who/manifesto), solo per essere sicuri che quello di sapere quello che state facendo. Siete sicuri? Bene, allora
    potete [cliccare sul link](https://www.autistici.org/services/).
-   Ultima ma non meno importante trovate la zona centrale del **Pannello Utente** con il suo grande **rettangolo rosso**: è la zona dove
    troverete le informazioni principali per accedere e configurare i vostri servizi su A/I. La maggior parte di quello che trovate in
    questo manuale riguarda questa sezione del pannello. Vorremmo però farvi notare anche un piccolo ma rilevante **rettangolo rosso** in
    alto a destra: è il punto in cui potete sapere con quale utente vi siete collegati ai nostri server e di fianco trovate il link da cui
    potete effettuare il **logout** dal **Pannello Utente**. Se non siete passati a leggere la vostra posta, prima di uscire ricordatevi di
    cliccare su quel link prima di chiudere il browser.

<a name="languagechange"></a>

Come cambiare la lingua del proprio Pannello Utente (rettangolo giallo)
-----------------------------------------------------------------------

Se volete cambiare la lingua in cui visualizzate il vostro **Pannello Utente** non dovrete far altro che selezionare la lingua che
desiderate nel menù a tendina e cliccare sul bottone *Update* (nella zona cerchiata in giallo nell'immagine sottostante). Una volta cliccato
il bottone la pagina verrà ricaricata tradotta nella lingua selezionata e un avviso nella barra rosso scuro in alto vi informerà
dell'avvenuto cambiamento di lingua del vostro **Pannello Utente**.

![](/static/img/man_userpanel/it/language.png "cambiate la lingua in cui il pannello viene visualizzato")

<a name="mailbox"></a>

Come leggere la propria mail e come cambiare le sue impostazioni dal Pannello Utente
------------------------------------------------------------------------------------

Abbiamo già spiegato come [cambiare la password della vostra mail](#passwordchange) e come
[impostare la vostra domanda per il recupero della password in caso di smarrimento](#recoverquestion). Ora potete finalmente collegarvi alla
vostra casella di posta e leggere un po' di e-mail: cliccate sulla *icona a forma di busta delle lettere* o sul vostro
*indirizzo di posta elettroncia* nella zona principale del
**Pannello Utente**, e verrete indirizzati automaticamente alla nostra webmail. Se volete sapere come usarla, potete leggere il [nostro
bellissimo manuale](/docs/mail/roundcube). Ricordatevi che una volta che avrete fatto logout dalla webmail, sarete anche stati
scollegati dal vostro **Pannello Utente** automaticamente.

![](/static/img/man_userpanel/it/mailbox_read.png "Cliccate per leggere la vostra mail!")

Se volete sapere quanto spazio state usando sui nostri server, leggete la parte inferiore dell'area in cui accedete alla vostra casella di
posta: vorremmo ricordarvi che non esiste un limite allo spazio disco che potete usare sui server di A/I, ma ricordatevi sempre che le
nostre risorse non sono infinite e che le condividete con migliaia di altre persone. Prestate attenzione a quanto spazio utilizzate.

![](/static/img/man_userpanel/it/mailbox_space.png "Guardate quanto spazio occupate sui nostri server")

<a name="alias"></a>

### Come aggiungere o cancellare un alias

Prima di tutto: che diavolo è un **alias**?

Un **alias** è un indirizzo di posta elettronica alternativo su cui volete ricevere i vostri messaggi: in pratica chi invierà un messaggio
al vostro indirizzo o al vostro **alias** vi raggiungerà senza problemi. Inoltre tale indirizzo può essere usato come mittente dei vostri
messaggi di posta elettronica se configurate il vostro client di posta adeguatamente. In ogni caso il vostro **alias** sarà collegato
inscindibilmente al vostro indirizzo di posta.

Sui server di A/I potete gestire in autonomia al massimo 5 alias per ogni casella di posta che avete richiesto. Se avete bisogno di più di 5
alias di posta, [contattateci](/get_help) e vedremo cosa possiamo fare.

Se volete creare un **alias** per la vostra casella di posta cliccate sul link di fianco al vostro indirizzo e-mail nella zona principale
del **Pannello Utente**

![](/static/img/man_userpanel/it/mailbox_createalias.png "Cliccate per creare un alias")

Una volta che avrete cliccato verrete indirizzati a una form dove potrete inserire l'indirizzo alternativo che volete attivare. Cliccate poi
sul bottone *Crea* e avrete il vostro nuovo indirizzo di posta elettronica.

![](/static/img/man_userpanel/it/mailbox_createaliasform.png "Cliccate per creare un alias")

Ora la zona principale del **Pannello Utente** è un po' cambiata e potete trovare il vostro **alias** di fianco al vostro indirizzo di
posta. Un avviso in una barra rosso scuro in alto vi conferma l'avvenuta creazione del nuovo **alias** per la vostra mailbox.

![](/static/img/man_userpanel/it/mailbox_aliascreated.png "Avete creato il vostro primo alias!")

Se avete già un alias e volete rimuoverlo, dovete semplicemente cliccare sul segno *meno* di fianco all'indirizzo che volete cancellare.
 **Attenzione**: non vi verrà chiesta alcuna conferma prima di cancellare il vostro alias. Se scoprite di aver cancellato l'**alias**
sbagliato, non andate in panico: con un click potrete crearlo di nuovo e tutto tornerà come prima. L'unico ricordo del vostro errore sarà la
barra rosso scuro in cima alla pagina che ormai conoscete fin troppo bene dalle precedenti immagini! :)

![](/static/img/man_userpanel/it/mailbox_deletealias.png "Cliccate per cancellare un vostro alias")

<a name="addressbook"></a>

### Come scaricare la propria rubrica

Se avete usato molto la vostra webmail, avrete anche collezionato numerosi indirizzi di posta dei vostri contatti. Se volete farne una copia
di scorta o se volete passare a un client di posta per scaricare la vostra posta (cosa che consigliamo vivamente di fare!), cliccando sul
link cerchiato in rosso nell'immagine (a fianco del vostro indirizzo di posta) potrete scaricare una copia della vostra rubrica per poi
importarla dove meglio credete.

![](/static/img/man_userpanel/it/mailbox_addressbook.png "Scaricate la vostra rubrica")

<a name="website"></a>

Come gestire un sito ospitato su A/I dal Pannello Utente
--------------------------------------------------------

Se avete richiesto ad A/I qualcosa di più di una semplice casella di posta elettronica il vostro **Pannello Utente** sarà un po' più
complicato di quanto visto finora.
 Se gestite anche un sito web ospitato su A/I la vostra area principale del **Pannello Utente** avrà una nuova sezione.

![](/static/img/man_userpanel/it/userpanel_main2.png "Gestite il vostro sito dal Pannello Utente")

Se cliccate sulla freccia a fianco della parola *Siti Web* o *Indirizzi Email* potete attivare o disattivare le relative sezioni del
**Pannello Utente**. Siccome abbiamo già spiegato cosa accade nella sezione dedicata alle [mailbox](#mailbox), passiamo alla sezione
dedicata ai siti ospitati su A/I.

![](/static/img/man_userpanel/it/userpanel_main3.png "Gestite il vostro sito dal Pannello Utente")

Come potete osservare la gestione di un sito ospitato su A/I prevede tre sottosezioni: una dedicata al **vostro sito** in senso proprio; una
dedicata all'**account WebDAV** usato per caricarvi i materiali; e un'altra ancora relativa alla gestione dell'eventuale **database**
connesso al sito. Ognuna di queste sottosezioni vi offre la possibilità di configurare cose diverse.

![](/static/img/man_userpanel/it/website_submenus.png "Quante cose bisogna configurare per poter ospitare un sito funzionante!")

Se cliccate sul *nome del sito* o sull'*icona a forma di pianeta* verrete rediretti sul vostro sito. A destra del *nome del sito* potete
vedere quattro link:

-   *Stats* vi indirizza a una pagina dove potete trovare il codice necessario ad attivare le statistiche fornite direttamente da A/I. Leggi
    il [manuale sull'uso delle statistiche nei siti web ospitati da A/I](/docs/web/webstats) per maggiori informazioni.
-   *Description* vi permetterà di inserire una generica descrizione del sito.
-   *Options* vi permetterà di scegliere se il vostro sito supporta il codice php, python o solo pagine in semplice html,
    e se il vostro sito necessita delle funzioni di invio mail di php.
-   *Help* vi indirizzerà alla pagina di A/I con i [manuali per i siti ospitati su A/I](/services/website#howto)

![](/static/img/man_userpanel/it/website_management_options.png)

![](/static/img/man_userpanel/it/website_stats.png "Copiate e incollate il codice indicato per attivare le statistiche sul vostro sito")

![](/static/img/man_userpanel/it/website_description.png "Inserite una descrizione e una categoria per il vostro sito, poi cliccate sul bottone
'Salva'")

![](/static/img/man_userpanel/it/website_options.png "Personalizzate le caratteristiche del vostro sito")

<a name="webdav"></a>

Come gestire il proprio account WebDAV dal Pannello Utente (e come cambiare la propria password WebDAV)
-------------------------------------------------------------------------------------------------------

Per aggiornare il vostro sito avrete bisogno di un utente e una password (diverse da quelle che usate per collegarvi al **Pannello Utente**
e alla vostra casella di posta).

Utilizzando tali nuove password tramite un client WebDAV come descritto nel nostro [manuale per WebDAV](/docs/web/webdav)
potrete accedere e aggiornare il vostro sito web.

![](/static/img/man_userpanel/it/ftp.png "Gestite il vostro account WebDAV")

Di fianco alla sottosezione **account WebDAV** troverete due utilissimi link:

-   Il primo vi indica quale URL dovrete usare per collegarvi con il vostro client WebDAV per aggiornare il vostro sito.
-   Il secondo link vi permetterà di cambiare la vostra password WebDAV. Cliccate sul link, inserite la vostra nuova password (**due
    volte**) e poi cliccate sul bottone *Salva*. Una volta compiuta questa operazione verrete riportati automaticamente al pannello con il
    consueto avviso nella barra rosso scuro in cima alla pagina circa il cambiamento della password WebDAV.

![](/static/img/man_userpanel/it/ftp_passwordchange.png "Cambiate la vostra password WebDAV")

<a name="mysql"></a>

Come gestire il proprio database mysql dal Pannello Utente
----------------------------------------------------------

![](/static/img/man_userpanel/it/mysql.png "Gestite il vostro database MySQL")

Se avete richiesto un database MySQL da usare con il vostro sito, questa sottosezione dell'area principale del **Pannello Utente** vi
consentirà di:

-   Resettare la vostra password per i database cliccando sul link *Resetta password*

<a name="mailinglist"></a>

Come gestire le proprie mailing list dal Pannello Utente
--------------------------------------------------------

Se siete amministratore di una o più liste, le troverete nel vostro **Pannello Utente**.

![](/static/img/man_userpanel/it/userpanel_main4.png "Gestite le vostre liste")

Cliccando sul link con *il nome della lista* o l'*icona a forma di busta da lettera* verrete indirizzati verso l'interfaccia web di
amministrazione della lista. Se la vostra lista è pubblica avrete anche il link agli archivi pubblici della lista proprio di fianco al suo
indirizzo mail.

Per cambiare la password di amministrazione della lista cliccate sul link *Reset admin password*

<a name="blog"></a>

Come gestire i propri blog su Noblogs.org dal Pannello Utente
-------------------------------------------------------------

Se vi siete iscritti a [Noblogs.org](http://noblogs.org) utilizzando una mailbox di A/I allora i blog a cui partecipate vi saranno indicati
nel **Pannello utente**.

![](/static/img/man_userpanel/it/userpanel_main5.png "Gestite i vostri blog")

Cliccando su *Nome del Blog* o sull'*icona a forma di pianeta* verrete indirizzati al vostro blog. Se cliccate sul link *admin* esso vi
porterà alla [pagina di amministrazione del vostro blog.](http://noblogs.org/wp-admin/).

<a name="close"></a>

Come chiudere il proprio account mail dal Pannello Utente
-------------------------------------------------------------

![](/static/img/man_userpanel/it/close.png "Chiudere il proprio account")

Cliccando sul link *Close accont* e confermando, la vostra mailbox verra' disabilitata.

Questo significa che:

smetterà istantaneamente di ricevere e inviare posta (anche tramite eventuali alias)

nel giro di un paio di giorni tutti i messaggi presenti nella mailbox e non scaricati
saranno cancellati, in modo irreversibile.

la rubrica (se utilizzata con la webmail) restera' conservata nei nostri backup.

la mailbox e gli alias resteranno comunque presenti nel nostro sistema di gestione degli utenti
in modo che nessun altro possa richiedere un account con lo stesso indirizzo e ricevere in futuro messaggi
che erano indirizzati a voi.

I dati che saranno conservati sono:

* l'indirizzo email principale e i suoi alias
* la data di creazione dell'account di posta
* la domanda (non criptata) e la risposta di recupero (criptata)
* la password della mailbox (criptata)
* i codici OTP, se impostati (criptati)

Se invece volete una rimozione completa inserite un ticket nel nostro sistema di
[helpdesk](https://helpdesk.autistici.org)

