title: Newsletter Howto
----

Newsletter Howto
==================

What is a newsletter
-------------------------------------------------------------

A **newsletter** is essentially a list of mail addresses to which mails are sent automatically by a software installed on the server.

Unlike a mailing list, newsletters are set as a one way communication,
essentially they work as a **closed** mailing list.

To participate to a list and receive messages sent to it you need to subscribe.

To subscribe, you have to know the newsletter name and visit the page **https://noise.autistici.org/mailman/listinfo/NAME**
fill in the form and you should receive a confirmation message.

Enjoy!

Howto manage a newsletter
-----------------------------

When you ask for a mailing list on our servers (managed by the software "Mailman"),
the first thing you'll have to do is log into the newsletter administration panel and configure its various options:
we have set the default options according to how *we think* people will want to use a newsletter, but our
ideas could be **very wrong** about your needs and wishes.

Some of the settings are forced:

    send_reminders = 0
    respond_to_post_requests = 0
    default_member_moderation = 1
    generic_nonmember_action = 3

if you change them, they will be reset in a few minutes.

So log into your **https://noise.autistici.org/mailman/admin/NAME** page and start working on it.

### General Options

In the *General Options* page you will be able to set your newsletter welcome page, its description, the welcome message for people subscribing to
it and many more basic stuff, the maximum allowed message size and so on.

Most of the default options will be fine, but you should know that you shall **never change the *host\_name* option** since it would break
your newsletter.


### Password

In the *Password* page you will be able to change and reset your admin password.


### Membership Management

The *Membership management* page will easily be one of the most visited page in your newsletter administrator experience.
 In the *Membership List* subpage you'll find a list of all the mail subscribed to the newsletter along with a series of option for each one of
them:

- *mod* refers to moderated users, ie users whose message will need administrator approval before being forwarded to the list
		All users should be moderated, with the exception of the user(s) authorized to send a newsletter.
- *nomail* refers to users who are not receiving the list messages for several reasons (their mailbox could be full or disabled).
- *digest* and *plain* refer to the way these users receive the list messages: *plain* means they will get each mail individually, while
    *digest* allows them to receive a daily/weekly/monthly summary of the newsletter messages in a single mail

If you want to unsubscribe a single user, just click on the *unsub* box next to his/her mail and then on *"Submit your changes"*.

To subscribe multiple users simultaneously, go to the *Mass subscription* subpage and fill in the form with one mail per line. You will also
be able to write a short welcome message to the ones you are subscribing.

**ATTENTION**:  do not subscribe hundreds of email addresses without their explicit consent.  In the mass subscription page you can choose "invite" instead of
"subscribe" in the checkbox so that users will receive an invitation messages with a link to click to complete the subscription.


### Privacy Options

The *Privacy Options* page is **one of the most important part** of your administration panel. Let's start with the *Subscription rules*.

- *Advertised* is the option to point out wether you want your list to be known to the public or not. The default value is **no**.
- *Subscribe Policy* specifies what a user should do to subscribe to a list: reply to an automatic mail sent by the mailing list software,
    wait for administrator approval or both. The default value is to require both steps to subscribe to the list.
- The *Ban list* is a list of email who are forbidden subscription to the list
- The last interesting option allows you to choose who can see the list of people subscribed to the list. The default option allows only
    list subscribers to see who are the other people subscribed, but it's possible to extend this possibility to anyone or to shrink it to
    the administrator only.

The *Sender filters* is set so that messages coming from non-subscribers are *discarded* by default, without any notification.
If you want to authorize someone that is not subscribed to the newsletter to write messages,
search for the option "List of non-member addrresses whose postings should be automatically accepted" and add the
address you want to authorize.

### Archiving Options

The *Archiving Options* page is **very important** since it includes the options to decide wether a list messages will be **public** or
**private**. Please be careful with this page options since setting up a **public list** implies that your subscribers messages will be
available on search engines freely!

- The *Archive* option allows the administrator to decide wether to keep a copy of the list messages on the servers or not. The default
  option is **no** so if you do not change this the only copy of the list mails will be in your subscribers computers.
- The *Archive private* option allows the administrator to decide wether the list archive will be publicly available on [our servers](http://lists.autistici.org/)
  or only available to subscribers on a dedicated page (whose link you will fine in your administration panel)


For details, see the Mailman manual:

**https://www.gnu.org/software/mailman/mailman-admin/index.html**
