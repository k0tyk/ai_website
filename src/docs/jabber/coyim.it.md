title: Come configurare CoyIM
----

Come configurare CoyIM
======================


CoyIM è un programma multipiattaforma (Windows, Mac e Linux) per chat orientato
a garantire la sicurezza dell'utente. Obiettivo dichiarato delle persone che lo
sviluppano è quello di  _ridurre al minimo la superficie di attacco al vostro
sistema_.  Per questo motivo CoyIM **supporta un solo protocollo di
comunicazione** (XMPP, lo stesso che utilizziamo per i nostri servizi di instant
messaging), non prevede l'installazione di plugin di terze parti e ha
un'**interfaccia grafica spartana**, tanto da non contenere neppure delle
emoticon. I componenti embeddati nel client sono ridotti al minimo e
rappresentano lo standard riconosciuto in fatto di comunicazione sicura on-line.

Tra questi troviamo:

*   **Tor**. CoyIM rileva automaticamente se Tor è installato sul computer
    dell'utente: in questo caso, indirizza in maniera automatica le sue
    connessioni verso la rete a cipolla, rendendole così anonime.
*   **OTR**. Ogni messaggio inviato con CoyIM è cifrato automaticamente con
    OTR. Inoltre CoyIM non permette l'invio di messaggi in chiaro ad altri
    client XMPP.
*   **TLS**. Il canale di comunicazione tra CoyIM e il server di chat viene a
    sua volta cifrato con un ulteriore strato di cifratura.

Altro aspetto importante di CoyIM sono le sue impostazioni di default che
garantiscono sin dal primo avvio un livello ottimale di protezione per l'utente.
Infine, CoyIM implementa un tool per **importare con facilità la vostra chiave
privata OTR** da altri client XMPP (come Pidgin, Adium o Jitsi). In questo breve
tutorial vedremo come configurarlo per i servizi di chat di Autistici/Inventati.

## Cifrare la configurazione di CoyIM ##

La prima volta che lancerete CoyIM vi apparirà questa finestra:

![](/static/img/man_jabber/coyim/coyim_1.jpg)

CoyIM vi dà la possibilità di salvare il file di configurazione del vostro
client in modalità cifrata. In questo modo, qualora qualcuno avesse accesso in
maniera indebita al vostro computer, non sarà in grado di conoscere né il
vostro nome utente, né la vostra password di login, né il server di chat che
utilizzate. Se decidete di abilitare questa opzione cliccate su _"Yes"_. 

![](/static/img/man_jabber/coyim/coyim_1_password.jpg)

Scegliete una password per cifrare la configurazione di CoyIM e premete _"Ok"_.
Il vostro file di configurazione di CoyIM ora è cifrato. La password per
decifrarlo vi verrà chiesta ogni volta che avvierete il programma. Nel caso la
perdiate dovrete nuovamente configurare CoyIM.

## Configurazione di base ##

Ora configuriamo CoyIM per poterlo utilizzare con i servizi di chat di
Autistici/Inventati. 

![](/static/img/man_jabber/coyim/coyim_2.jpg)

Cliccate su _"Existing Account"_.

![](/static/img/man_jabber/coyim/coyim_3.jpg)

Ora inserite:

*   Il vostro nome utente (i.e. nomeutente@autistici.org) alla voce _"Your
    account"_

*   La password del vostro account alla voce "Password" (o una password
    specifica per Jabber se avete attivato la [Two Factor
    Autenthication](/docs/2FA) sui nostri
    servizi),

*   Il nickname che i vostri contatti visualizzeranno alla voce _"Display Name"_

Infine cliccate su _"Save"_. Se la configurazione è andata a buon fine vi
apparirà questa finestra:

![](/static/img/man_jabber/coyim/coyim_4.jpg)

## Configurazione con Hidden Service ##

Se volete garantirvi un livello ulteriore di privacy e sicurezza potete accedere
a Jabber utilizzando il nostro [hidden service Tor](/docs/anon/tor). Per farlo
dovete applicare alcune semplice modifiche alla configurazione di base che
abbiamo appena visto. 

Una volta che avete inserito il vostro username, la vostra password e il vostro
nickname, spuntate la casella _"Display all settings"_

![](/static/img/man_jabber/coyim/coyim_6.jpg)

Ora selezionate il menu _"Server"_ e inserite: 

*   L'indirizzo dell'hidden service XMPP di Autistici/Inventati
    - autinv5q6en4gpf4.onion - nella casella _"Server"_. 

*   La porta del server (5222) nella casella _"Port"_.

![](/static/img/man_jabber/coyim/coyim_8.jpg)

Finito! Ora potete utilizzare l'Hidden Service di AI per chiacchierare con i
vostri amici e le vostre amiche su Jabber.

## Usare OTR ##

Come già spiegato nell'introduzione di questo tutorial, CoyIM cifrerà in
maniera automatica il contenuto delle vostre conversazioni con
[OTR](https://otr.cypherpunks.ca/) e non permetterà l'invio di messaggi in
chiaro: questo significa che la vostra interlocutrice dovrà necessariamente
usare un client XMPP che supporti a sua volta OTR. 

![](/static/img/man_jabber/coyim/coyim_10.jpg)

**Non dimenticate di verificare l'autenticità della chiave!**

## Per approfondire ##

*   [Il sito ufficiale di CoyIM](https://coy.im/)

*   [CoyIM su GitHub](https://github.com/twstrike/coyim/)
