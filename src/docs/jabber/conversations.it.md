title: Come configurare Conversations
----

Come configurare Conversations
==============================


Conversations è un client di chat instantanee per smartphone Android 4.0+. Si
basa sul protocollo XMPP e pertanto può essere utilizzato con i servizi di chat
offerti da Autistici/Inventati. Dalla sua ha diversi punti di forza.

Innanzi tutto rispetta quella che per noi è una _conditio sine qua non_: questo
software è rilasciato sotto licenza **GPL 3** e [il suo
codice](https://github.com/siacs/Conversations) è pertanto liberamente
ispezionabile e modificabile.

Per quel che riguarda invece il lato sicurezza Conversations implementa
l'utilizzo di tre differenti protocolli per la **cifratura end-to-end delle
comunicazioni**: OpenPGP (supportato solo in maniera sperimentale),
[OTR](https://otr.cypherpunks.ca/) e [OMEMO](https://conversations.im/omemo/).

Sempre su questo versante, il software creato da _inputmice_ e soci/e ha una
**gestione non invasiva dei permessi**: connettività, vibrazione (per ricevere
le notifiche) e poco altro. Se decidete di integrare la vostra lista di utenti
jabber con la rubrica telefonica, l'app avrà accesso anche ai vostri contatti:
in ogni caso, questi resteranno sul vostro smartphone e non saranno uplodati sui
server di terze parti.

Altro aspetto importante da segnalare, è che Conversations può essere scaricato
anche da tutte coloro che per motivi di tutela della privacy - propria ed altrui
- utilizzano una versione di Android senza Play Store: il team che sviluppa
l'applicazione l'ha resa infatti liberamente disponibile su
[F-Droid](https://f-droid.org/).

Infine, questo client per la messaggistica ha un **impatto ridottissimo sul
consumo energetico** della batteria e si presenta con un'**interfaccia utente
pulita** e **di facile utilizzo**.

Vediamo come configurarlo in pochissimi passaggi.


## Configurazione base ##

Una volta installato, aprite Conversations: la prima schermata che vi apparirà
sarà questa.

![](/static/img/man_jabber/conversations/Conversations_1.png)

Selezionate _"Use my own provider"_. Vi si aprirà questa seconda finestra.

![](/static/img/man_jabber/conversations/Conversations_2.png)


Inserite:

*   Il vostro nome utente (i.e. nomeutente@autistici.org) alla voce _"Jabber
    ID"_,

*   La password del vostro account alla voce _"Password"_ (o una password
    specifica per Jabber se avete attivato la [Two Factor
    Autenthication](/docs/2FA) sui nostri
    servizi).

Et voilà. Il gioco è fatto. Premete il pulsante _"Next"_ e siete pronte per
utilizzare Conversations con il server jabber di AI.


## Configurazione con Hidden Service ##

Se volete garantirvi un livello ulteriore di privacy e sicurezza potete accedere
a Jabber utilizzando [il nostro hidden service Tor](/docs/anon/tor).

Per farlo scaricate da
[F-Droid](https://f-droid.org/repository/browse/?fdfilter=orbot&fdid=org.torproject.android)
o dal [Play
Store](https://play.google.com/store/apps/details?id=org.torproject.android)
l'applicazione Orbot. Una volta che questa è stata lanciata, ci sono solo
alcune modifiche da fare alla configurazione base di Conversations.

Aprite la schermata principale di Conversations e selezionate _"Use my own
provider"_. Vi si aprirà questa seconda finestra.

![](/static/img/man_jabber/conversations/Conversations_2.png)

Per ora non inserite né il vostro nome utente, né la vostra password. Toccate
i tre punti in alto a destra per accedere al menu opzioni dell'applicazione e
selezionate la voce _"Expert Settings"_.

![](/static/img/man_jabber/conversations/Conversations_3.png)

A questo punto scorrete la finestra in basso fino alla voce _"Connection"_ e
selezionate le opzioni _"Extended Connection Settings"_ e _"Connect via Tor"_.

![](/static/img/man_jabber/conversations/Conversations_4.png)

Tornate alla schermata iniziale e inserite

*   Il vostro nome utente (i.e. nomeutente@autistici.org) alla voce _"Jabber
    ID"_

*   La password del vostro account alla voce _"Password"_ (o una password
    specifica per Jabber se avete attivato la [Two Factor
    Autenthication](/docs/2FA) sui nostri
    servizi)

*   L'indirizzo dell'hidden service XMPP di Autistici/Inventati
    - autinv5q6en4gpf4.onion - nella casella _"Hostname"_

*   La porta del server (5222) nella casella _"Port"_

![](/static/img/man_jabber/conversations/Conversations_5.png)

Finito! Ora potete utilizzare l'Hidden Service di AI per chiacchierare con i
vostri amici e le vostre amiche su Jabber.


## Usare OTR ##

Come già detto, Conversations implementa diversi protocolli di cifratura
end-to-end finalizzati a garantire una maggiore riservatezza alle vostre
comunicazioni. Vediamo come utilizzare [OTR](https://otr.cypherpunks.ca/).

La prima volta che aprite una finestra di dialogo con un utente presente nella
vostra lista di contatti, premete sul lucchetto nel menu in alto e selezionate
la voce _"OTR"_.

![](/static/img/man_jabber/conversations/Conversations_6.png)

Scrivete un primo messaggio e inviatelo selezionando l'icona della freccia in
basso a sinistra. Conversations avvierà automaticamente lo scambio delle chiavi
di cifratura tra voi e il vostro interlocutore. Da questo momento in avanti le
conversazioni con questo contatto verrano cifrate automaticamente.

![](/static/img/man_jabber/conversations/Conversations_7.png)

**Non dimenticate di verificare l'autenticità della chiave!**


## Usare OMEMO ##

Un altro protocollo per la cifratura end-to-end implementato da Conversations è
[OMEMO](https://conversations.im/omemo/). A differenza di OTR, OMEMO fornisce
alcune interessanti funzioni come le chat di gruppo crittate o la possibilita di
sincronizzare in maniera sicura i vostri messaggi tra differenti dispositivi,
anche quando questi sono off-line.  Vediamo come utilizzarlo.

La prima volta che aprite una finestra di dialogo con un utente presente nella
vostra lista di contatti, premete sul lucchetto nel menu in alto e selezionate
la voce _"OMEMO"_.

![](/static/img/man_jabber/conversations/Conversations_8.png)

Scrivete un primo messaggio e inviatelo selezionando l'icona della freccia in
basso a sinistra. Conversations avvierà automaticamente lo scambio delle chiavi
di cifratura tra voi e la vostra interlocutrice. Da questo momento in avanti le
conversazioni con questo contatto saranno cifrate automaticamente.

![](/static/img/man_jabber/conversations/Conversations_9.png)

**Non dimenticate di verificare l'autenticità della chiave!**
