title: CoyIM Configuration Howto
----

CoyIM Configuration Howto
=================================


CoyIM is a cross-platform (Windows, Mac and Linux) chat tool oriented towards
user security. An overt goal of its developers is to _reduce the attack surface
of your system  to the minimum_. For this reason, CoyIM supports only one
communication protocol (XMPP, the same we use for our instant messaging
services); it does not envisage the installation of third-party plugins; and
features a **minimal graphic interface**, which doesn't even include emoticons.
The embedded components in the client are very limited and represent the
recognized standard in the field of secure online communication.

Some interesting features of CoyIM are:

*  **Tor**. CoyIM automatically detects whether Tor is installed on the user's
   computer: in this case, it automatically routes its connections towards the
   onion network, thus making them anonymous.
*   **OTR**. Every message sent through CoyIM is automatically encrypted with OTR.
    Moreover, CoyIM does not allow to send unencrypted messages to other XMPP
    clients.
*   **TLS**. The communication channel between CoyIM and the chat server is
    encrypted with a further encryption layer.

Another important feature of CoyIM is its default configuration, which
guarantees an optimal level of user protection from its first launch. Lastly,
CoyIM implements a **tool to easily import your OTR private key** from other
XMPP clients (like Pidgin, Adium or Jitsi). In this short tutorial we will see
how to configure it for the Autistici/Inventati chat services.

## Encrypt the CoyIM configuration ##

The first time you launch CoyIM this window will appear:

![](/static/img/man_jabber/coyim/coyim_1.jpg)

CoyIM gives you the chance of saving your client configuration file in an
encrypted mode. In this way, should someone illegitimately access your
computer, they will be neither able to see your username, nor your login password
or the chat server you are using. If you choose to enable this option, click on
_"Yes"_. 

![](/static/img/man_jabber/coyim/coyim_1_password.jpg)

Choose a password to encrypt the CoyIM configuration and press _"Ok"_.  Your
CoyIM configuration file is now encrypted. You'll be required to enter the
password to decrypt it each time you launch the program. If you lose it by any
chance, you will have to configure CoyIM anew.

## Basic configuration ##

Now let's configure CoyIM in order to use it with the Autistici/Inventati chat
services. 

![](/static/img/man_jabber/coyim/coyim_2.jpg)

Click on _"Existing Account"_.

![](/static/img/man_jabber/coyim/coyim_3.jpg)

Now enter:

*   Your username (i.e. username@domain.xx) in _"Your account"_

*   Your account password in the _"Password"_ field (or a Jabber specific
    password if you activated the [Two Factor Autenthication](/docs/2FA)
    on our services)

*   The nickname that will be visualized by your contacts in the _"Display
    Name"_ field

Finally, click on _"Save"_. If your configuration works, this window
will appear:

![](/static/img/man_jabber/coyim/coyim_4.jpg)


## Hidden service configuration ##

If you wish to secure a further level of privacy and security for yourselves you
can access Jabber by using our [Tor hidden service](/docs/anon/tor). To do so,
you need to apply some simple changes to the basic configuration we have just
gone through.

Once you have inserted your username, password and nickname, check the _"Display
all settings"_ box

![](/static/img/man_jabber/coyim/coyim_6.jpg)

Now select the _"Server"_ menu and add: 

*   The Autistici/Inventati XMPP hidden service address (autinv5q6en4gpf4.onion)
    in the _"Server"_ box. 

*   The server port (5222) in the _"Port"_ box.

![](/static/img/man_jabber/coyim/coyim_8.jpg)

Done! Now you can use the AI Hidden Service to chat with your friends on Jabber.


# How to use OTR #

As explained in the introduction of this tutorial, CoyIM will automatically
encrypt the content of your conversations with
[OTR](https://otr.cypherpunks.ca/) and will not allow sending unencrypted
messages: this means that your contact must necessarily use an XMPP client that
supports OTR at the same time. 

![](/static/img/man_jabber/coyim/coyim_10.jpg)


**Do not forget to verify the authenticity of the key!**

## More Info ##

*   [CoyIM Official website](https://coy.im/)

*   [CoyIM on GitHub](https://github.com/twstrike/coyim/)
