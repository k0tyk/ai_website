title: Tor Messenger Configuration Howto
----

Tor Messenger Configuration Howto
==================================

Tor Messenger is a chat client oriented towards security, anonimity and
usability.  Developed by the [Tor
Project](https://blog.torproject.org/blog/tor-messenger-beta-chat-over-tor-easily),
it is a tool that tries to be as user-friendly as possible while safeguarding
our online conversations' privacy as much as possible. 

There are many features that make this software stand out. Above all, Tor
Messenger **automatically routes your conversations to the anonymous Tor
network**, with no need to modify anything in its configuration. Moreover, chat
sessions are **encrypted by default** with the [OTR
protocol](https://otr.cypherpunks.ca/). Its **graphic interface** is usable and
available in many languages. Another important element of this instant messaging
client lies in its **interoperability**: you can install and use it on Linux,
Mac OS X and Windows. _Last but not least_, Tor Messenger is released under the
**GPL3** license: therefore, its source code is freely downloadable, editable
and searchable.

Tor Messenger supports different chat protocols, including XMPP - the same
Autistici/Inventati uses for our instant messaging service. Let's see how to
configure it in a few steps.


## How to directly connect to the Tor network ##

The first time you launch Tor Messenger this window will open up:

![](/static/img/man_jabber/tormessenger/tm_1.jpg)

Click on the _"Connect"_ button and wait a few seconds.

![](/static/img/man_jabber/tormessenger/tm_2.jpg)

Once the client creates a circuit through the Tor network, you will be asked to
configure your XMPP account on the Autistici/Inventati servers.


## How to connect to the Tor network with pluggable transport or proxy ##

In most cases you should have no problems in directly connecting to the Tor
network. But if (a) your provider blocks access to Tor, (b) you don't
want your provider to know you are using Tor or (c) you are using a network -
even your company network - that allows proxy-only Internet access, then click on
_"Configure"_. 

The following window will open up:

![](/static/img/man_jabber/tormessenger/tm_3.jpg)

If your provider prevents access to the Tor network, select _"Yes"_ and
configure a pluggable transport in order to circumvent the block (we suggest to
read the [official Tor Project
documentation](https://www.torproject.org/docs/pluggable-transports.html.en) for
further info). Otherwise, if you do not believe that your connection is being
censored, select _"No"_ and move on to the following screen.

![](/static/img/man_jabber/tormessenger/tm_4.jpg)

In this second window you will be asked whether you are using a proxy in order
to connect to the Internet. If this is the case, select _"Yes"_ and enter the
specific connection parameters. You do not know them? Ask your network admin to
give you a hand. Otherwise, if your network connection does not use a proxy,
click on _"Continue"_. 


## How to use Tor Messenger on AI ##

Once you are connected to the Tor network, a menu will open up with the
different communication protocols supported by Tor Messenger. Our chat server
uses _"XMPP"_: select it and click _"Next"_.

![](/static/img/man_jabber/tormessenger/tm_5.jpg)

Now enter your Autistici/Inventati account credentials. If your account is
bob@autistici.org, you will have to write bob in the _"Username"_ field and
autistici.org in _"Domain"_. When you're done click _"Next"_.

![](/static/img/man_jabber/tormessenger/tm_6.jpg)

Now enter your account password (or a Jabber specific password if you activated
the [Two Factor Autenthication](/docs/2FA) on our services) and click
_"Next"_.

![](/static/img/man_jabber/tormessenger/tm_7.jpg)

Now the _"advanced options"_ window will open up. Leave it blank and
proceed by clicking the _"Next"_ button.

![](/static/img/man_jabber/tormessenger/tm_7_bis.jpg)

Tor Messenger is ready to be used with Autistici/Inventati chat services. Click
on 'Connect' and wait a bit.

![](/static/img/man_jabber/tormessenger/tm_8.jpg)

Et voilà! You did it!

![](/static/img/man_jabber/tormessenger/tm_9.jpg)


## How to use Tor Messenger with the AI hidden service ##

If you wish to secure a further level of privacy and security for yourselves,
you can access our chat services by using our [Tor hidden
service](/docs/anon/tor). To do so, follow the instructions in the above
section ("How to use Tor Messenger on AI") up to the _"advanced options"_
window. When in the _"advanced options"_ window, open the _"XMPP Options"_ menu,
and enter the Autistici hidden service address (autinv5q6en4gpf4.onion) in the
_"Server"_ field.

![](/static/img/man_jabber/tormessenger/tm_10.jpg)


## How to use OTR ##

As explained in the introduction of this tutorial, Tor Messenger will
automatically encrypt the content of your conversations with
[OTR](https://otr.cypherpunks.ca/) and will not allow sending unencrypted
messages: this means that your contacts must necessarily use an XMPP client that
supports OTR. 

![](/static/img/man_jabber/tormessenger/tm_11.jpg)


**Do not forget to verify the authenticity of the key!**

## More info ##

*   Tor Messenger on the [Tor Project
    wiki](https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger).
*   Tor Messenger on the [Tor Project
    blog](https://blog.torproject.org/category/tags/chat-client).
*   The [design
    document](https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger/DesignDoc)
    for Tor Messenger.
