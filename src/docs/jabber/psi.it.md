title: Come configurare PSI per Jabber
----

Come configurare PSI per Jabber
======================================

Installate [psi](http://psi-im.org/download) e quindi lanciatelo: vi si aprira' la finestra "Add Account".

![](/static/img/man_jabber/it/psi-01.png)

Nella casella "Name" inserite il vostro indirizzo e-mail completo di dominio e cliccate su "Add". A questo punto vi si aprira' la finestra
"Account Properties".

![](/static/img/man_jabber/it/psi-02.png)

Nella casella "Jabber ID" inserite il vostro indirizzo e-mail e passate alla scheda "Connection".

E' preferibile non inserire la password in questa schermata (dove viene memorizzata) ma digitarla ad ogni connessione a jabber.

![](/static/img/man_jabber/it/psi-03.png)

Spuntate la casella "Use SSL encryption" e come "Host" scrivete "jabber.autistici.org". Verificate che la porta sia la 5222 e passate alla
scheda "Preferences".

![](/static/img/man_jabber/it/psi-04.png)

Spuntate la casella "Ignore SSL warnings", quindi salvate le configurazioni. A questo punto non dovrete fare altro che collegarvi con la
vostra password e cominciare a chiacchierare con jabber :)
