title: User Panel Howto
----

User Panel Howto
================

** ATTENTION - THIS PAGE IS OBSOLETE, THE IMAGES DO NOT CORRESPOND TO THE CURRENT PANEL, WE ARE WORKING TO UPDATE IT **

To use any of the [services](/services/) provided by A/I, you have to own a A/I account and thus be able to access your
**User Panel** where you can find any information about your services and how to handle them.

This manual wishes to detail what you can do within your **User Panel**

<a name="topsummary"></a>

Summary
-------

- [Login](#login)
- [Things to do after the first login](#firstlogin)
  1.  [Set password recovery question](#recoverquestion)
  2.  [Change password](#passwordchange)
- [Main page](#main)
- [How to change the User Panel language](#languagechange)
- [How to read a mailbox and how to change its settings form the User Panel](#mailbox)
  1.  [How to add or remove an alias](#alias)
  2.  [How to download your address book](#addressbook)
- [How to manage a website from the User Panel](#website)
- [How to manage an WebDAV account from the User Panel (and how to change WebDAV account password)](#webdav)
- [How to manage a mysql database from the User Panel](#mysql)
- [How to manage mailing lists from the User Panel](#mailinglist)
- [How to manage a blog on Noblogs.org from the User Panel](#blog)
- [How to close your email account](#close)

<a name="login"></a>

Login
-----

As you can easily see from the image, to login into your **User Panel** you have to click on the big red button in the [homepage](/), fill
in your account (the full mailbox address) and your password. Then click on *login*.

![](/static/img/man_userpanel/en/login_page01.png "User Panel Login Page")

If that button does not work, you can access the **User Panel** login page [directly](https://accounts.autistici.org) and fill in your account (the full
mailbox address) and your password. Then click *login*.

![](/static/img/man_userpanel/en/login_page02.png "User Panel Login Page")

<a name="firstlogin"></a>

### Things to do after the first login

<a name="recoverquestion"></a>

#### Recover question

If it's the first time you login into your **User Panel** there are some things your have to do right away.

As you can see from the dark red warning on the top of your **User Panel**, first of all you have to setup a **recover question**: if you
happen to forget or lose your password, this question and its answer will be the only ways to recover it and be able to login to your panel
again.

![](/static/img/man_userpanel/en/first_login_set_recover_question.png "Warning set password recovery question")

Once you click on the link included in the dark red warning you will enter our *Gatti* section ("Gatti" means "cats" in Italian and the
password recovery procedure is called like this since the default question concerns your pets' name) and will be able to setup your password
recovery question.

![](/static/img/man_userpanel/en/gatti01.png "Password Recovery Form - aka Gatti")

Fill in a question and its relative answer, then click on the *Save* button.

![](/static/img/man_userpanel/en/gatti02.png "Password Recovery Form - aka Gatti")

**Warning**: you have to be sure to remember the exact answer to the question that you typed (including Capital letters or symbols or
numbers) since providing the correct answer will be the only way to recover your password.

Please keep in mind that **we do not know the answer you provided** since it's kept encrypted in our database. You are the only person
knowing what to answer and thus being able to recover your lost or forgot password.

Once you have clicked on the *Save* button you will be automatically redirected back to your **User Panel** where a new darkred warning
informs you that you have successfully set your password recovery question. Good, let's proceed to the next step.

![](/static/img/man_userpanel/en/first_login_set_recover_question_done.png "You successfully changed your password recovery question")

<a name="passwordchange"></a>

#### Change your password

The second thing you need to do right away after you first login into your **User Panel** is changing your password, especially if you asked us to send it via email.

To change your password, click on your user name in the top right
corner, to open a drop-down menu with the password change option.

![](/static/img/man_userpanel/en/pannello_cambio_password_en.png)

**Please note** that in this drop-down menu you will also find a link for setting your
[password recovery question (if you haven't done so already)](#recoverquestion),
and to enable [2-factor authentication](../2FA).

Once you click on the *Change password* link you will be directed to a form where you can fill in **twice** your new password. Once you'll
have done so and clicked on the *Update* button, you'll have a new password to login into your **User Panel** AND **to login into your
mailbox**.

![](/static/img/man_userpanel/en/change_password01.png "Change Password Page")

![](/static/img/man_userpanel/en/change_password02.png "Fill in the form and click on Save to change your password")

**Warning**: here you are changing your main account password. If you wish to change your WebDAV password or Mysql password, you'll have to
look into [other parts of this manual](#topsummary).

Once you have clicked on the *Update* button you will be automatically redirected back to your **User Panel** where a new darkred warning
informs you that you have successfully changed your password. Now you are ready to get acquainted with your **User Panel**. Let's move on.

![](/static/img/man_userpanel/en/userpanel_main_password_changed.png "Your password has been successfully changed")

<a name="main"></a>

User Panel Main Page
--------------------

![](/static/img/man_userpanel/en/userpanel_main.png "Your User Panel Main page")

What you can see in the picture above is a very basic version of how your **User Panel** could look like. It's the **User Panel** of someone
owning a simple mailbox on our servers. No additional addresses, no website, no blogs, no list, no newsletter, no nothing. More or less like
the 50% of our users, so don't feel ashamed or anything.

Now let's try to make it a bit more cheerful and to make sense of it.

![](/static/img/man_userpanel/en/userpanel_main_boxes.png "Your User Panel Main page areas")

You can split up the **User Panel** in different areas: some of them are really crowded but the colors should help you make out which is
which.

-   The **Blue boxes** is where news go: on the right sidebar of the panel you can find the latest post on the [A/I collective
    blog](http://cavallette.noblogs.org), ie where you can find something we think it's important for you to know, including hardware or
    software failures; on the bottom part of the panel you can find a three fold news areas, including the latest post about
    [noblogs sysadmin news](http://noblogs.org) and the latest post from the [noblogs community](http://noblogs.org).
-   Below the bottom blue boxes you can find a single **Yellow box**: here is where you can change the language your panel uses. By default
    it's set to the language your browsers shows to understand, but it can be wrong. If you want to know how to change the **User Panel**'s
    language, [see below](#languagechange).
-   In the top part of the page the **Green box** shows you where you can find support, documentation, howtos and anything that could help
    you sort problems you are facing with our services. The same resources can be found in our [Support page](/get_help).
-   Just under the green box there's a single **Purple box** marking the link to ask for more stuff from A/I: if you already have a mailbox
    but would like to open up a website or a mailing list, that's the link you have to click. Before you do, i suggest you stroll over our
    [policy](/who/manifesto), just to be sure you are about to know
    what you are getting messed with. You sure? Go on and [click on the link then](https://wwww.autistici.org/services/).
-   Last but not list, the central part of the **User Panel** main page is squatted by a big **Red box**: here is your account management
    area, where you'll find all the services you are running on our servers and any information connected to them. Most of what you find in
    this manual relates to this box. Note, however, that there is a small but very important **Red box** in the top-right corner of the
    page: there you can read which user you are for our servers, and a link to **logout** of the **User Panel**. If you did not move on to
    your mailbox, remember to click on it before leaving the panel.

<a name="languagechange"></a>

How to change the User Panel language (Yellow box)
--------------------------------------------------

If you want to change the language of your **User Panel** simply choose your desired language from the dropdown menu in the bottom part of
the panel main page and click on the *Update* button (see the yellow circle in the picture below). Once you click on the button the page
will reload in the chosen language and a darkred alert (top of the picture) will inform you that your **User Panel** language has been
changed.

![](/static/img/man_userpanel/en/language.png "change your user panel language")

<a name="mailbox"></a>

How to read mailbox and to change its settings
----------------------------------------------

We have already explained [how to change your mailbox password](#passwordchange) and [how to set your password recovery question](#recoverquestion).
Now it's time to move on and to login into your mailbox to read some e-mails: click on the *mail icon* or on
your *mail address* in the account management area. You will be redirected automatically to our webmail: you can find a
[useful howto about it in our support page](/docs/mail/roundcube). Remember that once you logout of the webmail, you'll be also
automatically logged out of the **User Panel**.

![](/static/img/man_userpanel/en/mailbox_read.png "click to read your mailbox")

If you want to know how much space are you using up, just read at the bottom of your mailbox management area: we would like to remind you
that we do not set a maximum amount of space for your mailbox, but remember that our resources are not endless and that you share our
servers with thousands of other people. Be careful how much space you use up.

![](/static/img/man_userpanel/en/mailbox_space.png "read how much space you use up on our servers")

<a name="alias"></a>

### How to add or remove and alias

First of all: what is an **alias**?
 An **alias** is an alternative mail address that you can give around to receive e-mails on your account. You can also configure it as
sender in your mail client to send e-mails. But it will always be connected to the same account: that is your mailbox account will be able
to send and receive e-mails on both addresses.

On A/I servers you can have and manage at most 5 aliases for any single account you have. If you need more, please [contact us](/get_help).
 If you want to create an **alias** for your mailbox, click on the link next to your mailbox account.

![](/static/img/man_userpanel/en/mailbox_createalias.png "Click to create an alias")

Once you click you will be directed to a form where you can fill the alternative address you wish for and click on the *Create* button.

![](/static/img/man_userpanel/en/mailbox_createaliasform.png "Click to create an alias")

Now your mailbox management area changed a bit and you can find your **alias** next to your mailbox address. A dark red warning at the top
of the page confirms you have successfully create an **alias** for your mailbox.

![](/static/img/man_userpanel/en/mailbox_aliascreated.png "You have a new alias!")

If you already have an alias an want to remove it, you simply have to click on the *minus* sign next to the address you wish to delete.

**Warning**: you won't be asked any confirmation before removing the alias. If you discover you deleted the wrong **alias** just create it
anew. The only memory of the mistake will be the dark red warning you have come now to know so well from the previous pictures! :)

![](/static/img/man_userpanel/en/mailbox_deletealias.png "Click here to delete an alias")

<a name="addressbook"></a>

### How to download your address book

If you have been using our webmail a lot, you'll have gathered a certain amount of mail addresses in your address book. If at some point you
wish to back them up or to move onto a mail client, clicking on the link you find next to your mailbox address will enable you to download a
copy of your addressbook to import it wherever you need it.

![](/static/img/man_userpanel/en/mailbox_addressbook.png "download your addressbook")

<a name="website"></a>

How to manage a website from the User Panel
-------------------------------------------

If you requested to A/I something more than a simple mailbox address then your **User Panel** will be somewhat more complicated.
 If you are managing some website on A/I servers, your account management area will be enriched by a new dropdown menu.

![](/static/img/man_userpanel/en/userpanel_main2.png "check your website in the user panel")

If you click on the arrow next to the word *Websites* or *Email Addresses* you can open or close the related dropdown menu. Since we have
already seen what the [*Email Addresses* menu includes](#mailbox), let's move on to the *Websites* tab.

![](/static/img/man_userpanel/en/userpanel_main3.png "check your website in the user panel")

As you can see managing a website includes three different kind of submenu: one dedicated to your **website** proper; another one to the
**WebDAV account** you can use to update your website; and another one to the **databases** connected to a site. Each of this submenu holds
specific configuration possibilities.

![](/static/img/man_userpanel/en/website_submenus.png "How many things you have to manage for a website to work!")

If you click on the *website name* or on the *world icon* you'll be redirected to your webpage. Right next to the *website name* you can
find four links:

-   *Stats* opens a page where you can find the code you need to add to your site to use A/I webstatistics tool. See [A/I stats Howto](/docs/web/webstats)
    for more information.
-   *Description* lets you enter a generic description of your website contents
-   *Options*: you can choose if your site need php, python or simple html pages and if your site needs to send mail via php.
-   *Help* is a link to A/I [website hosting howtos](/services/website#howto)

![](/static/img/man_userpanel/en/website_management_options.png)

![](/static/img/man_userpanel/en/website_stats.png "copy and paste the code for having web statistics on your site")

![](/static/img/man_userpanel/en/website_description.png "Enter a description for your website and click on the 'Save' button")

![](/static/img/man_userpanel/en/website_options.png "personalize your website dynamic properties")

<a name="webdav"></a>

How to manage an WebDAV account from the User Panel (and how to change WebDAV account password)
-----------------------------------------------------------------------------------------------

To update your site you have been given a username and a password (different from the one you use to login into the **User Panel** and
mailbox account).

Using those credentials through a WebDAV client as detailed in [A/I WebDAV Howto](/docs/web/webdav) will allow you
to update your website.

![](/static/img/man_userpanel/en/ftp.png "Manage your WebDAV account")

Next to the **WebDAV Account** tab of your website management area in the **User Panel** you'll find two useful links:

-   One states the full WebDAV URL you have to connect to in order to update your site.
-   Through the second link you can change your WebDAV account password. Click on the link, fill in the new password (**twice**) and then
    click on the *Save* button. After having done so, you'll be returned to your panel with the familiar dark red warning confirming your
    success in changing your WebDAV Account password

![](/static/img/man_userpanel/en/ftp_passwordchange.png "Change your WebDAV account password")

<a name="mysql"></a>

How to manage a mysql database from the User Panel (and how to reset your database password)
--------------------------------------------------------------------------------------------

![](/static/img/man_userpanel/en/mysql.png "Manage your Mysql database")

If you are using a MySQL database to run your website, this tab of your website management area lets you do the following:

-   Click on the *admin* link to reach our MySQL Web Administration interface. You'll have to provide it with your database username and
    password: they should have been sent to you with the website or database activation message. Remember that once you logout from the
    Mysql Administration website you'll still be logged into your **User Panel**. Remember to log out from that as well.
-   Reset your database password by clicking on the *Reset password* link.

<a name="mailinglist"></a>

How to manage a mailing list from the User Panel
------------------------------------------------

If you are a mailing list administrator you will find your mailing lists in your **User Panel**.

![](/static/img/man_userpanel/en/userpanel_main4.png "Manage your Lists")

Clicking on the *Mailing list address* or on the *mail icon* will direct you to the mailing list administration web interface. If your
mailing list is public, you will find the link to the public archives next the the list address.
 To change a list password, click on the *Reset admin password* link.

<a name="blog"></a>

How to manage a blog on noblogs.org from the User Panel
-------------------------------------------------------

If you subscribed to the [Noblogs.org](http://noblogs.org) platform using your A/I account, then the blogs you participate to will be
available in your **User Panel**.

![](/static/img/man_userpanel/en/userpanel_main5.png "Manage your Blogs")

Clicking on the *Blog name* or on the *World icon* will direct you to the blog webpage. Clicking on the *admin* link will direct you to
[noblogs.org admin page](http://noblogs.org/wp-admin/).


<a name="close"></a>

How to close your mail account
-------------------------------------------------------------

![](/static/img/man_userpanel/en/close.png "Close your account")

Clicking on *Close accont* and confirming, your mailbox will be disabled.

That means:

no messages will be received/sent anymore (including the aliases)

in a couple of days all messages in the mailbox will be deleted

if you have set up an addressbook in the webmail, that will remain in our backups.

your email and the aliases will be kept in our user management system so that no one will be able to request
the same addresses (to avoid that someone will receive messages addressed to you).

The data we will keep are:

* the mail email address and the aliases you set
* the mailbox creation date
* the question (not encrypted) and the security answer (encrypted)
* the mailbox password (encrypted)
* the OTP codes, if set (encrypted)

Se invece volete una rimozione completa inserite un ticket nel nostro sistema di
[helpdesk](https://helpdesk.autistici.org)

