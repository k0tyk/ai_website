title: Anonymous Remailer Howto
----

Anonymous Remailer Howto
========================

These are the fundamental instructions to understand how remailer messages are created: if you use a specific client, you won't need to
learn this process, but this text can help you anyway to understand how remailers work.

If you want to send an anonymous message, first of all create a file containing:

- two colon signs ( :: ) in the first line,
- the phrase "Anon-To: e-mail address" in the second line

whereby the e-mail address should be the address the remailer will send the message to.

The third line should be empty and the message text will follow.

e.g.:

    =============================================================
    ::
    Anon-To: destinatario@esempio.org

    Message text
    =============================================================


Remailers only accept messages encrypted through PGP or GPG, so your message must be encrypted with the remailer public key, which you can
get by sending a message to the remailer (mixmaster@remailer.paranoici.org) and by entering "remailer-key" in the subject.

So the above message must be encrypted with the remailer PGP key and eventually sent to mixmaster@remailer.paranoici.org by entering two
colon signs at the beginning of the message and, in the second line, the phrase "Encrypted: PGP" followed by the previously encrypted
message.


    =============================================================
    From: joe@test.com
    To: mixmaster@remailer.paranoici.org

    ::
    Encrypted: PGP

    -----BEGIN PGP MESSAGE-----
    Version: 2.6.3i
    owE1jMsNwjAUBH3gZMk9PClnUoBPUANpwElW2OBPZD8H0gd1UCP2gduuNDNfj
    IcSHT4zCbQmtlbzGFM9T0jSD7QVvEzaPcUlBSSWHQclbnR9YWJNp5BFSLdR9s
    CijF3NGxybry/1Rsqn4la3a0JiIhLvnYGCu9HFtiC8oIxnlkeuIYe+EH=HgDa
    -----END PGP MESSAGE-----
    =============================================================


The remailer will decode your message and send it anonymously. If you want to include a subject or other headers which shouldn't be filtered
by the remailer, you can enter them as explained below before encrypting the message for the remailer:


    =============================================================
    ::
    Anon-To: destinatario@esempio.org
    ##
    Subject: Re: Twofish
    In-Reply-To: Your message of "Tue, 12 Jan 1999 22:47:04 EST." <199901130247.WAA02761@example.com>

    Message text
    =============================================================


Even if PGP encryption is very safe, using a remailer in the simplest way is not the best system for protecting your identity. Therefore,
you can tell the remailer, for instance, to keep the message you've sent for a certain period of time and to forward it later so as to avoid
the so-called traffic analysis.

If you enter the header `Latent-Time: +2:00`, your message will be delayed by 2 hours, while if you use the syntax `Latent-Time: +5:00r`,
you'll have a random delay between 0 and 5 hours.

The best way to use a remailer is by using them in a chain, by sending a message from one remailer to the other before it reaches your
addressee.

Let's make an example with the above message:

    =============================================================
    ::
    Anon-To: mixmaster@remailer.paranoici.org

    ::
    Encrypted: PGP

    -----BEGIN PGP MESSAGE-----
    Version: 2.6.3i
    owE1jMsNwjAUBH3gZMk9PClnUoBPUANpwElW2OBPZD8H0gd1UCP2gduuNDNfI
    T4zCbQmtlbzGFM9T0jSD7QVvEzaPcUlBSSWHQclbnR9YWJNp5BFSLdR9CijF3
    ybry/1Rsqn4la3a0JiIhLvnYGCu9HFtiC8oIxnlkeuIYe+EH=HgDq
    -----END PGP MESSAGE-----
    =============================================================


you can encrypt this message with the PGP key of another remailer (e.g. Cripto) and send the encrypted message to this remailer:
[anon@ecn.org](mailto:anon@ecn.org)

Thus Cripto will receive the message and decode it and will find the instructions to send it to mixmaster@remailer.paranoici.org, which in
its turn will decode it and send it to addressee@example.org

You can find a list of cryptographic software and remailer clients here: [autistici.org/crypto](https://www.autistici.org/crypto) and some additional info on our remailer webpage:
<http://remailer.paranoici.org>



