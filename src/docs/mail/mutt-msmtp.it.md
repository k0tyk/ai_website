title: Configurazione di Mutt+msmtp
----

Configurazione di Mutt+msmtp
========================================

Obiettivi:
----------

-   leggere la propria posta via POP/SSL con mutt
-   inviare la propria posta su un canale crittato e autenticato con mutt e msmtp

Presupposti:
------------

-   familiarità con la gestione di una mail via POP con mutt
-   famigliarità con la configurazione di msmtp con mutt
-   disponibilità di un account UTENTE@DOMINIO.ORG su A/I e relativa PASSWORD

Leggere la posta via POP con mutt
---------------------------------

Per leggere la vostra posta via POP con mutt dovete aggiungere e personalizzare le seguenti righe al vostro file *.muttrc*: ovviamente al
posto di PASSWORD e UTENTE dovrete inserire [i dati che vi sono stati forniti](/docs/mail/connectionparms) all'atto
dell'attivazione della casella di posta.

    	macro index <F8> ":set dsn_notify=<enter>:unhook *<enter>:unset
    	pop_user<enter>:unset pop_pass<enter>:set
    	pop_host=mail.autistici.org<enter>:set
    	pop_user=UTENTE@DOMINIO.ORG<enter>:set
    	pop_pass=PASSWORD<enter><change-folder-readonly>
    	pops://UTENTE@DOMINIO.ORG@mail.autistici.org<enter>:my_hdr From:
    	UTENTE <UTENTE@DOMINIO.ORG><enter>:set sendmail='/usr/bin/msmtp -a
    	UTENTE'<enter>:reply_to From: UTENTE
    	<UTENTE@DOMINIO.ORG><enter>" "UTENTE@DOMINIO.ORG POP"

Aggiungete il certificato di A/I per il protocollo POP al vostro file *~/.mutt/certificates* e aggiungete la seguente linea al vostro
*.muttrc*

    	set certificate_file=~/.mutt/certificates

Alla prima connessione altrimenti mutt vi chiederà se salvare il certificato automaticamente

Se avete dei conflitti con altri account configurati sul vostro mutt, allora aggiungete al vostro *.muttrc* la seguente riga:
    	
    	set pop_authenticators="digest-md5:apop:user"

Inviare posta via SMTP con msmtp
--------------------------------

Per inviare posta aggiungete le seguenti righe al vostro file *.msmtprc*:

    	account UTENTE@DOMINIO.ORG
    	port 587
    	from UTENTE@DOMINIO.ORG
    	user UTENTE@DOMINIO.org
    	auth on
    	password PASSWORD
    	tls on
    	tls_starttls on
    	tls_trust_file /ets/ssl/certs/ca-certificates.crt   # questo file contiene la CA di letencrypt che firma i nostri certificati
                                                            # e' contenuto nel pacchetto ca-certificates che dovete avere installato
    	host smtp.autistici.org
    	tls_certcheck on

Verificate che le vostre impostazioni funzionino inviandovi un messaggio di test:

    	echo test | msmtp -a UTENTE UTENTE@DOMINIO.ORG
