title: Configurazione di Outlook Express
----

Configurazione di Outlook Express
=========================

Ultimo aggiornamento: *Ottobre 2005*

Questa guida mostra come impostare il proprio account di posta elettronica su A/I in modo che le comunicazioni siano sicure e quindi
a prova di intercettazione. In questo esempio imposteremo l'account di "Antani Sblinda", corrispondente all'indirizzo
"antani@inventati.org".

NOTA: impostazioni testate su Microsoft Outlook Express 6, ma le differenze con la versione 4 e 5 dovrebbero essere trascurabili.

<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left">Aprite Outlook Express. Se è la prima volta che lo fate (aprire
OE, s'intende...) verrete spediti diretti all'impostazione di un nuovo account.
Altrimenti selezionate la voce &quot;Account...&quot; dal menu
&quot;Strumenti&quot; (fig. 1); nella finestra che apparirà, cliccate sul
bottone<br />
 &quot;Aggiungi...&quot; e quindi su &quot;Mail&quot;.</td>
</tr>
<tr class="even">
<td align="left"><div align="center">
<em>Fig. 1</em><br />
 <img src="/static/img/man_mail/it/posta_oe_1.jpg" alt="" />
</div></td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 2<br />
</em> <img src="/static/img/man_mail/it/posta_oe_2.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">La prima cosa da impostare è il nome da visualizzare (Full Name) nei messaggi che invierete. Scrivetelo, e cliccate su &quot;Avanti&gt; &quot;.
</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 3</em><br />
 <img src="/static/img/man_mail/it/posta_oe_3.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Nella finestra successiva viene chiesto se utilizzare
l'indirizzo disponibile o crearne uno nuovo su Hotmail. Scegliete la prima
possibilità e inserite l'indirizzo di posta elettronica assegnato. Cliccate
quindi su &quot;Avanti&quot;.</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 4<br />
</em> <img src="/static/img/man_mail/it/posta_oe_4.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Il passo seguente è di impostare i server di posta. Se non è
già impostato, scegliete &quot;POP3&quot; come tipo di server, e scrivete
&quot;mail.autistici.org&quot; nella casella &quot;Server posta in arrivo&quot;
e smtp.autistici.org nella casella &quot;Server posta in uscita&quot;. Cliccate
ancora su &quot;Avanti&quot;.</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 5<br />
</em> <img src="/static/img/man_mail/it/posta_oe_5.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Dobbiamo ora impostare il nome dell'account, ossia il vostro nome utente (identico all'indirizzo di posta, in questo caso antani@inventati.org), e, se volete, la vostra password. Tenete presente che, nel caso in cui decidiate di memorizzare la password, questa verrà scritta sul disco fisso del vostro computer, il che significa che chiunque può accedere a quel computer può scaricare la vostra posta. Cliccate su &quot;Avanti&quot;.</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 6<br />
</em> <img src="/static/img/man_mail/it/posta_oe_6.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Abbiamo quasi finito: cliccate su &quot;Fine&quot;.</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 7<br />
</em> <img src="/static/img/man_mail/it/posta_oe_7.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Dovreste essere tornate alla finestra di impostazione degli account. Se non ci siete, selezionate la voce &quot;Account...&quot; dal menu &quot;Strumenti&quot;, come in figura 1. Selezionate la scheda &quot;Posta elettronica&quot;, come in figura 8, e fate doppio clic sull'account appena impostato.</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 8<br />
</em> <img src="/static/img/man_mail/it/posta_oe_8.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Si aprirà una finestra come quella di figura 9; selezionate la scheda &quot;Server&quot; e la finestra diventerà come quella di figura 10.</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 9<br />
</em> <img src="/static/img/man_mail/it/posta_oe_9.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left"><div align="center">
<em>Fig. 10<br />
</em> <img src="/static/img/man_mail/it/posta_oe_10.jpg" alt="" />
</div></td>
</tr>
<tr class="odd">
<td align="left">Qui selezionate, come in figura 10, la casella &quot;Autenticazione del server necessaria&quot;. Selezionate quindi la scheda &quot;Impostazioni avanzate&quot;</td>
</tr>
<tr class="even">
<td align="left">Figura 11. Selezionate ENTRAMBE le caselle &quot;Il server necessita di una connessione protetta (SSL)&quot;. Modificate quindi la porta della &quot;Posta in uscita (SMTP)&quot; da 25 a 465 (dovrebbero funzionare entrambe).</td>
</tr>
<tr class="odd">
<td align="left"><div align="center">
<em>Fig. 11<br />
</em> <img src="/static/img/man_mail/it/posta_oe_11.jpg" alt="" />
</div></td>
</tr>
<tr class="even">
<td align="left">Potete ora cliccare su OK sia in questa finestra che in quella delle impostazioni, e scaricare in tranquillità i vostri messaggi dal server, e spedirli sempre utilizzando una connessione sicura. Ricordatevi solo di inserire la password quando il programma ve la chiede...</td>
</tr>
</tbody>
</table>

Se volete saperne di piu sul funzionamento della posta e sul perché di qeuste impostazioni leggete la [nostra pagina sul servizio di mailbox di A/I](/services/mail).
Per altre info date pure un'occhio alla [nostra sezione HOWTO](/docs/).


