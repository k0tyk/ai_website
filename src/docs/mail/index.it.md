title: Documentazione dei servizi di posta
----

# Documentazione per gli utenti dei servizi di posta di A/I

* [Parametri di connessione per i client di posta](/docs/mail/connectionparms "parametri di connessione per gli utenti di posta di A/I")
* [Note tecniche sul servizio](/docs/mail/tech_mail "note tecniche sul servizio di posta elettronica di A/I")
* [Autenticazione a due fattori](/docs/2FA "Autenticazione a due fattori")
* [Alcune semplici regole per tutelare la propria privacy](/docs/mail/privacymail "Alcune semplici regole per tutelare la propria privacy")
* [Come creare una password sicura](/docs/mail/passwd_safe "come creare una password sicura")
* [Cambiare o recuperare la propria password](/docs/mail/passwd "come cambiare o recuperare la propria password")
* [Come usare la webmail di A/I (Roundcube)](/docs/mail/roundcube "come usare la webmail di A/I")
* [Come configurare dei filtri sulla webmail di A/I)](/docs/mail/sieve "Come configurare dei filtri sulla webmail di A/I")



## Istruzioni su come configurare i vari mail clients.

* [Evolution](/docs/mail/evolution)
* [Fetchmail](/docs/mail/fetchmail)
* [iPhone](/docs/mail/iphone)
* [K9 Mail](/docs/mail/k9mail)
* [Apple Mail (macOS)](/docs/mail/mail-osx)
* [Mutt](/docs/mail/mutt-msmtp)
* [Outlook](/docs/mail/outlook)
* [Sylpheed](/docs/mail/sylpheed)
* [Postfix](/docs/mail/postfix)
* [Thunderbird](/docs/mail/thunderbird)

