title: Come e perché tutelare la privacy della tua posta
----

Come e perché tutelare la privacy della tua posta
=================================================

Sommario
-------------------

 <a name="s1"></a>[1. Sulla privacy](#1)

 <a name="s1.1"></a>[1.1. Perché crittare le mail](#1.1)

 <a name="s1.1.1"></a>[1.1.1. Segreti non legati alle persone: negoziati, finanza, giustizia](#1.1.1)

 <a name="s1.1.2"></a>[1.1.2. Segreti legati alle persone: vita personale, intimità, sentimenti privati](#1.1.2)

 <a name="s2"></a>[2. I rischi della TUA casella di posta elettronica](#2)

 <a name="s2.1"></a>[2.1. Il percorso delle email](#2.1)

 <a name="s2.1.1"></a>[2.1.1. Tutte le copie delle tue mail](#2.1.1)

 <a name="s2.2"></a>[2.2. Il rischio si articola dunque su due livelli](#2.2)

 <a name="s3"></a>[3. Accorgimenti tecnici e crittografia](#3)

 <a name="s3.1"></a>[3.1. Prima di tutto, crittare è più semplice di quanto non pensi](#3.1)
 <a name="s3.2"></a>[3.2. Come funziona la crittografia?](#3.2)
 <a name="s3.3"></a>[3.3. Che programma devo usare per crittare le mie mail?](#3.3)
 <a name="s3.3.1"></a>[3.3.1. Ma per usare GPG che cosa devo fare? Molto semplice](#3.3.1)

 <a name="s3.3.2"></a>[3.3.2. Ma nella pratica come funziona?](#3.3.2)

 <a name="s3.3.3"></a>[3.3.3. Dove metto la mia chiave pubblica?](#3.3.3)

 <a name="s3.4"></a>[3.4. Ricapitolando](#3.4)
 <a name="s4"></a>[4. Per i più paranoici: anonymous remailer](#4)

<a name="1"></a>

[1. Sulla privacy](#s1)

Sempre più spesso nelle inchieste della magistratura si fa esplicito riferimento alle discussioni degli indagati circa la possibilità di
cifrare le proprie missive e si valuta questo fatto come una precisa indicazione della loro volontà di compiere azioni illegali. Altrettanto
frequentemente si leggono negli atti stralci di email intercettate.

A partire dal capillare [spionaggio su scala globale](https://www.eff.org/nsa-spying) operato
dall'[NSA](https://it.wikipedia.org/wiki/National_Security_Agency) fino alle numerose [inchieste che A/I ha dovuto
affrontare](/who/collective), i fatti di ogni giorno dimostrano che le nostre comunicazioni e le nostre vite sono
costantemente sorvegliate.

Stiamo procedendo a grandi passi verso un controllo di massa, nel quale la privacy cade giù a colpi di ragion di stato, tra teoremi
inquisitori e lo spauracchio del terrorismo.

In un sistema di cose nel quale il controllo viene sempre più imposto come la panacea di tutti i mali, vorremmo cercare di fare chiarezza
sui pericoli che la comunicazione via email comporta, e sulle contromisure che è possibile adottare.

<a name="1.1"></a>

[1.1. Perché crittare le mail](#s1.1)

Se non sai cosa significa cifrare, crittare ecc., dai un'occhiata qui: <https://it.wikipedia.org/wiki/Crittografia>.

Una mail che non è stata crittata ed è stata inviata tramite Internet è come una cartolina senza busta: postini, portieri, vicini e chiunque
altro possa venirne in possesso sono liberamente in grado di leggere il messaggio che hai scritto.

Non ci stancheremo mai di ricordare che l'uso della crittografia non serve a proteggere solo la tua privacy, ma anche quella dei tuoi
corrispondenti.

<a name="1.1.1"></a>

[1.1.1. Segreti non legati alle persone: negoziati, finanza, giustizia](#s1.1.1)

Giornalisti, avvocati, giudici, medici, commercialisti: sono molte le professioni che, per contratto, deontologia o legge, sono tenute al
segreto professionale. A usare Internet per motivi professionali sono sempre più persone, e chi deve salvaguardare il segreto professionale
è tenuto a crittare le sue mail per evitare che nei meandri del Web si diffondano liberamente proposte commerciali, dossier giudiziari o
cartelle cliniche.

Se questi documenti non vengono crittati, si tralasciano le precauzioni minime necessarie per salvaguardare il segreto professionale e ci si
espone a rischi legali e finanziari notevoli.

<a name="1.1.2"></a>

[1.1.2. Segreti legati alle persone: vita personale, intimità, sentimenti privati](#s1.1.2)

Non critti le mail perché non hai “niente da nascondere”? Complimenti, però di solito della tua intimità ti preoccupi molto lo stesso,
altrimenti perché chiudere le tende delle finestre?

Certo non ti piacerebbe che qualche sconosciuto seduto dietro al computer del tuo fornitore d'accesso a Internet sorridesse nel leggere, a
tempo perso, le mail che ti scambi con l'amic\* del cuore. Se finora non hai crittato le tue mail, è assolutamente plausibile che qualche
sconosciuto abbia già letto quel che hai scritto...

<a name="2"></a>

[2. I rischi della TUA casella di posta elettronica](#s2)

<a name="2.1"></a>

[2.1. Il percorso delle email](#s2.1)

La tua posta elettronica è a rischio in vari modi: quando spedisci un messaggio, per prima cosa il client di posta che usi contatta un
server attraverso un protocollo detto SMTP e trasferisce a quest’ultimo il messaggio. Questo trasferimento a volte avviene in chiaro
(nessun tipo di cifratura viene applicato al messaggio). Il server SMTP a sua volta contatta il server di destinazione, e il passaggio
attraverso la rete può avvenire di nuovo in chiaro.

Inoltre, ogni volta che spedisci una mail, i computer dei fornitori di servizi memorizzano una copia della tua lettera. Vediamo come:

<a name="2.1.1"></a>

[2.1.1. Tutte le copie delle tue mail](#s2.1.1)

Le tue mail viaggiano in Internet attraverso una sequenza di copie.

Le e-mail si spostano in rete attraverso una sequenza di copie effettuate da un server di posta (ad esempio quello del tuo ISP, il fornitore
d'accesso a Internet) su un altro server di posta.

Se per esempio abiti a Roma Nord e invii una mail a una persona che abita a Roma Sud, le copie che si creeranno della tua mail saranno
perlomeno:

1.  Il tuo computer (**copia originale**) invia la mail a un primo computer situato presso il tuo fornitore d'accesso (**copia 1**);
2.  il computer del fornitore d'accesso manda la copia nel migliore dei casi direttamente ad un computer del fornitore d'accesso del tuo
    destinatario (**copia 2**). Ma i passaggi potrebbero essere di piu';
3.  il computer dell'ISP del tuo destinatario mantiene una copia della mail (**copia 3**), in attesa che quest'ultimo scarichi la posta
    (**copia finale**).

Per attraversare solo qualche quartiere, insomma, la tua mail è stata salvata almeno 2 volte su 2 dischi fissi diversi (2 server server di
posta degli ISP), e tutte e 2 le volte in copie perfette. E dietro ciascuno di questi dischi rigidi si nascondono imprese commerciali,
informatici curiosi, amministratori pubblici dei generi più diversi, vari ed eventuali... Inoltre 2 copie è il caso migliore, se provi a
visualizzare gli header delle mail che ricevi (ogni programma di posta ha un'opzione per farlo), vedrai che i passaggi sono molti più di 2, e di conseguenza anche le copie.

In teoria, per il momento queste copie multiple della tua mail dovrebbero essere cancellate nell'arco di qualche ora da ogni fornitore
d'accesso. Tuttavia, le leggi europee contro il “cybercrimine” prevedono la conservazione di tutte le
copie per diversi mesi, almeno per quanto riguarda le parti che indicano il mittente e il destinatario

<a name="2.2"></a>

[2.2. Il rischio si articola dunque su due livelli](#s2.2)

In primo luogo, durante le sue peregrinazioni attraverso la rete, il messaggio può essere intercettato e letto se la connessione non è cifrata. In secondo luogo, durante le
soste nei vari server, il messaggio è accessibile a chiunque vi abbia accesso.

<a name="3"></a>

[3. Accorgimenti tecnici e crittografia](#s3)

Per quanto riguarda il primo problema, puoi utilizzare canali di comunicazione cifrati fra te e i server che usi (usando SMTP con supporto TLS/SSL, POP3s o IMAP su TLS/SSL).

Tra i nostri manuali trovi [alcuni documenti](/services/mail#howto) che illustrano come configurare il client di posta
in modo da utilizzare SSL/TLS con Autistici/Inventati

I nostri server offrono TLS/SSL, ma altri provider non lo fanno. Controlla se il provider di posta della persona a cui scrivi cifra la connessione andando su [questo sito](https://www.checktls.com/TestReceiver) e inserendo nello spazio "eMail Target" il dominio del provider di posta del tuo destinatario (la parte dopo la @ nell'indirizzo di posta).

Per quanto riguarda il secondo problema, la soluzione è utilizzare programmi per la cifratura del contenuto delle email. È fondamentale però
che si tratti di un **programma a codice aperto**: solo in questo modo si può star sicuri che faccia proprio quello che sostiene di fare. I
programmi di crittazione possono essere infatti sottoposti ad attacchi: qualcuno potrebbe volersi creare una “backdoor”, una scappatoia che
gli permetta di riuscire comunque a leggere una mail crittata. Se il programma non è a codice aperto, se cioè il suo codice sorgente non è
disponibile e quindi non è leggibile, i programmatori decisi a garantire a ognun\* la possibilità di salvaguardare la propria privacy non
potranno verificare che il programma non contenga nessuna backdoor.

<a name="3.1"></a>

[3.1. Prima di tutto, crittare è più semplice di quanto non pensi](#s3.1)

Se sai gestire un normale programma di posta (scrivere mail, inserire allegati ecc.), allora non avrai problemi a gestire la crittografia.

GPG, il programma a codice aperto che noi (e non solo noi) vi consigliamo, è infatti piuttosto semplice da utilizzare, anche perché può
essere installato tramite un apposito plugin su diversi client di posta. In particolare noi raccomandiamo il connubio del client di posta
Thunderbird e del software Enigmail (disponibili per tutti i sistemi operativi).

In ogni caso, la sicurezza che si ottiene è strettamente legata al livello di protezione che si riesce a esercitare sulla propria chiave
privata, cioè sul file che contiene “l'innesco” dei meccanismi crittografici. Persa questa, ogni sicurezza diviene illusoria. Per sapere che
cos'è una chiave privata, leggi oltre.

<a name="3.2"></a>

[3.2. Come funziona la crittografia?](#s3.2)

I metodi per crittare si dividono in due categorie: simmetrici e asimmetrici.

Un metodo di crittazione simmetrico è quello, ad esempio, che fa coincidere le lettere dell'alfabeto coi relativi numeri:

A ---&gt; 1

B ---&gt; 2

C ---&gt; 3

e così via.

Il punto debole di questi sistemi è che nel momento stesso in cui si viene a sapere come è stato crittato un messaggio, lo si può
agevolmente decrittare.

Un sistema asimmetrico è invece più raffinato. La crittazione e la decrittazione avvengono tramite due meccanismi differenti, così tanto
differenti che il meccanismo utilizzato per crittare può essere tranquillamente di pubblico dominio.

Facciamo un esempio per capire meglio. Una spia parte per uno stato estero, e deve riferire periodicamente al suo capo. Come può fare a
mandare i suoi rapporti? Semplice: prima di partire la spia si fa dare dal suo capo 100 lucchetti aperti; quando dovrà mandare un messaggio
lo metterà in una solida scatola, chiuderà la scatola con un lucchetto e la spedirà per posta. Una volta chiusa, la scatola può essere solo
aperta dal capo, che è rimasto in patria con la chiave. Quali sono i punti salienti di questo sistema?

1.  Una volta che la spia ha chiuso la scatola, non la può riaprire nessuno se non chi ha la chiave, quindi nemmeno la spia stessa.
2.  Se la spia venisse arrestata, la polizia non sarebbe in grado di decrittare nulla, perché avrebbe in mano solo dei lucchetti.
3.  Non c'è nessuna necessità, per la spia, di nascondere i lucchetti, tanto servono solo a chiudere le scatole, non ad aprirle.

L'esempio parla di una spia, ma per crittare non è necessario avere qualcosa da nascondere. È fondamentale sfatare l'equazione crittare =
avere cose illegali da nascondere. E se ci pensi è un'equazione che sfati tutti i giorni, quando chiudi le tende, appunto, o quando eviti
che qualcuno sbirci il monitor mentre scrivi una mail.

Crittare significa semplicemente evitare che qualcuno (dall'impiegato del tuo provider, a un ufficio di marketing che controlla le parole
utilizzate nelle email per bilanciare le sue produzioni) legga quello che scrivi, le tue questioni personali.

Trasponendo questo esempio nel mondo digitale, chiamiamo il codice per crittare (il lucchetto) chiave pubblica, e il codice per decrittare
(la chiave) chiave privata.

Chi usa un sistema di crittazione asimmetrico, ovviamente, è dotato di due chiavi, una privata, che deve custodire con molta attenzione e
tenere assolutamente al sicuro, e una pubblica, che invece deve essere messa a disposizione di tutti, ad esempio su un apposito sito
internet.

<a name="3.3"></a>

[3.3. Che programma devo usare per crittare le mie mail?](#s3.3)

PGP (Pretty Good Privacy) è un software che ci consente di comunicare in forma assolutamente riservata anche tra persone che non si sono
mai viste di fatto e che vivono a decine di migliaia di chilometri di distanza l'una dall'altra. Tutto ciò è possibile grazie alla
crittografia a chiave pubblica.

Purtroppo le ultime versioni di PGP non possono essere considerate sicure, in quanto non è consentito all'utente di controllare il codice
del programma.

Questo è uno dei motivi per cui è nato GnuPG (Gnu Privacy Guard), un software in tutto e per tutto analogo a PGP, ma rilasciato sotto
licenza Gnu e di cui è verificabile il codice.

Per saperne di più su PGP, visita
 <https://it.wikipedia.org/wiki/PGP>

Per altre informazioni su GPG invece:

- <https://it.wikipedia.org/wiki/GNU_Privacy_Guard>
- <http://www.gnupg.org/>

<a name="3.3.1"></a>

[3.3.1. Ma per usare GPG che cosa devo fare? Molto semplice](#3.3.1)

Avendo installato Thunderbird ed Enigmail sul tuo computer, tutto quello che ti serve è creare un paio di chiavi. Per farlo segui le
istruzioni che trovi qui: <https://emailselfdefense.fsf.org/it/>

<a name="3.3.2"></a>

[3.3.2. Ma nella pratica come funziona?](#s3.3.2)

1.  Con il software di crittazione si crea una chiave di due parti – una pubblica e una privata. Alle persone con cui vuoi comunicare,
    trasmetti la parte pubblica della vostra chiave. Solo tu puoi usare la chiave privata.
2.  Quando scrivi un messaggio di posta elettronica, usi la chiave pubblica del destinatario per crittarlo.
3.  Il processo di crittazione inserisce una sorta di “lucchetto” elettronico nel tuo messaggio. Anche se questo è intercettato durante il
    suo tragitto, il suo contenuto è inaccessibile per mancanza della chiave.
4.  Quando il messaggio arriva, il destinatario inserisce una password (composta da una o più parole). Il software usa la chiave privata per
    verificare che per la crittazione sia stata usata la chiave pubblica del destinatario.
5.  Usando la chiave privata, il software sblocca la crittazione e consente di leggere il messaggio.

semplice no?

e qui trovi un manuale che, per quanto abbia la sua età e sia relativo a un particolare programma, nelle linee generali è estremamente
chiaro e valido:

 <http://www.ecn.org/crypto/crypto/guidapgp.htm>

Qua invece un manuale di respiro molto, molto, molto più ampio:

<http://www.ecn.org/zero/digguer.htm>

<a name="3.3.3"></a>

[3.3.3. Dove metto la mia chiave pubblica?](#s3.3.3)

Avrai capito che la diffusione della tua chiave pubblica è **essenziale** per poter utilizzare la crittazione asimmetrica: se nessuno ha la
tua chiave pubblica, nessuno potrà crittare messaggi diretti a te.

Puoi caricare la tua chiave pubblica sui keyserver direttamente con Enigmail, cliccando con il tasto destro sulla tua chiave nella finestra della gestione delle chiavi. Ti raccomandiamo di usare keyserver di default: hkps://hkps.pool.sks-keyservers.net.
<a name="3.4"></a>

[3.4. Ricapitolando](#s3.4)

Utilizza un software di crittografia per scrivere i messaggi personali e configura il tuo programma di posta affinché riceva e invii posta
in modo sicuro (SMTP con supporto SSL, POP3s, IMAP su SSL).

Per approfondimenti ti segnaliamo:

- <http://www.gnupg.org>
- [Un vecchissimo tutorial per Enigmail in italiano](https://www.autistici.org/loa/web/doc/enigmail/)
- [Un recente tutorial per Enigmail in italiano](https://emailselfdefense.fsf.org/it/)
- (In inglese) Security in a Box - Guida alla configurazione di Thunderbird ed Enigmail - [Linux](https://securityinabox.org/en/guide/thunderbird/linux/) - [Windows](https://securityinabox.org/en/guide/thunderbird/windows/) - [Mac](https://securityinabox.org/en/guide/thunderbird/mac/)

<a name="4"></a>

[4. Per i più paranoici: anonymous remailer](#s4)

La crittografia ha però un problema, e cioè che per divulgare la propria chiave pubblica bisogna pubblicarla su un server, e il solo fatto
che l'indirizzo sia collegabile a un nome mina alla base il concetto di privacy. Per questo motivo è stata creata una serie di strumenti
conosciuti come anonymous remailer in grado di celare il mittente di una mail.

Un buon riferimento a questo proposito è di nuovo [Kriptonite](http://www.ecn.org/kriptonite), al capitolo 5. In generale gli anonymous
remailer sono server che funzionano da intermediari tra il mittente e il destinatario nella fase di consegna della mail, per cui il reale
mittente viene sostituito dall’intermediario. Usando in catena più intermediari, si realizza un livello di sicurezza accettabile e si può
affermare che la propria mail è stata spedita in maniera anonima.

Un utilizzo intelligente implica però la fusione tra anonymous remailer. Per raggiungere un buon livello di sicurezza è infatti necessario
prima cifrare il messaggio e quindi spedirlo attraverso una catena di remailer. Esistono diversi client grafici per l’uso di remailer.

Per un elenco conviene consultare [http://www.autistici.org/crypto](http://www.autistici.org/crypto/index.php/remository)

I server e i client di mail possiedono ormai quasi tutti il supporto per il protocollo SSL o per TLS. Entrambi aggiungono un livello
crittografico utile ad esempio a non far passare la propria password in chiaro per Internet. È piuttosto semplice abilitarli dal lato
client, non è però detto che il proprio provider li supporti.

Per approfondire sugli anonymous remailer, vai a questa [pagina tecnica](/docs/anon/remailer).

Il caso già citato, quando, prendendo a utile pretesto un'inchiesta per terrorismo, la polizia postale ha
sequestrato a nostra insaputa il contenuto dell'intero server facendosi aiutare dal provider che lo ospitava, dimostra una volta di più che
non è sicuro delegare a nessuno la propria privacy, e che la riservatezza che abbiamo in rete non è poi tanto diversa da quella che possiamo
avere camminando per le strade di questo mondo veramente poco libero. 
