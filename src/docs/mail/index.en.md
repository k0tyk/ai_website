title: Mail services documentation
----

# Documentation for users of the A/I mail service

* [Connection parameters for e-mail clients](/docs/mail/connectionparms)
* [Technical notes about the service](/docs/mail/tech_mail)
* [Two-factor authentication](/docs/2FA "Two-factor authentication")
* [Protecting your privacy](/docs/mail/privacymail)
* [How to create a safe password](/docs/mail/passwd_safe "How to create a safe password")
* [How to change or recover your account password](/docs/mail/passwd "How to change or recover your account password")
* [Roundcube A/I webmail howto](/docs/mail/roundcube "Roundcube A/I webmail howto")
* [Howto setup webmail filters](/docs/mail/sieve "Howto setup webmail filters")

## Instructions on how to set up various e-mail clients

* [Evolution](/docs/mail/evolution)
* [Fetchmail](/docs/mail/fetchmail)
* [iPhone](/docs/mail/iphone)
* [K9 Mail](/docs/mail/k9mail)
* [Apple Mail (macOS)](/docs/mail/mail-osx)
* [Mutt](/docs/mail/mutt-msmtp)
* [Outlook](/docs/mail/outlook)
* [Sylpheed](/docs/mail/sylpheed)
* [Postfix](/docs/mail/postfix)
* [Thunderbird](/docs/mail/thunderbird)

