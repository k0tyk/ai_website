title: Protecting your privacy: how and why
----

Protecting your privacy: how and why
====================================

Summary
------------------

 <a name="s1"></a>[1. About privacy](#1)

 <a name="s1.1"></a>[1.1. Why should I encrypt my mail?](#1.1)

 <a name="s1.1.1"></a>[1.1.1. Non-personal secret: negotiations, finance, justice](#1.1.1)

 <a name="s1.1.2"></a>[1.1.2. Personal secrets: private life and feelings](#1.1.2)

 <a name="s2"></a>[2. All risks suffered by YOUR mailbox](#2)

 <a name="s2.1"></a>[2.1. The route of your e-mail messages](#2.1)

 <a name="s2.1.1"></a>[2.1.1. The countless copies of your e-mail messages](#2.1.1)

 <a name="s2.2"></a>[2.2. The risk is subdivided between two levels](#2.2)

 <a name="s3"></a>[3. Technical tips and encryption](#3)

 <a name="s3.1"></a>[3.1. For a start: encrypting is much easier than you can think
](#3.1)
 <a name="s3.2"></a>[3.2. How does encryption work?
](#3.2)
 <a name="s3.3"></a>[3.3. What application should I use to encrypt my mail?
](#3.3)
 <a name="s4"></a>[4. For the most paranoid ones: anonymous remailer](#4)



<a name="1"></a>

### [1.1. About privacy](#s1)

Police enquiries mention more and more often sentences uttered by the accused as regards the possibility of encoding their letters, and
these reflections are considered as an explicit suggestion of their will to carry out illegal acts. With the same frequency, police files
contain passages of tapped e-mail messages

From the [NSA case](https://www.eff.org/nsa-spying) through the [several inquiries](/who/collective) A/I had to face, it
is a well-known fact that our lives and communications are monitored.

We are striding towards mass control, when privacy will collapse for the sake of law and order, among suspicion paradigms and the fake
terror issue.

In a situation where control is always forced onto us as the solution to all evils, we'd like to cast some light into the dangers implied by
e-mail communication and into the countermeasures you can use

<a name="1.1"></a>

### [1.1. Why should I encrypt my mail?](#s1.1)

If you don't know what encode, encrypt, etc. mean, have a look at this page:
[https://en.wikipedia.org/wiki/Cryptography](https://it.wikipedia.org/wiki/Crittografia).

A non encrypted e-mail message sent through the Internet is like a post card without an envelope: postpeople, doorkeepers, neighbours and
anybody else who can put their hands on it will easily read the message you've written

We'll never get tired of repeating that using cryptography does not only protect yours, but also your addresse's privacy.

<a name="1.1.1"></a>

### [1.1.1. Non-personal secret: negotiations, finance, justice](#s1.1.1)

Journalists, lawyers, physicians, accountants: there are many occupations which bind by contract, by ethics or by law to keep professional
secrecy. More and more people use the Internet for work reasons, and those who must protect their clients' secrets are obliged to encrypt
their e-mail if they don't want their commercial proposals, law files and case histories to get lost in the Web red tape.

If they don't encode their documents, they will be neglecting the necessary measures aimed at keeping professional secrecy and they will be
risking significant legal and financial consequences.


<a name="1.1.2"></a>

### [1.1.4. Personal secrets: private life and feelings](#s1.1.2)

You don't encrypt your mail because you have nothing to hide? Very well, but how come do you close the curtains at home then?

You certainly wouldn't like some stranger sitting at the desktop of your Internet provider to grin while reading, for leisure, the messages
you're sending to your best friend. If you have never encrypted your e-mail, it is likely that some stranger may have read what you've
written...


<a name="2"></a>

[2. All risks suffered by YOUR mailbox](#s2)

<a name="2.1"></a>

### [2.1. The route of your e-mail messages](#s2.1)

Your e-mail is exposed to several risks: when you send a message, your mail client contacts a server through a so called SMTP protocol and
transmits your message to this server. This transmission can take place in plain text (without any encoding). The SMTP server
contacts in its turn the destination server, and this transmission can happen in plain text again.

What's more, every time you send an e-mail message the service provider's computers will record a copy of your letter. Let's see how it
works:

<a name="2.1.1"></a>

### [2.1.1. The countless copies of your e-mail messages](#s2.1.1)

Your e-mail travels along the Internet leaving behind a number of copies.

E-mail messages move through the Web with a sequence of copies recorded by providers (e.g. your ISP's -- Internet service provider) to reach their recipients' mail server.

If for example you send a mail from your home in the North of Rome to a person living in the South of Rome, your mail will be copied at
least 3 times:

1.  Your computer (**original copy**) sends your message to a first computer at your provider's location (**1st copy**);
2.  in the best hypothesis, the provider's computer sends its copy directly to your recipient's provider's location (**2nd copy**). But there could be more steps;
3.  your recipient's ISP's computer keeps a copy of your mail (**3rd copy**), while waiting for someone to download her mail (**end copy**).

So, just to travel through some neighbourhoods, your mail has been recorded at least 3 times in 2 different hard disks (2 ISP mail servers) and
every time in a perfect copy. And behind these hard disks hide commercial firms, curious IT technicians, all sorts of officials and many
more... Besides, these three copies are the best hypothesis: if you give a look at the headers of your incoming mail (every client has an
option to do this), you'll se that the steps are many more, as many as the copies of your messages

Theoretically, these multiple copies of your mail should be erased within few hours by each ISP. But the new laws which are being passed
worldwide against "cybercrime" provide for the retention of all copies for several months, at least as regards the parts signalling the
sender's and the addressee's metadata.

<a name="2.2"></a>

### [2.2. The risk is subdivided between two levels](#s2.2)


First of all, during its various movements along the Web, a message can be tapped and read. Second, the copied messages in the servers can
be read by anyone who can access these computers.

<a name="3"></a>

[3. Technical tips and encryption](#s3)

As regards the first problem, you can use encoded communication channels to send your messages to your servers (by using SMTP with SSL
support and POP3 or IMAP with SSL).

Our howtos include [some pages](/services/mail#howto) that explain how to configure your email client so as to use SSL/TLS on Autistici/Inventati.

Our servers offer TLS/SSL, but other providers don't. Check if your recipient's mail provider encrypts connections by going to [this website](https://www.checktls.com/TestReceiver) and entering in the "eMail Target" field the domain of your recipient's provider (the string after @ in the email address).

As for the second problem, it can be solved by using a program for e-mail content encryption. But be careful: you should absolutely use an
**open-source program**, otherwise you can't be sure that it does what it claims. Encryption programs can actually suffer several attacks:
someone could want to create a backdoor allowing them to read an encoded e-mail message anyway. If the program you're using is not
open-source, that is if the source code is not available and cannnot be examined, then a programmer resolved to grant everybody the
possibility of protecting their privacy will not be able to assess the possible presence of backdoors.

<a name="3.1"></a>

#### [3.1. For a start: encrypting is much easier than you can think](#s3.1)

If you know how to manage a normal mail program (writing e-mail messages, inserting attachments, etc.), then you should not have problems in
managing encryption programs.

GPG, the open-source program we (and many others) recommend, is actually rather simple to be used, as it can be installed through
an ad-hoc plugin on several e-mail clients. In particular, we recommend coupling Thunderbird (mail client) with Enigmail (encryption
plugin), which are both available for every operating system.

At any rate, the degree of safety you'll get closely depends on the level of protection you can assure to your private key, that is the file
containing the cryptographic mechanisms trigger. If you lose your private key, security will only be delusive. If you want to know what a
private key is, read on.

<a name="3.2"></a>

### [3.2. How does encryption work?](#s3.2)

There are two different categories of encryption methods: symmetric and asymmetric.

A symmetric encryption method can, for instance, establish a correspondence between letters and respective numbers:

A ---&gt; 1

B ---&gt; 2

C ---&gt; 3

and so on.

The drawback of these system is that as soon as you discover how a message has been encoded, there will be no problems in deccoding it.

An asymmetric system is much more sophisticated. Encoding and decoding take place through two different mechanisms: their difference is
such, that the encryption mechanism can be made public.

Here's an example to better understand how this system works. A secret agent going abroad must periodically report with her bosses. How can
she send her reports? Easy: before leaving, the agent gets from her boss 100 open locks; when she needs to send a message, she puts it in a
solid box, closes the box with one of the locks and sends it by mail. Once it has been closed, the box can only be opened by the agent's
superiors, who has stayed at home with the necessary key. What are the fundamental aspects of this system?


1.  When the agent closes the box, nobody can open it without the respective key, not even the agent herself.
2.  If the agent gets caught, the police cannot decode anything, because they can only put their hands on the locks.
3.  There's no need for the agent to hide her locks, since they can only close boxes, but they can't possibly open them.

This example is about a secret agent, but encryption is recommendable regardless of your having something to hide. It is crucial to
demystify the equation encoding = having illegal things to hide. And if you think about it, this is an equation you deny every day, when you
draw your curtains, indeed, or when you avoid curious glances at your monitor while you're writing an e-mail.

Encoding just means avoiding that someone (from your provider's employees to the marketing firms monitoring words used in e-mail messages in
order to adjust their company's production) reads what you write, your private affairs.

Moving to the digital realm, we call public key the encryption code (the lock), and private key the decryption code (the key).

If you use an asymmetric encryption code, you'll have two keys, of course: a private key you have to protect very carefully and keep
absolutely safe, and a public key, which can be made available to anybody, on a suited website, for instance.

<a name="3.3"></a>

### [3.3. What application should I use to encrypt my mail?](#s3.3)


PGP (Pretty Good Privacy) is a software tool allowing for totally reserved communication even between persons who have never seen each other and
who live thousands kilometres apart. This is possible thanks to public key cryptography.

Unfortunately the latest PGP versions cannot be considered safe, in that users cannot check the program code.

This is one of the reasons why GnuPG (Gnu Privacy Guard) was created: GnuPG is a software tool very similar to PGP which is released under a Gnu
license, so that its code can be verified.

For more details about PGP, go to:

 <https://it.wikipedia.org/wiki/PGP>

For more information about GPG, go to:

<https://it.wikipedia.org/wiki/GNU_Privacy_Guard>


<http://www.gnupg.org/>


To learn how to install and configure Thunderbird and Enigmail, you can read the guides on Security in a Box for [Linux](https://securityinabox.org/en/guide/thunderbird/linux/), [Windows](https://securityinabox.org/en/guide/thunderbird/windows/), and [Mac](https://securityinabox.org/en/guide/thunderbird/mac/).


<a name="4"></a>

[4. For the most paranoid ones: anonymous remailer](#s4)

Cryptography does have a problem, though: since you have to publish your public key on a server in order to make it available, the mere fact
that your address can be connected to a name challenges the concept of privacy. That's why a series of tools known as anonymouse remailer
has been created: in fact these tools can totally hide the sender's name.

A good reference on the subject is [Andre' Bacard's FAQ](http://www.andrebacard.com/remail.html). Generally speaking, anonymous remailers
are servers functioning as mediators between senders and addressees at the mail delivery stage: thus the real sender is replaced by the
mediator. By passing through a chain of several mediators, you get a decent security degree and it can be argued that your mail has been
sent anonymously.

For a clever use of this tool, you should pass through many anonymous remailers. If you want to get a good security degree, you need to
encode your message first and then to send it through a chain of remailers. Remailers can be used through several clients.

A list of clients can be found here:
[http://www.autistici.org/crypto/](http://www.autistici.org/crypto/index.php/remository/Remailer-clients/)

SSL or TLS protocols support is embedded in almost every mail server and client, by now. Both protocols add a cryptographic level that
prevents your password, for example, from travelling in plain-text through the Internet. Enabling them in a client is rather easy, but not
all providers support them.

Our [mail howto section](/services/mail#howto) offers several manuals explaining how to configure a mail client so as to
use SSL/TLS with A/I, while our <u>[remailer page](/docs/anon/remailer)</u> offers more information about anonymous
remailers.

There's a lesson to be learned from the mentioned A/I case, when the postal police used the excuse of an enquiry on terrorism for
secretly seizing our server content, with the help of the hosting webfarm: this teaches us once more that it is unsafe to rely on other
people for one's individual privacy, and that privacy in the Internet is not much different from the privacy we get while walking in the
streets of this truly unfree world.
