title: XChat Howto
----

XChat Howto 
===========

Download XChat [here](http://www.xchat.org/download/) or, if you use Windows, [here](http://www.silverex.org/news/).

Run Xchat and click 'Server List' in the 'XChat' menu.

![](/static/img/man_irc/en/xchat1.png)

You'll be presented with a window containing a server list. Click 'Add' and replace 'New Server' with the name you prefer for your network,
such as mufhd0, the name of our network.

![](/static/img/man_irc/en/xchat2.png)

Click 'Edit': you'll see a new window where you can set the IRC network configuration parametres.

Click the 'Edit' button next to the server box and write ai.irc.mufhd0.net/9999 instead of Newserver/6667.

Tick the 'Use SSL for all servers' box and close the window.

![](/static/img/man_irc/en/xchat3.png)

You'll now see the Server List again and you'll just need to click 'Connect' and join your favourite channel (see command list).

<a name="TOR"></a>

If you prefer to anonymize your connection through Tor, install Tor and Privoxy as explained in
[these](https://www.torproject.org/docs/documentation.html.en) howtos, then choose 'Preferences' in the 'Settings' menu and click 'Network
settings' in the left-hand menu.

![](/static/img/man_irc/en/xchat4.png)

Enter 'localhost' in the Host Name box, select Port 9050 and choose 'Socks5' in the Type menu.

From now on your connection to IRC will be rather reliably anonymous.

If you don't want to connect anonymously, replace 'Socks5' with 'Disabled'.


