title: Wie man für das Projekt spendet 
----

Wie man für das Projekt spendet
===============================

Da unser Projekt **ausschließlich** über Mitgliederbeiträge finanziert wird, sind wir für jede kleine Geste sehr dankbar.  Auf dieser Seite
werden viele verschiedene Möglichkeiten vorgestellt, wie man Spenden kann, zögern Sie also nicht!  Wen interessiert, wie hoch die Kosten für
all das hier sind, der kann sich [hier](/who/costs) informieren.  Leider akzeptieren wir keine
Bitcoins. [Hier](https://cavallette.noblogs.org/2013/07/8333#eng) wird erklärt, warum.

- [Direkte Überweisung an die Banca Etica, die ethischen Bank](#bonifico)
- [Online-Spende](#carta) (you need a credit card)
- [Persönliche Übergabe](#life) (Wenn man jemanden aus dem Projekt kennt und ihm vertraut)

Einzelheiten zur Zahlung
------------------------

### Direkte Überweisung

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN ("International Bank Account Number" = Internationale Bankkontonummer): IT83P0501802800000016719213 
    SWIFT CODE/BIC ("banc identifier code", international standardisierter Bankcode): CCRTIT2T84A
    Banca Popolare Etica - Milan Agency
    V. Santa Tecla, 5
    Verwendungszweck ("XXXX" bitte durch das aktuelle Jahr ersetzen): donation to investici (year XXXX)

### Online-Spende (PayPal)

<a name="carta"></a>

Sie können mit einer Kreditkarte spenden. Für die Transaktion benutzen
wir PayPal (Sie brauchen kein spezielles Konto). Wir mögen PayPal
nicht wirklich, haben allerdings bisher noch keine gute Alternative
gefunden. Sollten Sie eine solche kennen, zögern Sie bitte nicht, uns
zu informieren.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Regelmäßige Spende (PayPal)

Auch eine regelmäßige Spende.  Wenn Sie sich sicher sind, ein
Spendenabonnement abzuschließen, können Sie das folgende Formular
benutzen. Jedes Jahr verschwinden so 25€ (oder eine Summe Ihrer Wahl)
in unseren Off-Shore-Konten ;) (wir spaßen natürlich nur).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>

### Online-Spende (Liberapay)

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Persönliche Übergabe

<a name="life"></a>

Wenn Sie in der Nähe einer Person wohnen, die zum Projekt gehört und
der Sie vertrauen, können Sie der Ihre Spende anvertrauen.  Dabei wäre
wichtig, dass Sie eventuell erst verschiedene Spenden (von Freunden
oder Gemeinschaften) einsammeln, damit die arme Seele nur eine
Überweisung tätigen muss.

