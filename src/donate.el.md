title: How to donate to the project 
----

Πως μπορείτε να συνεισφέρετε στο εγχείρημα
==========================================

Το εγχείρημα βασίζεται **ΑΠΟΚΛΕΙΣΤΙΚΑ** χάρη στην συμβολή των χρηστών του, γι' αυτό και είμαστε απόλυτα ευχαριστημένοι κι ευγνώμονες με
κάθε ποσό που μπορείτε να βοηθήσετε. Σ' αυτή την σελίδα θα βρείτε πολλούς τρόπους που μπορείτε να συνεισφέρετε. Ελάτε, μη ντρέπεστε!
Εάν θέλετε να μάθετε πόσο κοστίζει ο κόπος μας [ρίξτε μιά ματιά!](/who/costs). Συγνώμη, αλλά δεν μπορούμε να δεχτούμε Bitcoins.
[Σ' αυτήν την ανάρτηση](http://cavallette.noblogs.org/2013/07/8333#eng) εξηγούμε γιατί.

- [Άμεση μεταφορά στην Banca Etica](#bonifico)
- [On-line δωρεά](#carta) (μέσω Πιστωτικής Κάρτας)
- [Χέρι-χέρι](#life) (εάν γνωρίζετε κι εμπιστέυεστε κάποιον από το εγχείρημα)

Λεπτομέρειες
------------

### Άμεση μεταφορά

<a name="bonifico"></a>

    Associazione AI-ODV
    EU IBAN: IT83P0501802800000016719213
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Filiale di Milano
    V. Santa Tecla, 5
    Reason: donation to investici (year 202x)

### On-line δωρεά (PayPal)

<a name="carta"></a>

Μπορείτε να κάνετε δωρεά μέσω της πιστωτικής σας κάρτας. Για την συναλλαγή χρησιμοποιούμε PayPal (δεν θα χρειαστείτε κάποιον συγκεκριμένο
λογαριασμό). Δε γουστάρουμε ιδιαίτερα την PayPal αλλά μέχρι στιγμής δεν μπορέσαμε να βρούμε καμία καλύτερη εναλλακτική λύση. Εάν έχετε
οποιεσδήποτε προτάσεις, μπορείτε να μας ενημερώσετε. 

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Συστηματική δωρεά (PayPal)

Πρόκειται για συστηματική δωρεά. Εάν θέλετε να είστε σίγουροι ότι θα εγγραφείτε στο project ετήσιως, μπορείτε να συμπληρώσετε την παρακάτω
φόρμα. 25€ ετησίως (ή το άθροισμα της επιλογής σας) θα πιστωθεί στους off-shore τραπεζικούς λογαριασμούς μας ;) (αστειευόμαστε φυσικά).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>

### On-line δωρεά (Liberapay)

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Χέρι-χέρι

<a name="life"></a>

Εάν ζείτε κοντά σε κάποιον από το εγχείρημα που γνωρίζετε και φυσικά εμπιστεύεστε, μπορείτε να του δώσετε την δωρεά σας, χέρι-χέρι.
Είναι σημαντικό για 'μας να προσπαθήσετε να μαζέψετε διαφορετικές δωρεές (από συλλογικότητες, φίλους κι ομάδες) προτού δώσετε τα
χρήματα, έτσι ώστε ο φτωχός που θα τα λάβει να κάνει μία και μόνο συναλλαγή.
