title: Comment faire un don au projet 
----

Comment faire un don au projet
===============================

Notre projet vit **EXCLUSIVEMENT** grâce aux contributions personnelles, donc nous serons très contents et reconnaissants, quelle que soit
la somme que vous donnerez pour nous aider.
Sur cette page vous pouvez trouver de nombreuses façons de donner, ne soyez pas timides !
Si vous voulez savoir combien ça coûte de faire tourner tout ça, [regardez par ici !](/who/costs)
Nous n'acceptons pas les dons de bitcoin. Dans [cet article de blog](http://cavallette.noblogs.org/2013/07/8333) (en italien et en
anglais), nous expliquons pourquoi.

- [Virement direct à la Banque Ethique](#bonifico)
- [Don en ligne](#carta) (il faut une carte de crédit)
- [Vraie vie](#life) (si vous connaissez et faites confiance à quelqu'un du projet)

Détails
-------

### Virement direct

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN: IT83P0501802800000016719213 
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Milan Agency
    V. Santa Tecla, 5
    Raison: donation to investici (year 202x)
    
### Don en ligne (PayPal)

<a name="carta"></a>

Vous pouvez faire votre don avec une carte de crédit. Nous utilisons
Paypal pour la transaction (vous n'avez pas besoin de compte chez
eux).  Nous n'aimons pas trop Paypal, mais nous n'avons pas trouvé de
meilleure alternative. Si vous avez des suggestions, n'hésitez pas à
nous en faire part.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Hosting">Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Revolutions!">Revolutions!</option>
</select>
<input type="text" size="6" name="amount" value="25.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="hidden" name="cmd" value="_xclick"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Donazione per A/I Servers"></input>
<input type="hidden" name="item_number" value="10011"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="tax" value="0"></input>
<input type="hidden" name="lc" value="IT"></input>
<input type="image" src="/static/img/thirdparty/x-click-but11.gif" border="0" name="submit" alt="[Donate]"></input>
</form>

### Don répété (PayPal)

C'est un don répété.  Si vous voulez être sûr de contribuer à notre
projet tous les ans, vous pouvez utiliser le formulaire suivant.  Tous
les ans, 25€ (ou la somme de votre choix) seront crédités sur notre
compte en banque off-shore ;) (on plaisante bien sûr)

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per"></input>For&nbsp;</td>
<td align="left"><select name="os0">
<option value="E-mail">E-mail</option>
<option value="Mailing-List">Mailing-List</option>
<option value="Web-Hosting">Web-Hosting</option>
<option value="NoBlogs">NoBlogs</option>
<option value="Legal Support">Legal Support</option>
<option value="Revolution!">Revolution!</option>
</select>
<input type="text" size="6" name="a3" value="50.00"></input> &euro;</td>
</tr>
</tbody>
</table>
<input type="image" src="/static/img/thirdparty/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]"></input>
<input type="hidden" name="cmd" value="_xclick-subscriptions"></input>
<input type="hidden" name="business" value="donate@inventati.org"></input>
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad A/I"></input>
<input type="hidden" name="item_number" value="10012"></input>
<input type="hidden" name="no_shipping" value="1"></input>
<input type="hidden" name="no_note" value="1"></input>
<input type="hidden" name="currency_code" value="EUR"></input>
<input type="hidden" name="lc" value="IT">
<input type="hidden" name="p3" value="1"></input>
<input type="hidden" name="t3" value="Y"></input>
<input type="hidden" name="src" value="1"></input>
<input type="hidden" name="sra" value="1"></input>
</form>
    
### Don en ligne (Liberapay)

<a href="https://liberapay.com/autistici.org/donate"><img alt="Donate using Liberapay" src="/static/img/thirdparty/donate.svg"></a>

### Vraie vie

<a name="life"></a>

Si vous vivez non loin de quelqu'un du projet que vous connaissez et en qui vous avez confiance, vous pouvez lui donner votre don.  
Il est important que vous essayez de collecter différents don (de collectifs, amis ou équipes) avant de donner l'argent, ainsi la pauvre
âme qui le récoltera n'aura qu'une seule transaction à faire.

