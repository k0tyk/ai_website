title: Contact
----

Input
=====

If you need to get in contact with us, you can:

* Join the #ai channel on our [IRC server](https://www.autistici.org/docs/irc/).
* If you encounter technical problems, write to [helpdesk](mailto:help@autistici.org).
* For legal enquiries, write to [Associazione AI-ODV](mailto:associazione@ai-odv.org). 
* Other issues should be addressed to our [e-mail contact](mailto:info@autistici.org).

Output
======

We use these accounts to send out updates
(**but don't try get in contact with us here, publishing is automated and the accounts unattended**): 

* Our official [Blog](https://cavallette.noblogs.org/)
* Our one and only [official Twitter account](https://twitter.com/cavallette)
* Our [account on Mastodon.bida.im](https://mastodon.bida.im/@cavallette)
