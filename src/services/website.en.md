title: Website hosting
----

Web Site Hosting
================

A/I Collective gives you the possibility to publish your own (static) web site.

The [criteria](/who/policy) for participating and enjoying the services offered by our servers are sharing our
**principles of anti-fascism, anti-racism, anti-sexism, anti-militarism** that foster this project.
Your website must share the non-commercial approach we support.

Your (static) site will generally be visible as a **subdirectory** of autistici.org or inventati.org (e.g.: www.autistici.org/mysite or
www.inventati.org/yoursite) and you'll be given a login name and password to upload via [WebDAV](/docs/web/webdav).
your files.

If your prefer, you can host a [**domain**](/docs/web/domains) of your own choosing (but you will have to purchase
it from a commercial provider).

You will find a lot of useful information on how to run a website on A/I servers in our howtos and manuals: <a name="howto"></a>

- [How to host your own domain on A/I servers](/docs/web/domains "how to host your own domain on A/I servers")
- [How to use webdav to update and upload your site](/docs/web/webdav "how to use webdav to update and upload your site")
- [A very short reminder about webmasters' privacy and webusers' data](/docs/web/privacyweb "a very short reminder about webmasters' privacy and webusers' data")
- [Some very frequently asked questions on sites hosted on A/I](/docs/faq/)
- [How to activate statistics on your site](/docs/web/webstats "Howto activate statistics on your site")
- [Technical notes on A/I hosted websites](/docs/web/tech_web "technical notes on A/I hosted websites")
- [Howto add A/I banners to your A/I hosted website](/docs/web/banner "Howto add A/I banner on your website")

An email account on our servers has to be connected to each site for technical communications and news on the web hosting services. So, if
you don't have one, you will have to open a mailbox account as well as request website hosting.

**Very important note about server security**

We used to allow the installation of CMSs such as Drupal, b2evolution, Wordpress and the like, offering spaces for dynamic websites.
If you wonder about why we are not offering this option anymore, first of all ask yourself if it
wouldn't suit you best to [open a blog on our blogging platform Noblogs.org](/services/blog); we regularly maintain and update this software,
carefully checking at all times if new versions have come out. Furthermore we do some additional verification to ensure the maximum possible
level of security and privacy on this platform.

This is utterly important for your and every other user's security, since not updated software contains bugs which are regularly
exploited to access the server and make a mess.

Since you're not the only one using our servers, but you're sharing a space, we think it is absolutely necessary that everyone is
responsible for what they install in the servers, because our boxes are self-managed and based on shared participation and responsibility.
But this expectation has not been meet, therefore we don't provide hosting for dynamic websites anymore. We spent too much time mending the
suddenly detected holes left open in old, often abandoned installations of outdated CMS'es, that had to urgently be deactivated.

Now that you have read all about our static web hosting service, go on and **[ask us to open your website](/get_service)** but
remember that this project lives on exclusively thanks to our users **[donations!](/donate)**

Please be sure to check out our **[help page](/get_help)** and **the above mentioned manuals** for any problem you
might encounter (webdav access, web statistics, domain registration included especially).
