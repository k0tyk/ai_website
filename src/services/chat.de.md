title: Instant Messaging und Chat
----

Instant Messaging und Chat
==========================

Instant Messaging und Chaträume werden weltweit zunehmend stärker in Anspruch genommen, doch die verbreitesten Dienste sind kommerzieller
Natur und bieten wenig Schutz für die Privatsphäre der Nutzer.

Das IRC-Netzwerk und der Jabber Dienst von A/I erlauben kostenloses Chatting bei gleichzeitigem besseren Schutz der eigenen Anonymität.

<a name="irc"></a>

Irc - Chatdienst
----------------

A/I erlaubt seinen Nutzern die Möglichkeit zum Chatten via IRC (Internet Relay Chat), einem verbreiteten Chatprotokoll.
 IRC ist einfach zu bedienen viele Menschen verwenden es. Es ist ausserdem sehr schnell, was zu einer weiten Verbreitung unter den
elektronischen Kommunikationsmitteln geführt hat.

Unser IRC-Netzwerk heisst **mufhd0** und wird in Gemeinschaft von A/I, [ECN](http://www.ecn.org) und [indivia](http://indivia.net)
verwaltet. Sie können sich in das Netzwerk über einen IRC Client, z.B. [X-Chat](/docs/irc/xchat), oder [Irssi](/docs/irc/irssi) (Linux command line).
 Wir empfehlen X-Chat, weil es Unterstütztung für **SSL-Verschlüsselung** und Verbindung über [Tor](/docs/anon/tor)
anbietet.

Für weitere Informationen über IRC/-Befehle, wie man einen IRC-Client einrichtet und welche Kanäle verfügbar sind, besuchen Sie unsere
**[Howtos](/docs/irc/)**.

<a name="jabber"></a>

Jabber/XMPP - Instant Messaging
-------------------------------

Jabber ist ein modernes Kommunikationsmittel für Nutzer im Internet. Es ist ähnlich zu anderen IM-Protokollen wie ICQ, MSN oder Yahoo IM,
aber wesentlich flexibler konfigurierbar. (Google Talk basiert auf Jabber, ist aber beschränkt auf Google)

Vor allem bietet Jabber standardmäßig Unterstützung für SSL-Verbindungen an und kann auch den Kommunikationsinhalt
selbst verschlüsseln mit kryptographischen Schlüsseln, die die Nutzer selbst verwalten (z.B. GPG oder OTR).
 Wir empfehlen für diese Verwendungart [Pidgin zusammen mit dem OTR Plugin](/docs/jabber/pidgin).

Jeder unserer Mailbox-Accounts besitzt automatisch einen Jabber-Account mit demselben Usernamen: *user@domain.org*

Um Jabber zu benutzen, empfehlen wir einen Client in deinem Computer zu installieren un so einzurichten, daß deine Gespräche durch OTR verschlüsselt sind.
Um herauszufinden wie man einen Jabber Client einrichtet, welche Befehle es gibt und wie sie benutzt werden können, sowie alle weiteren
Informationen finden sich in unserem [Jabber Handbuch](/docs/jabber/).

