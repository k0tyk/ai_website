title: Boite aux lettres
----

** attention, cette page est obsolète, la version anglaise est plus à jour. **

Boite aux lettres
=================

[Avoir un boite aux lettres](/get_service) sur nos serveurs (si vous ne savez pas ce qu'est un courrier electronique,
allez [en bas de cette page!](#what "what is an email?")) est une façon de sauvegarder votre vie privee (et la notre), evitant ainsi de
donner des donnees personnelles ou des contacts aux multinationales ou \`a un quelconque fournisseur commercial. En meme temps, il est
necessaire de savoir utiliser quelques outils qui sont sur nos serveurs pour y arriver, comme le cryptage SSL des communications, par
exemple.

En outre, nous ne vous demanderons aucun code fiscal, ni aucun numero d'un quelconque document, ni votre prenom et nom de famille pour
activer votre boite aux lettres sur A/I.

Les boite aux lettres sur A/I n'ont aucune limite de taille mais nous esperons que vous aurez le bon sens de ne pas gaspiller des ressources
precieuses que vous partagez avec beaucoup d'autres utilisateurs (pensez à [faire un don](/donate "donate"))! Nous
recommandons fortement les utilisateurs à **telecharger** leur courrier electronique des que possible, afin de diminuer la quantite de
donnees personnelles presentes sur les serveurs.

Le service de boite aux lettres A/I vous offre une vaste gamme de domaines... [faites votre choix](/get_service)!

Et avec votre compte de boite aux lettres vous avez la possibilite de creer jusqu'à **cinq** autres adresses (comme des
pseudonymes de votre compte de boite aux lettres)!

Si vous avez des questions sur l'utilisation de notre service de boite aux lettres, veuillez consultez les manuels suivant :

- [Parametres de connexion](/docs/mail/connectionparms "connection parameters for A/I email users")
- [Notes techniques sur le service de boite aux lettres A/I](/docs/mail/tech_mail "technical notes on A/I e-mail service")
- [Authentification à deux facteurs](/docs/2FA "authentification à deux facteurs")
- [Regles de base tres simples pour proteger votre vie privee en utilisant les courriers electroniques](/docs/mail/privacymail "very simple basic rules to protect your privacy while using emails")
- [Comment changer ou recuperer le mot de passe de votre compte](/docs/mail/passwd "how to change or recover your password")
- [Comment utiliser la boite aux lettres distante Roundcube A/I (webmail)](/docs/mail/roundcube "howto use A/I webmail")
- [Comment configurer les filtres de la boite aux lettres distante (webmail)](/docs/mail/sieve "howto setup filters in the A/I webmail")
- [Comment configurer Evolution comme client de courrier pour A/I](/docs/mail/evolution "howto setup Evolution mail client for A/I")
- [Comment configurer Fetchmail comme client de courrier pour A/I](/docs/mail/fetchmail "howto setup Fetchmail mail client for A/I")
- [Comment configurer mutt+msmtp comme client de courrier pour A/I](/docs/mail/mutt-msmtp "howto setup muttmsmtp mail client for A/I")
- [Comment configurer Thunderbird comme client de courrier pour A/I](/docs/mail/thunderbird "howto setup Thunderbird mail client for A/I")
- [Comment configurer Sylpheed comme client de courrier pour A/I](/docs/mail/sylpheed "howto setup Sylpheed mail client for A/I")
- [Comment configurer Postfix pour traiter le courrier d'A/I](/docs/mail/postfix "howto setup Postfix to handle A/I mail")
- [Comment configurer Mail OSX comme client de courrier pour A/I](/docs/mail/mail-osx "howto setup Mail OSX mail client for A/I")
- [Comment configurer Outlook comme client de courrier pour A/I](/docs/mail/outlook "howto setup Outlook mail client for A/I")
- [Comment configurer un client de courrier pour A/I sur votre iPhone](/docs/mail/iphone "howto setup your AI/ mail account on your iPhone")
- [Comment configurer le client de courrier pour A/I sur Android avec K-9 Mail](/docs/mail/k9mail "howto setup your AI/ mail account on Android K-9 mail")

Pourquoi devrais-je creer un compte de boite aux lettres sur A/I ?
------------------------------------------------------------------

**Pour :**

- Parce que je pense que la lutte pour defendre la liberte d'expression et la communication ne peut etre deleguee à personne.
- Parce que je veux faire une petite difference dans ma vie quotidienne, me garder loin des societes multinationales. Et parce que pour
    quelques raisons que ce soit, (elles sont mauvaises, vous ne devriez jamais avoir confiance en personne, rappelez vous!) vous avez
    confiance en nous :)
- Parce que je prefere que ma boite aux lettres reste privee (et vous voulez utiliser SSL)
- Parce que je ne veux pas aider la publicite pour une grande entreprise quand je donne à quelqu'un mon adresse de courrier.
- Parce que j'en ai assez de ceux qui me creent un compte de courrier electronique gratuit et m'envoient ensuite des tonnes de bannieres
    et des offres promotionnelles.
- Parce que quand quelqu'un essaye de fermer des services A/I ou menace leur securité, le collectif A/I essayera de faire de son mieux
    pour l'eviter. Et plus nous sommes nombreux, plus cela sera interessant.
- Parce que j'aime vos domaines (bon, si c'est seulement pour cette raison, nous ne serons pas vraiment "enthousiastes" pour vous donner
    un compte ... essayez de comprendre nos motivations et qui nous sommes...)
- Parce que je ne veux pas que mon activite sur Internet soit espionnee, copiee et revendue \`a l'entreprise qui fait la meilleure offre.
- Parce que plus nous sommes nombreux, plus nous pouvons nous faire entendre.
- Parce que vous repondrez à mon courrier electronique, tot ou tard ... bon, au moins vous essaierez!

**Contre :**

- Parce que ce n'est pas un service gratuit (comme la biere gratuite) mais un service fonctionnant seulement si tout le monde y contribue
    selon ses possibilites chaque annee.
- Parce que quand ils attaquent la liberte de parole et de communication je serai le premier à le savoir... ou presque le premier...
- Parce que nos serveurs ne sont pas 100 % securises et que la sauvegarde de ma vie privee depend finalement de moi.
- Parce que j'ai d'autres ressources Internet eprouvees que je peux utiliser.
- Parce que je n'ai rien à cacher.
- Parce qu'il me semble que Gmail marche mieux.

A/I comparé à Gmail : avantages et inconvenients si vous nous choisissez
------------------------------------------------------------------------

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">A/I</td>
<td align="left">G-mail</td>
</tr>
<tr class="even">
<td align="left">Espace disponible</td>
<td align="left">Pas de limites, mais s'il vous plait, TELECHARGEZ vos messages!</td>
<td align="left">Pas de limites : ils gardent absolument tout (selon leur bon vouloir).</td>
</tr>
<tr class="odd">
<td align="left">POP3 et POP3-SSL</td>
<td align="left">OUI (certificats letsencrypt)</td>
<td align="left">OUI (certificats Google INC)</td>
</tr>
<tr class="even">
<td align="left">IMAP et IMAP-SSL</td>
<td align="left">OUI (certificats letsencrypt)</td>
<td align="left">OUI (certificats Google INC)</td>
</tr>
<tr class="odd">
<td align="left">SMTP TLS</td>
<td align="left">OUI (certificats letsencrypt)</td>
<td align="left">OUI (certificats Google INC)</td>
</tr>
<tr class="even">
<td align="left">Analyse du contenu de vos messages</td>
<td align="left">NON, jamais.</td>
<td align="left">OUI, pour de la publicite ciblée.</td>
</tr>
<tr class="odd">
<td align="left">Collecte de vos adresses IP de connexion</td>
<td align="left">NON, jamais</td>
<td align="left">OUI, toujours.</td>
</tr>
<tr class="even">
<td align="left">SSL Webmail</td>
<td align="left">OUI (certificats letsencrypt)</td>
<td align="left">OUI (certificats Google INC)</td>
</tr>
<tr class="odd">
<td align="left">Bannieres dans le webmail</td>
<td align="left">NON</td>
<td align="left">OUI (en format texte)</td>
</tr>
</tbody>
</table>

Qu'est ce que veut dire "avoir un compte de courrier electronique" ?
--------------------------------------------------------------------

Pour ceux qui ne savent vraiment pas ce qu'est un courrier electronique, nous serons heureux de vous eclairer.

Un compte de courrier electronique est exactement comme avoir un facteur dans votre ordinateur : vous ecrivez une lettre, vous l'envoyez à
un destinataire et le courrier est livre par une serie de serveurs (un serveur est un ordinateur gerant un service) entre l'endroit ou vous
etes et la destination du courrier.

Avoir un compte de courrier electronique necessite d'avoir un nom et une adresse ou les gens peuvent vous envoyer des courriers.

Il y a deux fac,ons principales de visualiser vos messages electroniques : vous pouvez les lire sur Internet (par Firefox, Netscape,
Konqueror, Galeon, Explorer, Opera, etc.) ou vous pouvez les telecharger et les envoyer par un logiciel sur votre ordinateur (un logiciel
comme Thunderbird, Netscape Messenger, Kmail, Pegasus Mail, Outlook Express, Eudora ou tout autre "client de courrier electronique" (client
mail) capable de "communiquer" avec votre serveur de courrier electronique.

Bien sur, en dehors de lire des messages electroniques, vous pouvez aussi les envoyer, soit toujours à l'aide d'un navigateur ou par
l'utilisation d'un client de courrier comme explique dans le paragraphe precedent. Pour envoyer votre courrier, vous utilisez un service
appelé le SMTP, conçu pour prendre vos lettres et les livrer à votre destinataire, ou qu'il soit.

Vous devez savoir que normalement toutes ces communications ne sont pas cryptees en chemin et elles peuvent etre facilement lues à n'importe
quelle etape(via des serveurs) de leur parcours sur Internet.
 Il est probable que si quelqu'un voulait vous controler, il lise vos messages comme s'ils etaient des prospectus laisses sur le trottoir
... Donc, il serait plus correct de comparer un message electronique avec une carte postale plutot qu'avec une lettre fermee dans une
enveloppe.

Pour limiter ces abus (et ne pas exclure n'importe quelle forme de controle, mais la limiter), vous pouvez choisir d'envoyer et telecharger
votre courrier electronique via SSL, c'est-à-dire crypté (code afin de ne pas etre facilement lisible).
