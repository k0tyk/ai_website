title: Noblogs: blogs sin logs 
----

Noblogs: blogs sin logs
========================

El [proyecto NoBlogs](http://noblogs.org/) nace de la voluntad de mejorar y mantener actualizados los [servicios de A/I](/services/)
con las herramientas de comunicación más utilizadas en los últimos años. Nos dimos cuenta de que mucha
gente no necesita realmente un sitio web, **sino que la mayoría busca un sitio donde pueda comunicarse simple y directamente**.

Puedes usar nuestro blog como prefieras, **siempre que sigas la** [política general](/who/policy) de nuestros servicios,
así como la [política específica](http://noblogs.org/policy/ "NoBlogs Policy") de nuestra plataforma de blogs.
 En realidad, no esperamos que el servicio se utilice para escribir diarios personales (aunque, por supuesto, puedes hacerlo si quieres),
sino para generar contenidos interesantes y que contribuyan a un espacio de **información, comunicación, relación e iniciativa política
independientes**.

**La libertad de expresión requiere prestar atención a la privacidad y seguridad**. Nosotr\*s hacemos lo mejor que
podemos, utilizando exclusivamente software libre, no guardando logs ni ningún tipo de información personal, y sugiriendo links útiles sobre
blogs y privacidad...pero esto no es suficiente si **tú no lo haces lo mejor que puedes también**, leyendo las [guías que te
sugerimos](/docs/blog/) y tomando todas las precauciones para asegurar tu privacidad. Te recordamos algunas

- Deja de usar gmail, hotmail o cualquier otro servicio comercial para intercambiar información importante. **Las cuentas de correo de
  estos proveedores no serán aceptadas para [solicitar un blog](http://noblogs.org/register)**.
  En su lugar, utiliza una cuenta en nuestros servicios o en cualquier servidor o colectivo no
  comercial (puedes ver una lista [aquí](/links)).
- Recuerda que muchos servicios alrededor del mundo guardan copias de cualquier comunicación no encriptada.
- No utilices software privativo, porque no puedes saber cómo funciona internamente, y si internamente han dispuesto funciones para poner
    en peligro tu privacidad.
- Presta la máxima atención cuando envíes digitalmente nombres, apellidos, números de teléfono, direcciones y otro tipo de información
    personal
- Los software de analíticas web ponen en riesgo la privacidad de las personas que navegan por nuestros blogs, es por ello que usamos
    nuestra propia [plataforma de analíticas web](/services/piwik "Matomo web analitics for A/I"): sí, es menos
    "informativa" que google analitics, pero mantiene a nuestr\*s visitantes a salvo de ojos indiscretos.

**Ahora, puedes ir a [Noblogs.org](https://noblogs.org/register/) y abrir tu blog**.

**Muy importante: Sobre la seguridad del servicio**. Si lo que buscas es simplemente publicar contenido. Te recomendamos activamente que te
inclines por un blog en nuestra plataforma Noblogs en lugar de un sitio web con un software como Wordpress. Ello porque el software **debe**
ser actualizado regularmente, mientras que en Noblogs no necesitas hacer esto :)
