title: Videoconferenze e streaming
----

Videoconferenze e streaming
===============================

Dalla primavera del 2020 Autistici/Inventati offre un servizio di videoconferenze e uno di streaming.

<a name="VC"></a>

Videoconferenze: Meet Jitsi
----------------------

A/I offre la possibilità di organizzare audio e videoconferenze sulla piattaforma [vc.autistici.org](https://vc.autistici.org), basata sul software open source [Jitsi Meet](https://github.com/jitsi).

Jitsi Meet permette di avviare una videoconferenza attraverso un semplice browser, senza bisogno di installare niente nel computer: vi basterà aprire il sito [vc.autistici.org](https://vc.autistici.org) in un browser (preferibilmente [Chromium](https://www.chromium.org/getting-involved/download-chromium)) e cliccare sul link "Go" per avviare una videoconferenza. Dopodiché potrete condividere l'url con le persone con cui volete parlare.

Per istruzioni dettagliate su come usare il servizio di videoconferenze di Autistici/Inventati, vai alla [guida](/docs/vc).

<a name="streaming"></a>

Streaming
--------------------------

Il nostro servizio di streaming si trova su [live.autistici.org](https://live.autistici.org).

Nella home page trovate istruzioni base sul suo funzionamento, mentre potete scoprire quali stream stanno funzionando in [questa pagina](https://live.autistici.org/stats).

Puoi trovare una guida di base su come avviare uno streaming sul servizio di Autistici/Intentati in [questa guida](/docs/stream-obs).
