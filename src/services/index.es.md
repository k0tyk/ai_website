title: Servicios
----

Servicios
=========

A/I ofrece herramientas para comunicarse y compartir de forma anónima. Recomendamos el uso de nuestros servicios junto a algunas
[herramientas personales de protección de la privacidad](/docs/mail/privacymail "protect your privacy").

Todos nuestros servicios se caracterizan por una atención particular hacia la privacidad de nuestr\*s usuari
\*s. Por ello, **no conservamos logs** de las conexiones ni mantenemos ninguna información que pueda relacionar
directamente un servicio, identidades o usuari\*s. También invitamos a tod\*s a utilizar
los sistemas de encriptación que ponemos a disposición (¡recuerda esto cuando configures tu cliente de correo o actualices tu sitio!).

Para utilizar los servicios que ofrece nuestra red, tienes que compartir los principios básicos de **anti-fascismo, anti-racismo,
anti-sexismo, anti-militarismo y el espíritu no comercial que anima este proyecto**, y por supuesto una buena dosis de voluntad de compartir
y relacionarse. Pero esperamos que si has llegado hasta este punto, todo esto ya lo supieras;)))))

Ahora que ya has leído todo esto, [regístrate y solicita un servicio](/get_service "regístrate y solicita un servicio")

## ¿Que servicios provee A/I?

Proveemos una amplia gama de instrumentos para comunicarse:

- [Email](/services/mail "Email on A/I")
- [Blogs](/services/blog "Blog on Noblogs.org")
- [Listas de Correo y Newsletters](/services/lists "Mailing lists on A/I")
- [Alojamiento Web](/services/website "Website hosting on A/I")
- [Mensajería instantánea y Chat](/services/chat "Instant Messaging and Chat on A/I")
- [Servicios de anonimización](/services/anon "Anonymity services on A/I")

## Costes

Los servicios que ofrecemos no tienen un precio definido, pero como es fácil de intuir, mantener toda la infraestructura funcionando (p.e.
hardware, software y conectividad) es [muy caro](/who/costs) (actualmente, gira entorno a los 13.000 euros al año).

Es por tanto fundamental, para que nuestros servicios sigan en línea y el proyecto sobreviva, que cada persona/grupo/proyecto que usa un
servicio ofrecido por la red haga una donación anual, de acuerdo a sus posibilidades.

La cantidad de dinero a donar puede variar, desde unos cuantos euros por una cuenta de correo, a 25-50 euros por un sitio web ordinario, a
donar un poco más en el caso de grupos o colectivos que usan cuentas de correo electrónico y listas de correos (pueden organizar una cena
para recaudar fondos de vez en cuando) o simplemente personas con capacidad y voluntad de contribuir con un poco más.

En cualquier caso, no nos metemos en el bolsillo de nadie, pero sí esperamos un poco de comprensión (también porque si no hay donaciones,
cerramos la choza!) y buena voluntad.

Piensa en esto, y [contribuye](/donate "donate") al proyecto.
