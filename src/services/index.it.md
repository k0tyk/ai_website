title: Servizi offerti
----

Servizi offerti
===============

Autistici/Inventati offre strumenti per comunicare e condividere in modo anonimo. Consigliamo l'utilizzo dei nostri servizi in abbinamento
con qualche [strumento per la difesa della privacy personale](/docs/mail/privacymail "proteggi la tua privacy").

Tutti i nostri servizi sono caratterizzati da un'attenzione particolare alla privacy degli utilizzatori. Per questo **non manteniamo log**
delle connessioni né manteniamo una correlazione diretta tra nomi utenti e utilizzatori. Inoltre invitiamo tutti a utilizzare i sistemi di
crittazione che mettiamo a disposizione (ricordatelo quando configurate la posta o aggiornate il sito web!).

Per poter usufruire dei servizi offerti dal network, bisogna condividere i principi di **anti-fascismo, anti-razzismo, anti-sessismo,
anti-militarismo e non commercialità** che animano questo progetto, oltre ovviamente a una buona dose di volontà di condivisione e di
relazione. Ma speriamo che se siete arrivati a leggere fino a qui, questo già lo sapeste ;)))))

Ora che avete letto tutto, potete [Creare il vostro account e chiedere un servizio](/get_service "crea un account")

## Quali servizi fornisce A/I?

A/I offre un'ampia gamma di strumenti per comunicare:

- [E-mail](/services/mail "E-mail")
- [Blog](/services/blog "Blog su Noblogs.org")
- [Mailing list e Newsletter](/services/lists "Mailing list")
- [Siti web](/services/website "Siti web")
- [Instant Messaging e chat](/services/chat "Instant Messaging e chat")
- [Videoconferenze e streaming](/services/videoconference-streaming "Videoconferenze e streaming")
- [Servizi per l'anonimato](/services/anon "Anonimato")

## Costi

I servizi che offriamo non hanno un prezzo definito però, come è facile intuire, mantenere l'intera infrastruttura funzionante (hardware e
connettività) ha [un costo significativo](/who/costs) (che si aggira al momento intorno ai 13.000 euro l'anno)

È quindi fondamentale, affinché i servizi restino online e il progetto sopravviva, che ogni persona/gruppo/progetto che utilizza un servizio
offerto dal network effettui una donazione annuale, secondo le proprie possibilità. Detto questo non accettiamo nessun nuovo utente in quanto "pagante":
i nostri servizi non sono in vendita, e destinati in modo esclusivo a chi dimostra affinita' coi nostri principi ispiratori. 

La donazione si può indicare con delle cifre che vanno dai pochi euri per una casella email, dai 25 ai 50 euri per un sito semplice, fino a
donazioni un poco più significative in caso di gruppi o collettivi organizzati che utilizzano anche caselle e-mail e mailing-list (che
possono organizzare una cena ogni tanto) o semplicemente di persone che possono e vogliono contribuire di più.

In ogni caso non facciamo i conti in tasca a nessuno, ma ci aspettiamo un po' di comprensione (anche perché se non ci saranno donazioni,
chiudiamo baracca e burattini) e buona volontà.

Pensateci sopra e date il vostro [Contributo](/donate "donate") al progetto.
