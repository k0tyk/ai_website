title: Serveis
----

Serveis
=======

A/I ofereix eines per comunicar-se i compartir de forma anònima. Recomanem l'ús dels nostres serveis al costat
d'[algunes eines personals de protecció de la privadesa](/docs/mail/privacymail "protect your privacy").

Tots els nostres serveis es caracteritzen per una atenció particular cap a la privadesa dels nuestr\*s usuari\*s. Per això, **no conservem
logs** de les connexions ni mantenim cap informació que puga relacionar directament un servei, identitats o usuari\*s. També convidem a
tot\*s a utilitzar els sistemes d'encriptació que posem a disposició (recorda açò quan configuris el teu client de correu o actualitzis el
teu lloc!).

Per utilitzar els serveis que ofereix la nostra xarxa, has de compartir els principis bàsics de **anti-feixisme, anti-racisme, anti-sexisme,
anti-militarisme i l'esperit no comercial** que anima aquest projecte, i per descomptat una bona dosi de voluntat de compartir i
relacionar-se. Però esperem que si has arribat fins a aquest punt, tot això ja ho sabessis;)))))

Ara que ja has llegit tot açò, [registra't i sol·licita un servei](/get_service "sign in for a service")

## Que serveis proveeix A/I?

Proveïm una àmplia gamma d'instruments per comunicar-se:

- [Email](/services/mail "Email on A/I")
- [Blocs](/services/blog "Blog on Noblogs.org")
- [Llestes de Correu i Newsletters](/services/lists "Mailing lists on A/I")
- [Allotjament Web](/services/website "Website hosting on A/I")
- [Missatgeria instantània i Xat](/services/chat "Instant Messaging and Chat on A/I")
- [Serveis d'anonimització](/services/anon "Anonymity services on A/I")

## Costos

Els serveis que oferim no tenen un preu definit, però com és fàcil d'intuir, mantenir tota la infraestructura funcionant (p.e. hardware,
software i connectivitat) és [molt car](/who/costs) (actualment, gira entorn als 13.000 euros a l'any).

És per tant fonamental, perquè els nostres serveis segueixin en línia i el projecte sobrevisqui, que cada persona/grup/projecte que usa un
servei oferit per la xarxa faci una donació anual, d'acord a les seves possibilitats.

La quantitat de diners a donar pot variar, des d'uns quants euros per un compte de correu, a 25-50 euros per un lloc web ordinari, a donar
una mica més al cas de grups o col·lectius que usen comptes de correu electrònic i llistes de correus (poden organitzar un sopar per
recaptar fons de tant en tant) o simplement persones amb capacitat i voluntat de contribuir amb una mica més.

En qualsevol cas, no ens fiquem en la butxaca de ningú, però sí esperem una mica de comprensió (també perquè si no hi ha donacions, tanquem
la barraca!) i bona voluntat.

Pensa en això, i [contribueix](/donate "donate") al projecte.
