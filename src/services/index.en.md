title: Services
----

Services
========

A/I provides [services and tools to achieve anonymous and secure communication](/docs/mail/privacymail "protect your privacy").

All our services are characterized by a peculiar attention to the user privacy. That's why **we keep no logs** of connections and we don't
record any information about identities and usernames. Furthermore, we enforce the use of the encryption on all the tools we offer.

## Which services do A/I provide?

We provide a suite of communication tools:

- [Email](/services/mail)
- [Blogs](/services/blog)
- [Mailing lists and Newsletters](/services/lists)
- [Web Hosting](/services/website)
- [Instant Messaging and Chat](/services/chat)
- [Videoconference and Streaming](/services/videoconference-streaming "Videoconference and streaming")
- [Anonymity Services](/services/anon)

## Costs

The services we offer have no defined price, but as you may easily imagine, keeping the whole system working (i.e. hardware and hosting) is
[very expensive](/who/costs) (at the moment it ranges around 13,000 euro every year).

In order to keep services online and our project to survive, would be very beneficial if every person/group/project using the
services offered by this network had given us a yearly donation, according to their possibilities. This obviously does not mean that we require or request donations 
in a "transactional" fashion: our services are not for sale, and a donation is neither mandatory or sufficient to obtain an account. However, if you
think that you deserve the support of our services on the base of your affinity to our inspiring principles, you probably also understand very well how
your support is vital to keep us doing what we do. Trust and support only exist when they reciprocate.

The amount of your donation may vary from a few euros for a mailbox to 25-50 euros for an ordinary website, up to greater donations if you are
a group or a collective using mailboxes and mailing lists or if you are simply a person able and willing to contribute with a more meaningful sum.
Some of our most loyal users have also organized fundraising parties to donate to us. That is probably the most flattering way to give us your support. 

In any case, we will never pry into your economic affairs, but we do expect some understanding (also because if we don't get any money,
we'll be forced to shut down the facilities).

Please, think about it, and [Donate](/donate "donate")
