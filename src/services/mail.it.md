title: E-mail
----

E-mail
======

*se non sai cosa sia un'e-mail o una casella di posta elettronica [dai un occhio in fondo a questa pagina!](#what "cos'è un'email?")*

[Avere una casella di posta](/get_service) sui nostri server significa poter esercitare una maggior tutela della propria privacy,
evitando di regalare dati personali e relazionali a multinazionali e provider commerciali.

Inoltre noi non vi chiederemo né un codice fiscale, né un numero di documento e neppure il vostro nome e cognome per attivare una casella di
posta.

Le mailbox su A/I hanno dei limiti di spazio dettati dal buon senso nell'uso di risorse preziose e condivise (e
[autofinanziate](/donate))! Per questo, è altamente consigliable, ogni volta che è possibile, *scaricare* il contenuto della propria
casella di posta, così da ridurre la quantità di dati riservati presenti sul server.

Infine puoi [scegliere tra molti simpatici domini quello che più ti aggrada ;)))](/get_service)

Inoltre con la tua casella di posta avrai la possibilità di crearti fino a cinque indirizzi alternativi (come
alias della stessa casella di posta)! NB: Quando possibile, preferiamo di gran lunga che tu ti crei un alias, piuttosto che richiedere un
account aggiuntivo.

Tieni presente che le mailbox che non vengono lette per più di 12 mesi sono automaticamente disattivate e il loro contenuto eliminato per sempre.


Se hai dubbi su come configurare e usare una casella di posta su A/I, consulta i manuali e gli howto in questa pagina:

- [Manuali e howto sul servizio di posta elettronica di A/I](/docs/mail/ "Manuali e howto per gli utenti di posta di A/I")


Perché una casella su A/I?
--------------------------

**Perché SÌ:**

- perché pensi che la lotta per difendere la libertà di comunicare debba partire da iniziative non commerciali.
- perché vuoi fare la tua piccola parte anche nella vita di tutti i giorni, stando lontano dalle multinazionali.
- perché condividi le nostre idee politiche, e vuoi che l'indirizzo che usi rifletta il tuo atteggiamento.
- perché quando qualcuno prova a interrompere i nostri servizi, o a minarne la sicurezza, noi faremo il possibile per impedirlo.
- perché più siamo più è facile farsi sentire.

**Perché NO:**

- perché credi che usando un indirizzo presso di noi, automaticamente sarai anonimo, indipendentemente da dove ti registrerai e da quello
    che scriverai.
- perché svolgi attività a fini di lucro che necessitano di rimanere private. Per farlo, prova a esplorare alternative commerciali, per
    esempio cercando [qui](http://prism-break.org).
- perché quando vorranno attaccare la libertà di comunicare di qualcuno tu sarai tra i primi a farlo.
- perché hai i mezzi o le competenze (o entrambi), e la difesa della privacy te la puoi gestire per conto tuo.
- perché vuoi solo creare un account anonimo su un sito per svolgere attività indesiderate o spiacevoli in un contesto che non ha nulla a
    che vedere con i nostri principi, con l'attivismo o con la giustizia sociale.
- perché hai solo bisogno di privacy, ma non abbiamo nessun altro punto di affinità in comune.

A/I VS Gmail: pro e contro nello scegliere noi
----------------------------------------------

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">A-I</td>
<td align="left">G-mail</td>
</tr>
<tr class="even">
<td align="left">Spazio disponibile</td>
<td align="left">nessun limite, ma SCARICATI la posta!</td>
<td align="left">nessun limite, archiviano tutto (finché gli piace).</td>
</tr>
<tr class="odd">
<td align="left">POP3 e POP3-SSL</td>
<td align="left">SÌ (certificati di letsencrypt)</a>)</td>
<td align="left">SÌ (certificati di Google INC)</td>
</tr>
<tr class="even">
<td align="left">IMAP e IMAP-SSL</td>
<td align="left">SÌ (certificati di letsencrypt)</td>
<td align="left">SÌ (certificati di Google INC)</td>
</tr>
<tr class="odd">
<td align="left">SMTP TLS</td>
<td align="left">SÌ (certificati di letsencrypt)</td>
<td align="left">SÌ (certificati di Google INC)</td>
</tr>
<tr class="even">
<td align="left">Analisi dei contenuti delle tue mail</td>
<td align="left">NO, mai.</td>
<td align="left">SÌ, per pubblicità calibrata (e quando lo chiede l'NSA).</td>
</tr>
<tr class="odd">
<td align="left">Log (tracciamento) degli IP delle tue connessioni</td>
<td align="left">No, mai.</td>
<td align="left">SÌ, sempre.</td>
</tr>
<tr class="even">
<td align="left">Webmail SSL</td>
<td align="left">SÌ (certificati di letsencrypt)</td>
<td align="left">SÌ (certificati di Google INC)</td>
</tr>
<tr class="odd">
<td align="left">banner in webmail</td>
<td align="left">NO</td>
<td align="left">SÌ (testuale)</td>
</tr>
</tbody>
</table>

Cosa significa avere una casella di posta
-----------------------------------------

In poche righe, per chi fosse completamente all'oscuro del concetto di posta elettronica, ecco che cosa vuol dire utilizzare una casella
e-mail.

Una casella e-mail è esattamente come avere un postino nel proprio computer: scrivi una cartolina, la invii a un indirizzo e la posta viene
recapitata attraverso l'interazione tra i server (si chiama server la macchina che gestisce un certo servizio) della tua e-mail e di quella
del tuo destinatario.

Avere un account di posta elettronica vuol dire avere un nome e un indirizzo a cui farsi inviare e da cui spedire posta.

Ci sono due principali modi per leggere la posta: guardarla via web attraverso un browser (Firefox, Chrome, Opera o quant'altro) 
o scaricarla sul proprio pc attraverso un cosiddetto client di posta elettronica (un programma come
Thunderbird, Sylpheed, Outlook Express o quant'altro) in grado di parlare con il vostro server e di
recapitarvi le mail sul vostro pc.
Il client può anche essere usato per leggere la posta senza scaricarla (IMAP invece di POP), cosa che è consigliabile fare per esempio su
uno smartphone (per Android consigliamo K-9 Mail); d'altra parte, se la posta non viene scaricata, c'è il problema che (1) le risorse dei server
di A/I vengono sfruttate eccessivamente e (2) che non hai il controllo fisico della tua corrispondenza. Per questo consigliamo di scaricare
ogni volta che è possibile la posta in un computer sicuro.

Ovviamente oltre a leggere la posta puoi anche inviarla, sia via web come sopra, che dal tuo computer, passando attraverso un altro servizio
offerto da tutti i server di posta del mondo, l'SMTP, che serve appunto a ricevere il messaggio e a girarlo al destinatario.

Nota bene che tutte queste comunicazioni non sempre viaggiano crittate lungo la rete, attraverso tutti i nodi che attraversano, ma in
chiaro senza alcuna protezione.
È facile pensare che se qualcuno volesse controllarci non ci metterebbe molto a intercettare questi messaggi come se fossero volantini
abbandonati sul marciapiede. Per questo l'analogia migliore consiste nel pensare ai propri messaggi e-mail come a semplici cartoline.

Per limitare questa possibilità (NB non per escludere le possibilità di controllo, ma per limitarle), forziamo l'utilizzo di connessioni
crittate con SSL per ricevere e inviare i messaggi.
