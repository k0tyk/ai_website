title: Hosting di siti web
----

Hosting di siti web
==================

Il collettivo A/I vi dà la possibilità di pubblicare il vostro sito.

Le pregiudiziali per poter partecipare ai servizi offerti sui nostri server sono la condivisione dei **principi di anti-fascismo,
anti-razzismo, anti-sessismo, anti-militarismo e non commercialità** che animano questo progetto, oltre ovviamente a una buona dose di
volontà di condivisione e di relazione ;)))))

Il vostro sito sarà visibile come **sottodirectory del server autistici.org o inventati.org** (es: www.autistici.org/vostrosito oppure
www.inventati.org/vostrosito) e vi sarà attivato un [account WebDAV](/docs/web/webdav) per poter uploadare e gestire il vostro
materiale via [WebDAV](/docs/web/webdav).

Se preferite è anche possibile ospitare uno spazio web con un suo proprio [**dominio**](/docs/web/domains) (ma dovrete
acquistarlo da un provider commerciale).

Vi preghiamo di segnalarci se avrete bisogno del supporto PHP e di un database MySQL quando [effettuate la richiesta per ospitare un sito
presso i nostri server](/get_service).

Nei manuali e nella documentazione qui sotto potete leggere tutto ciò che vi serve per ospitare un sito su A/I: <a name="howto"></a>

- [Come ospitare il vostro dominio sui server di A/I](/docs/web/domains "come ospitare il vostro dominio sui server di A/I")
- [Come usare un account WebDAV per aggiornare il vostro sito](/docs/web/webdav "come usare un account dav per aggiornare il vostro sito")
- [Alcuni appunti sulla vostra privacy e su quella dei vostri visitatori](/docs/web/privacyweb "alcune brevi note sulla vostra privacy e su quella dei vostri visitatori")
- [Alcune FAQ (Domande molto frequenti) sui siti ospitati sui server di A/I](/docs/faq/)
- [Come attivare le statistiche per il vostro sito](/docs/web/webstats "come attivare le statistiche per il vostro sito")
- [Note tecniche sull'hosting di siti su A/I](/docs/web/tech_web "Note tecniche sull'hosting di siti su A/I")
- [Come aggiungere i banner di A/I al vostro sito](/docs/web/banner "come aggiungere i banner di A/I al vostro sito")

Ad ogni sito web sarà associata una casella e-mail sui nostri server, dove invieremo le comunicazioni tecniche e le novità relative al
servizio.

**Importante: sulla sicurezza dei server**

Se nel tuo sito hai intenzione di installare un CMS come Drupal, b2evolution, Wordpress o qualunque altra cosa, innanzitutto chiediti
seriamente se non ti sarebbe più comodo [aprirti un blog su Noblogs.org](/services/blog), dopodiché, se proprio decidi che questa è
la scelta più appropriata, ti raccomandiamo di aggiornare regolarmente il software, controllando con attenzione la presenza di nuove
versioni.

Questo è molto importante per la tua sicurezza e per quella di tutt\* gl'altr\* utenti,
perché nelle versioni vecchie potrebbero esserci falle sfruttabili da eventuali malintenzionat\* o burlon\* per entrare nel server e fare danni.

Siccome non sei l'unic\* a usare i nostri server, ma condividi uno spazio con molt\*
altr\*, noi riteniamo sia indispensabile che tutt\* si assumano la responsabilità di
quello che installano sui server, perché i nostri server vivono di autogestione e quindi di partecipazione e responsabilità condivisa.

Sul sito del cms che avete scelto ci sono sicuramente le istruzioni su come aggiornare: di solito si tratta comunque di copiare dei file al
posto di quelli vecchi, lasciando intatti i contenuti.

Ora che avete letto tutto quello che potevate sul nostro servizio di hosting di siti web, potete **[chiederci di aprirvene uno](/get_service)**,
ma ricordate che questo progetto è completamente **[autofinanziato](/donate)** e vive solo grazie al
supporto di tutti e al lavoro volontario dei membri del collettivo.

Per qualsiasi problema doveste riscontrare (accesso webdav, accesso ai database, statistiche, registrazione del vostro dominio soprattutto)
assicuratevi di aver consultato la nostra **[pagina di supporto](/get_help)** e **i manuali menzionati in questa pagina** prima di
disperarvi.
