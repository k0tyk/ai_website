title: Alojamiento de sitios web
----

Alojamiento de sitios web
======================

El colectivo A/I te da la posibilidad de tener tu propio sitio web.

Los [criterios](/who/policy) para participar y disfrutar de los servicios ofrecidos por nosotr\*s son el
compartir nuestros **principios de anti-fascismo, anti-racismo, anti-sexismo y anti-militarismo** que animan este proyecto.

Tu sitio debe compartir el enfoque no-comercial que apoyamos.

Tu sitio web generalmente será visible como un **subdirectorio de** de autistici.org o inventati.org (por ejemplo: www.autistici.org/misitio
o www.inventati.org/tusitio) y se te dará un nombre de usuari\* y contraseña para subir (vía
[FTP](/docs/web/webdav)) tus archivos.

Si lo prefieres, puedes alojar un [**dominio**](/docs/web/domains) de tu propia elección (pero tendrás que comprárselo a algún
proveedor comercial).

Cuando [solicites un sitio web](/get_service) por favor dinos si vas a necesitar una base de datos MySQL y/o soporte para PHP.
 Encontrarás mucha información sobre cómo alojar un sitio web en A/I en nuestros tutoriales y manuales: <a name="howto"></a>

- [¿Cómo alojar tu propio dominio en nuestros servidores?](/docs/web/domains "Cómo alojar tu propio dominio en nuestros servidores")
- [¿Cómo usar WebDAV para actualizar y subir tu sitio web](/docs/web/webdav "Cómo usar webdav para actualizar y subir tu sitio web")
- [Un breve recordatorio acerca de la privacidad de los datos de webmasters y usuari\*s](/docs/web/privacyweb "Un breve recordatorio acerca de la privacidad de los datos de webmasters y usuari*s")
- [Algunas preguntas frecuentes sobre sitios alojados en A/I](/docs/faq/)
- [¿Cómo activar las estadísticas en tu sitio?](/docs/web/webstats "Cómo activar las estadísticas en tu sitio")
- [Características técnicas del hosting web de A/I](/docs/web/tech_web "Características técnicas del hosting web de A/I")
- [Cómo añadir el banner de A/I en tu sitio](/docs/web/banner "Cómo añadir el banner de A/I en tu sitio")

Una cuenta de email en nuestros servidores tiene que estar conectada con cada sitio web para comunicaciones técnicas y noticias de los
servicios web. Así que, si no tienes una, necesitarás abrirla para solicitar un sitio web.

**Nota muy importante sobre la seguridad de los servidores**

 Si lo que estás pensando es instalar en tu sitio web un gestor de contenidos como Drupal, b2evolution, Wordpress o similares, primero mira
si no sería mejor que [abrieras un blog en nuestra plataforma Noblogs.org](/services/blog); después de hacerlo, y si has decidido
que un CMS es la mejor opción para tu sitio web, te recomendamos que actualices regularmente el software, mirando cuidadosamente si hay
nuevas versiones.

Esto es importante la tu seguridad y la de cada usuari\*, porque versiones antiguas pueden tener bugs (fallos) que
pueden ser fácilmente explotados para acceder a los servidores y hacer líos.

Como no eres la única persona usando nuestros servidores, y además estás compartiendo el espacio, pensamos que es absolutamente necesario
que cada quien sea responsable de lo que instala en los servidores, porque nuestros servidores viven de la autogestión y de la
responsabilidad y participación compartida y colectiva.

En el sitio web del gestor de contenidos que elijas seguramente haya instrucciones acerca de cómo actualizar el software; generalmente se
tratará de reemplazar unos archivos viejos por otros, dejando los contenidos intactos.

Ahora que ya has leído todo acerca de nuestro servicio de alojamiento web, puedes **[solicitarnos abrir un sitio web](/get_service)**
pero recuerda que este proyecto vive exclusivamente gracias a las **[donaciones](/donate)** de
nuestr\*s usuari\*s.

Por favor, mira nuestra **[página de ayuda](/get_help)** y **los manuales mencionados** para cualquier problema que puedas
encontrar (ftp, bases de datos, estadísticas web, registro de dominios especialmente).
