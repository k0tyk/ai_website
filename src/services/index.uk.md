title: Послуги
----

Послуги
=======

A/I надає [послуги й засоби для досягнення анонімного й безпечного спілкування](/docs/mail/privacymail "захистіть свою приватність").

Усі наші послуги надають особливий пріоритет користувацькій приватності. Ми **не ведемо журналів** з'єднань і не записуємо жодних даних про осіб чи псевдоніми. До того ж усі пропоновані нами засоби використовують примусове шифрування.

## Які послуги надає A/I?

Ми надаємо набір засобів для спілкування:

- [Скриньки е-пошти](/services/mail)
- [Блоги](/services/blog)
- [Списки листування й розсилання](/services/lists)
- [Вебхостинг](/services/website)
- [Миттєві повідомлення й бесіди](/services/chat)
- [Відеовиклики й трансляції](/services/videoconference-streaming)
- [Послуги анонімності](/services/anon)

## Ціни

Послуги, які ми надаємо, не мають визначеної ціни, але, як ви можете легко здогадатись, підтримка роботи всієї цієї системи (тобто апаратного забезпечення й хостингу) [вкрай дорога](/who/costs) (наразі коштує нам приблизно 450 000 гривень за рік).

Щоб служби залишалися в мережі, а наш проєкт жив далі, було б дуже корисно, якби кожна особа, кожна група й кожен проєкт, що використовують надані нашою мережею послуги, надсилали нам щорічну пожертву відповідно до їхніх можливостей. Звісно, це не означає, ніби ми вимагаємо пожертви чи просимо їх «в обмін»: наші послуги — не для продажу, й пожертва не є ні обов'язковою, ні достатньою для отримання облікового запису. Проте якщо ви вважаєте, що заслуговуєте на підтримку нашими службами через вашу близькість до принципів нашого натхнення, ви ймовірно також чудово розумієте, наскільки ваша підтримка важлива для продовження нашої роботи. Довіра й підтримка існують лише тоді, коли вони взаємні.

Сума вашої пожертви може становити як сто гривень за скриньку, так і тисячу за звичайний вебсайт — чи й більше, якщо ціла ваша група чи колектив використовує поштові скриньки й списки листування, або якщо ви особисто просто маєте змогу й бажання зробити суттєвіший внесок. Дехто з наших найвідданіших користувачок і користувачів організовували цілі вечірки збору коштів для пожертви нам. Мабуть, це найбільш приємний нам спосіб підтримки.

Хай там як, ми ніколи не стежитимемо за вашими економічними справами, але ми все ж очікуємо на певне розуміння (зокрема через те, що якщо в нас не залишиться грошей, нам доведеться припинити надання послуг).

Просимо обдумати це й [внести пожертву](/donate)
