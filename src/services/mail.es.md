title: Cuentas de correo
----

** atención, esta página está obsoleta, la versión en inglés está más actualizada. **

Cuentas de correo
=================

[Tener una cuenta de correo electrónico](/get_service) en nuestros servidores (si no sabes qué es una cuenta de correo electrónico,
[¡mira al fondo de la página!](#what "¿Qué significa tener un Email?")) es una forma de salvaguardar tu (y nuestra) propia privacidad,
eliminando la necesidad de dar datos personales o tus relaciones a multinacionales o proveedores comerciales. Al mismo tiempo esto significa
que debes tener la capacidad de utilizar algunas de las herramientas que proveen nuestros servidores, tal como las comunicaciones cifradas a
través de SSL.

Además, no te preguntamos por tu código postal, ni por tu documento de identidad o tus nombres y apellidos a la hora de abrir una cuenta.

Las cuentas de correo de A/I no tienen límite de espacio, por eso esperamos que uses el sentido común y no gastes los preciosos recursos que
compartes con much\*s usuari\*s (¡y recuerda [donar](/donate "dona!")!).
Recomendamos activamente que **descargues** tu correo electrónico cada vez que te sea posible, no sólo para reducir la carga en nuestros
servidores, sino para que en ellos haya la menor cantidad de datos personales posible.

Nuestras cuentas de correo ofrecen una [amplia variedad de dominios simpáticos...¡así que elige! ;)))](/get_service)

Y junto a tu cuenta de correo tienes la posibilidad de crear hasta **cinco** cuentas adicionales (como alias de tu misma cuenta de correo)

Si tienes alguna pregunta (o duda) sobre cómo usar tu cuenta de correo, por favor mira los siguientes manuales:

- [Parámetros de conexión](/docs/mail/connectionparms "Parámetros de conexión para las cuentas de A/I ")
- [Notas técnicas sobre el email de A/I](/docs/mail/tech_mail "Notas técnicas")
- [Autenticación de dos factores](/docs/2FA "autenticación de dos factores")
- [Reglas muy sencillas y básicas para proteger tu privacidad cuando usas el email](/docs/mail/privacymail "Reglas muy sencillas y básicas para proteger tu privacidad cuando usas el email")
- [Cómo cambiar o recuperar tu contraseña](/docs/mail/passwd "Cómo cambiar o recuperar tu contraseña")
- [Como usar el webmail de A/I](/docs/mail/roundcube "Como usar el webmail de A/I")
- [Cómo configurar los filtros del webmail](/docs/mail/sieve "Cómo configurar los filtros del webmail")
- [Cómo configurar el cliente Evolution para A/I](/docs/mail/evolution "Cómo configurar el cliente Evolution para A/I")
- [Cómo configurar el cliente Fetchmail para A/I](/docs/mail/fetchmail "Cómo configurar el cliente Fetchmail para A/I")
- [Cómo configurar el cliente mutt+msmtp para A/I](/docs/mail/mutt-msmtp "Cómo configurar el cliente mutt+msmtp para A/I")
- [Cómo configurar el cliente Thunderbird para A/I](/docs/mail/thunderbird "Cómo configurar el cliente Thunderbird para A/I")
- [Cómo configurar el cliente Sylpheed para A/I](/docs/mail/sylpheed "Cómo configurar el cliente Sylpheed para A/I")
- [Cómo configurar Postfix para que maneje las cuentas de A/I](/docs/mail/postfix "Cómo configurar para A/I")
- [Cómo configurar el cliente Mail OSX para A/I](/docs/mail/mail-osx "Cómo configurar el cliente Mail OSX para A/I")
- [Cómo configurar el cliente Oulook para A/I](/docs/mail/outlook "Cómo configurar el cliente Oulook para A/I")

¿Porqué una cuenta de correo en A/I?
------------------------------------

**SÍ:**

- porque piensas que la lucha en defensa de la libertad de expresión y comunicación no es algo que pueda delegarse a cualquiera.
- porque quieres hacer una pequeña diferencia en tu día a día, manteniéndote al margen de corporaciones multinacionales y, por alguna
    razón -mala ;)-, te fías de nosotr\*s.
- porque prefieres que tu cuenta de correo sea una cuestión privada (y quieres usar SSL)
- porque no quieres ayudar a que una megatienda se promocione cuando le das tu cuenta de correo a alguien.
- porque te fastidian quienes te dan una cuenta de correo gratis para llenarte de publicidad.
- porque cuando alguien intente cerrar los servicios de A/I o amenacen su seguridad, el colectivo entero intentará hacer lo mejor
    para impedirlo. Y cuant\*s más seamos, más interesante será.
- porque te gustan nuestros dominios (bueno, si es sólo por esto mejor que comprendas con quienes te estás metiendo en negocios)
- porque no quiero que mis actividades en internet sean monitorizadas, procesadas y revendidas al mejor postor.
- porque cuant\*s más somos, más alto podemos gritar.
- porque te respondemos el correo, antes o después. Bueno, al menos lo intentamos.

**NO:**

- porque no es un servicio gratuito, sino que continúa sólo si TOD\*S contribuyen con lo que pueden cada año.
- porque cuando ataquen la libertad de expresión o comunicaciones, serás un\* de l\*s
    primer\*s en enterarte.
- porque nuestros servidores no son 100% seguros y la privacidad es algo que, en última instancia, depende de ti.
- porque tengo otros recursos en internet que puedo usar
- porque no tengo nada que esconder
- porque parece que gmail funciona mejor.

A/I vs Gmail: ventajas y desventajas
------------------------------------

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">A-I</td>
<td align="left">G-mail</td>
</tr>
<tr class="even">
<td align="left">Espacio disponible</td>
<td align="left">sin límite, pero por favor, ¡DESCARGA tu correo!</td>
<td align="left">sin límite: guardan un registro de todo (a su voluntad).</td>
</tr>
<tr class="odd">
<td align="left">POP3 y POP3-SSL</td>
<td align="left">SÍ (letsencrypt CA)</td>
<td align="left">SÍ (Google INC CA)</td>
</tr>
<tr class="even">
<td align="left">IMAP y IMAP-SSL</td>
<td align="left">SÍ (letsencrypt CA)</td>
<td align="left">SÍ (Google INC CA)</td>
</tr>
<tr class="odd">
<td align="left">SMTP TLS</td>
<td align="left">SÍ (letsencrypt CA)</td>
<td align="left">SÍ (Google INC CA)</td>
</tr>
<tr class="even">
<td align="left">Análisis de contenido de tu email.</td>
<td align="left">NO, nunca.</td>
<td align="left">SÍ, para publicidad personalizada.</td>
</tr>
<tr class="odd">
<td align="left">Rastreo de las IP desde las que te conectas</td>
<td align="left">NO, nunca.</td>
<td align="left">SÍ, siempre.</td>
</tr>
<tr class="even">
<td align="left">SSL Webmail</td>
<td align="left">SÍ (letsencrypt CA)</td>
<td align="left">SÍ (Google INC CA)</td>
</tr>
<tr class="odd">
<td align="left">Publicidad en webmail.</td>
<td align="left">NO</td>
<td align="left">SÍ (en formato de texto)</td>
</tr>
</tbody>
</table>

¿Qué significa tener un Email?
------------------------------

Para quienes no saben realmente que significa tener un email, trataremos de explicarlo en unas pocas líneas

Tener una cuenta de email es exactamente lo mismo que un correo físico dentro de tu ordenador: escribes una carta, la envías a una
dirección, y el correo es enviado a su destino a través de una serie de servidores (un servidor es un ordenador cumpliendo un servicio).

Tener una cuenta de email significa tener un nombre y una dirección a la que personas puedan enviarte correos.

Hay dos formas principales de leer los mensajes de email: puedes verlos a través de un navegador (Firefox, Netscape, Konqueror, Galeon,
Explorer, Opera y demás) o puedes descargarlo y enviarlo a través de un programa en tu ordenador (un programa como Thunderbird, Netscape
Messenger, Kmail, Pegasus Mail, Outlook Express, Eudora o cualquier otro "cliente de correo" capaz de "entenderse" con tu servidor de
correo.

Por supuesto, aparte de leer los mensaje de email, también puedes enviarlos, también a través de un navegador o un cliente de correo, como
en el párrafo anterior. Cuando envías un correo, usas un servicio llamado SMTP, diseñado para enviar tus cartas a la dirección que le has
dado, donde quiera que pueda estar.

Deberías saber que normalmente todas estas comunicaciones no están encriptadas a lo largo de todo su camino, y pueden ser leídas fácilmente
en cualquiera de los nodos por los que pasan en su viaje.
 No es impensable que si alguien quiere controlarte, leer tus mensajes como si fueran volantes, pasquines u octavillas abandonados en la
calle ;) -para esta analogía, es mejor pensar en tu correo como si fuera una simple carta.

Para limitar estos abusos (y no para excluir toda forma de control, sino para limitarlas), puedes elegir enviar y recibir tus correos vía
SSL, es decir, encriptado (codificado de forma que no sea fácilmente legible para cualquiera).
