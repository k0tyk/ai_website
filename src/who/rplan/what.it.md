title: Il Piano R* - Cosa significa in pratica?
----

Il Piano R\*: un network di comunicazione resistente
=============================================================================

Cosa significa in pratica per le comunicazioni digitali^
--------------------------------------------------------

Il Piano R\* \[[1](#1)\] prevede la distribuzione di vari server, e quindi dei servizi, in diversi provider sparsi
in giro per il mondo (siamo sempre in cerca di Provider o ISP disponibili a ospitare le nostre macchine).

Ogni server ospita tutti i servizi fondamentali offerti da A/I e alcuni servizi specifici, oltre a una copia delle informazioni necessarie a
ricreare l'intera infrastruttura (configurazioni e database utenti in particolare).

I dati pubblici (principalmente siti web) sono dislocati sui diversi server, in modo che sia molto difficile rendere inaccessibile un
singolo contenuto agli utenti.

I dati personali (principalmente le mailbox) sono distribuiti sui vari server, in un'ottica di "riduzione del danno". Abbiamo ritenuto più
importante salvaguardare la possibilità e la riservatezza delle comunicazioni che impegnarci a fare infinite copie di mail private da
sparpagliare in giro.

In caso di compromissione di un server, tutti gli utenti verranno traferiti, senza necessità di riconfigurazioni, sulle altre macchine
appartenenti al network, tentando di garantire così la continuità della comunicazione.

Nel caso in cui tutti i server pubblici venissero manomessi contemporaneamente, vorrebbe dire che la situazione attorno a noi è ben più
preoccupante. E non riuscire più a spedire le email potrebbe essere un piccolo imprevisto in uno scenario ben più complesso. In quel caso,
saremo raggiungibili su Alfa Centauri, dove speriamo che per allora sia pronto il mondo migliore di cui si parla tanto.

Oltre a questo, tutti i dati che possono servire a decrittare il traffico (chiavi di crittazione in primis) si troveranno sull'hard disk dei
server soltanto in forma cifrata e le macchine saranno dotate di sensori per verificare eventuali manomissioni fisiche. Continueremo a non
salvare i log (i dati necessari all'identificazione delle connessioni da e per la nostra rete) e a non conservare corrispondenze tra
identità e nickname degli utenti.

Le soluzioni pensate cercano di costringere, o quantomeno a invitare, chi voglia intervenire sul server a contattare A/I, in modo da
metterci in condizione di conoscere il tipo di intervento, le modalità e le motivazioni e di garantire la continuità delle comunicazioni.

Tutte queste cose dovrebbero essere scontate, ma evidentemente nella nostra buffa società, costruita sulla paura del proprio simile,
diventano qualcosa che è necessario inventare e costruire.

Il piano R\* si pone insomma l'obiettivo di:

- Scongiurare un'interruzione forzata delle comunicazioni.
- Poter decidere le modalità di eventuali interventi da parte di esterni sulla nostra rete (e quindi sui dati degli utenti).
- Offrire servizi a un numero crescente di persone che non vogliono consegnare le loro comunicazioni private ai sociologi delle
    multinazionali (quando siamo fortunati), superando il limite fisico legato all'esistenza di un solo server.
- Fare il possibile, inventando quando serve, per proteggere la privacy dei nostri utenti.

### La Svolta :)

Per noi è evidente che **tutto questo non basta**. Indubbiamente ci pone in condizioni migliori di quelle con cui abbiamo tirato avanti fino
a ora, ma **la battaglia per diffondere una cultura che si opponga al controllo e alla repressione come panacea di tutti i problemi è molto
più complicata di una semplice soluzione tecnica.**

Forse l'aggressione quasi maniacale alle libertà fondamentali di molti che caratterizza questi anni pieni di paure contribuirà a creare le
basi per una maggiore cultura dell'autodifesa della privacy e della libertà di esprimersi e di comunicare. O forse no. E allora tutti gli
escamotage tecnici che possiamo inventarci saranno probabilmente inutili.

La nostra speranza è che sempre più persone contribuiscano a formarsi e a formare altri nell'uso degli strumenti, informatici e non, per
tentare di fermare le minacce alla libertà di comunicare e di esprimersi.

Abbiamo lavorato per migliorare i nostri servizi, per renderli più resistenti, e continueremo a farlo. Ma questo non può e non deve bastare
a nessuno. L'ultima cosa che vorremmo è creare un falso senso di sicurezza per cui i nostri utenti si sentano nella condizione di delegare a
noi la tutela della propria riservatezza. Il mondo che ci circonda ci ha già dato prova che "fatto un piano trovato l'inganno", e che non ci
sono trucchi o piani infallibili che ci possano sollevare da un continuo e personale impegno nella nostra autodifesa personale. Chi voglia
approfondire la questione e scoprire quali sono gli strumenti per salvaguardare la propria privacy puo' leggere il nostro [manuale sulla
privacy della posta](/docs/mail/privacymail), che contiene anche molti riferimenti utili ad altri materiali.

- - -

**Note**

<a name="1"></a>\[1\] Viste le grandi soddisfazioni che il piano R ha dato al signor gran venerabile Licio Gelli, visto l'enorme successo
ottenuto in questi anni, abbiamo pensato di imparare dai vincitori: quello che ci mancava era una strategia. Abbiamo quindi fatto quello che
insegnano a scuola: abbiamo guardato alla storia del nostro grande paese per trarne ispirazione. Abbiamo letto di tutto: dal *Libro Cuore*
fino al *De prima Repubblica* di Giulio Andreotti. Ma quale migliore strategia di quella che si realizza punto per punto a 30 anni dal suo
concepimento? Altro che piano quinquennale. Serviva anche a noi un piano R, anzi di più un piano R\*.
