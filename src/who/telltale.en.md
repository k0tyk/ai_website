title: A short tale about why we are who we are and why we do what we do
----

A short tale about why we are who we are and why we do what we do
=================================================================

When we wrote [our first manifesto](/who/manifesto), the world wasn't a fairly
uncomplicated place. Our perspectives were more limited and
our future set into the shape of a snake eating its own tail. In those 
times A/I was a tiny dock for our vessel, representing survival 
and resistance. We wanted to contribute with the things we were able to do to 
support the small, stubborn and determined community of the in power dissidents.
We called ourselves autistics, or paranoids, well aware of
our attitude towards technology, and our instinctive distrust of the liberal
rhetorics of representative democracy and its inconsistent repressive tendencies. 
These attitudes were radical for those years -- times of optimism and compromise 
with the abuses of global capitalism.

We had nailed it already back then, more than 20 years ago. 
Today we are among the few providers of services that respect their users' privacy 
and equate it to a political claim, and if we do what we do it's because the 
system that systematically uses technology to surveil and control our lives 
(when it's not exploiting our private communications and our sensitive data 
on behalf of multinational companies) has officially let the mask fall. 
Since Snowden's leaks started to become public, our community has faced a flow 
of scared individuals, embittered and suddenly (or finally, perhaps) 
disillusioned. But at the same time we notice way too often a need to find a simple 
and quick solution: the expectation of obtaining free access to a safe harbour,
in the same fashion as with the corporate services one doesn't necessarily pay with money.
Once again an attitude that is a product of delegation, which is an utilitarian 
and irresponsible process. 

But in the same way as when we started, our approach is first and foremost political,
radical and stubbornly anti-authoritarian. From the very start we have been forced
to define ourselves by using negative statements, because the things happening around us
were so disgusting it was necessary to keep them at a distance without leaving any
uncertainty: therefore **antifascists, antiracists, antisexists, antimilitarists**.
In our harbour, protection is offered only to those who dare opposing those
hateful winds, which unfortunately are blowing all over Europe.

And of course it was necessary to state very clearly our
nature was (and still is) **non-commercial**, since we want to make clear that A/I
represents time stolen from our daily toil, from the precarious and wearying
torments of a job, contributing to the joy, the sweat, the hardwork and tears
of the struggles we participated to, against a world shaped into a coin.

In these flexibly painful times mankind is replaced by human capital (where, to
be precise, capital is the noun and human just an adjective). Being forced to
pick a role as an actor on such a stage, our primary need is to unite and contribute
with our skills and ideas to the survival of critical thinking and of our very
lives.

Engineering stems from the ability of man to use its opposable thumb but
in these times it has completed its transformation into religious belief, becoming a
strategic sector for an endless process of control and growth that is both deceptive and
suicidal.

The cycle of life: consuming resources to produce objects which will
soon become rubbish. Data is gathered to produce desires, profit and power,
and to create fear, self-censorship, resignation. Quite an experience to live
in fear, isn't it? That's what it is to be a slave.

Maybe what we wanted to tell to the people was just: don't be
afraid, stand up; dignity, determination, bravery, self-organization. And these may be the only words in
our manifesto that really matter.
