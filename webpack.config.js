const glob = require('glob');
const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { SubresourceIntegrityPlugin } = require('webpack-subresource-integrity');

module.exports = {
    mode: 'production',
    context: __dirname,
    entry: {
        ai: ['./ui/src/ai.js', './ui/src/ai.css'],
    },
    output: {
        publicPath: '/',
        path: __dirname + '/assets/',
        filename: 'static/[name].[contenthash].js',
        assetModuleFilename: 'static/[contenthash][ext]',
        crossOriginLoading: 'anonymous',
        clean: true,
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),
        new MiniCssExtractPlugin({
            filename: 'static/[name].[contenthash].css',
        }),
        new PurgeCSSPlugin({
            paths: glob.sync(__dirname + '/ui/templates/*.tmpl'),
            safelist: [/show/, /^tt-/],
        }),
        new SubresourceIntegrityPlugin(),
    ].concat(
        glob.sync(__dirname + '/ui/templates/*.tmpl').map(f => new HtmlWebpackPlugin({
            filename: path.join('templates', path.basename(f)),
            template: f,
            minify: false,
            inject: false,
        }))
    ),
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(ico|png|gif|jpe?g|svg|eot|otf|ttf|woff)$/,
                type: 'asset/resource',
            },
        ],
    },
};


