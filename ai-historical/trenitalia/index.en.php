<html>
<head>
  <title>A wagonful of...</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>A wagonful of...</h3>
  <small>Trenitalia Vs Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="mirrors.en.php">Mirrors</a></li>
   <li><a href="legaldocs.en.php">Legal Documentation [only ITA]</a></li>
   <li><a href="rassegna.en.php">Web/Press Release</a></li>
   <li><a href="comunicato_zenmai23.php">Zenmai23 statement [only ITA]</a></li>
   <li><a href="link.php">Links about Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.en.php">What can I do?</a></li>
	<ol>
	<li><a href="protesta.en.php#mail">Send an e-mail to Trenitalia</a></li>
	<li><a href="protesta.en.php#banner">Put our banner on your site</a></li>
	<li>Sign the <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.en.php#signature">Use this signature!</a></li>
	<li><a href="protesta.en.php#stampa">Printed material for divulgation</a></li>
	<li>Make a <a href="http://autistici.org/it/donate.html">donation!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page Autistici/Inventati</a><br/>

 </div>


 <div id="main">



<h1>
A wagonful of...
</h1>


<small><a href="materiali/trenitalia-01092004.pdf">Print! [ITA]</a>
</small>
<br>
<br>
<h2><b>Breaking news</b></h2>
<p>
Today (17/09/2004) Judges accepted our counter-claim against Trenitalia's requests,
which will pay our lawyers expenses. <br>
Obviously Autistici/Inventati is proud to present the "forbidden
webpage":<br>
<a href="http://www.autistici.org/zenmai23/trenitalia"
target="_blank">http://www.autistici.org/zenmai23/trenitalia</a>
</p>
<br>
<br>
<br>
<h2>Past days</h2>

<p>
Around the half of july 2004 autistici.org / inventati.org server received a legal document from Trenitalia (italian railway company, <em>ndt</em>).
Before going on holidays,
the italian railway company lawyers told us that the website
http://autistici.org/zenmai23/trenitalia was "awfully offending" the company and
that they were proceeding to a court case against us in Milan, where they asked us to:
</p>
<p>
<ul>
<li>remove the site immediately</li>
<li>publish at our expenses an advertisment that the site has been removed
on two national newspapers </li>
<li>do not use anymore metatags relating to trenitalia in
 the autistici/inventati websites
</li>
</ul>
</p>

<p>
Apart from this requests to be fulfilled urgently, the lawyers go on and ask us to refund the moral and actual damages the company suffered from the site.
</p>
<blockquote>
<a href="comunicato.en.html">
You can read the full autistici.org/inventati.org statement here
</a>
</blockquote>

<p>
After a first series of quick hearings to decide upon the charges,
the judge confirms trenitalia requests.
<b>We have been forced to remove the site</b>.
</p>

<blockquote>
<a href="sentenza-03-08-2004.en.html">
Read the judge decision.
</a>
</blockquote>
<p>
The 7th of September, 2004, we presented our counter-claims about the
site removal. Trenitalia has then asked the court to
"EXTEND THE INIBITORY DISPOSITION TO EVERY OTHER PAGE OF
ANALOGOUS CONTENT": this practically means the elimination of
the various spontaneous mirrors that are listed here (see below).
</p>

<p>
This is a very delicate point, from the juridical point of view:
the principle that states that linking to some resource
constitutes illegal activity in itself, paradoxicalally
transforms every search engine in a crucible of illegalities.
The idea of the Internet and its workings that the Trenitalia
lawyers have is really frightening.
</p>

<p>
In 60 days, the judges will take a decision, and we will know
whether the page could get back to its own place or not.
In these days the real trial about damages will start. This
might become a huge problem for our server, that only lives
on volountary subscriptions.
</p>

<p>
If we loose, it will take 20.000 euros only to afford the
publication of the sentence on major italian newspapers.
We're obviously talking about money that we never had, and
realistically never will.
</p>



  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
