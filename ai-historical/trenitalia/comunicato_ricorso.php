<html>
<head>
  <title>Comunicato sulla vittoria del ricorso</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">


<h3>
Fine primo round con
Trenitalia alla corde!
</h3>
<p>
<hr>
<small>
17 Settembre 2004
</small>
</p>
<p>
In un torrido agosto a seguito di una citazione da aprte di Trenitalia
siamo stati costretti a rimuovere un sito ospitato
sul nostro server http://autistici.org/zenmai23/trenitalia
<br />
Potete leggere della vicenda giudiziaria non ancora conclusa
sulle pagine

<br />
http://autistici.org/ai/trenitalia
</p>
<p> 
Qualche giorno fa, precisamente il 14 settembre abbiamo ottenuto
attraverso il ricorso presso il tribunale di Milano, l'annullamento
della precedente sentenza ed il sito e' potuto tornare on line.

</p>
<p>
La pagina rimossa era opera di un collettivo di web designer
e ricalcava in tutto e per tutto l'home page di Trenitalia,
inserendovi pero' una critica intransigente riguardo al
coinvolgimento di quest'ultima nel trasporto di armamenti
sul territorio italiano all'inizio della guerra in Iraq.
Ma in fondo non serve piu' che la descriviamo a parole,
dato che ora potete visitarla senza problemi...

</p>
<p>

Nel periodo in cui e' stata elaborata, circa un anno e mezzo fa,
erano in corso in tutta Italia diverse forme di protesta contro
l'imminente inizio della campagna irachena.

</p>
<p>

La sentenza attuale ha riconosciuto sostanzialmente il
legittimo esercizio della satira, ed in particolare che
quella pagina rappresenta "un evidente e severo giudizio critico
sul contributo posto in essere dalla societa' alla movimentazione di
mezzi militari".

</p>
<p>

Il risultato pratico: il sito torna on line e passiamo
dal dover pagare qualche decina di migliaia di euro a Trenitalia
per la pubblicazione della sentenza su quotidiani nazionali,
ad essere creditori nei confronti della compagnia ferroviaria
delle spese legali, per circa 5.000 euro.
In buona sostanza si e' ribaltata la situazione.

</p>
<p>

Pur non essendo particolarmente avvezzi all'ottimismo ed ai
facili entusiasmi, siamo comunque soddisfatti del risultato
ottenuto. In particolare vorremmo evidenziare il contributo
determinanete
di tutti coloro che ci hanno appoggiato in questo sforzo.

</p>
<p>

Siamo particolarmente felici che la richiesta di rimozione
dei link ai mirror sorti spontaneamente in rete ed ospitati da
autistici nella pagina http://autistici.org/ai/trenitalia,
sia stata evitata. Sarebbe stato l'ennesimo preoccupante
precedente giudiziario.

</p>
<p>

LA CAUSA NON E' PERO' TERMINATA. Le udienze tenutesi sinora
dovevano giudicare in merito all'urgenza del provvedimento.
La sentenza emessa in realta' va ben oltre questo
singolo aspetto, e si pronuncia decisamente a favore circa la
legittimita' del sito, vanificando di fatto la richiesta di danni
da parte di Trenitalia.
Si tratta pero' dell'opinione di questo particolare collegio
di giudici. Trenitalia potra' in ogni caso intentare, attraverso
un procedimento civile, una nuova causa.


</p>
<p>
In questo caso saremo di nuovo in ballo, forti di una sentenza
a nostro favore, non piu' preventivamente oscurati, ma di nuovo
dovremo confrontarci con gli avvocati della compagnia ferroviaria.


</p>
<p>
Ma al di la' della vicenda giudiziaria siamo particolarmente contenti
di poter ospitare di nuovo una pagina che, nel proprio piccolo,
si e' posta in maniera duramente critica nei confronti della
guerra in Iraq.
Ed in questo momento e' probabilmente la cosa che ci interessa di
piu'.


</p>
<p>
Il collettivo Inventati/Autistici 
<hr>

  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>


            
