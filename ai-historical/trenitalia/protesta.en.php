<html>
<head>
  <title>What you can do...</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>A wagonful of...</h3>
  <small>Trenitalia Vs Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="mirrors.en.php">Mirrors</a></li>
   <li><a href="legaldocs.en.php">Legal Documentation [only ITA]</a></li>
   <li><a href="rassegna.en.php">Web/Press Release</a></li>
   <li><a href="comunicato_zenmai23.php">Zenmai23 statement [only ITA]</a></li>
   <li><a href="link.php">Links about Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.en.php">What can I do?</a></li>
	<ol>
	<li><a href="protesta.en.php#mail">Send an e-mail to Trenitalia</a></li>
	<li><a href="protesta.en.php#banner">Put our banner on your site</a></li>
	<li>Sign the <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.en.php#signature">Use this signature!</a></li>
	<li><a href="protesta.en.php#stampa">Printed material for divulgation</a></li>
	<li>Make a <a href="http://autistici.org/it/donate.html">donation!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page Autistici/Inventati</a><br/>

 </div>


 <div id="main">



<h3>
How-to support the campaign...
</h3>
</p>
<hr>

<p>
<a name="mail"></a>
<h4>
Send e-mail to Trenitalia Headquarter!
</h4>
what about sendin one, two or more e-mails to the regional headquarters spread around Italy? 
Just get the most fancy e-mail from the list above and follow your heart!
</p>
<p>
<a href="http://www.regionale.trenitalia.it/direzioni_regionali.html">
http://www.regionale.trenitalia.it/direzioni_regionali.html
</a>
</p>
<p>
Otehrwise, you may copy the very polite italian message we already wrote for you! At least, change the subject...
</p>
<p>
[cut&paste text]
</p>
<p>
<strong>Subject: Reclamo contro...
</strong>
<br>
All'Attenzione della Direzione Regionale Trenitalia
<br/>
<br/>
<i>

La liberta' non viaggia sui binari di Trenitalia
<br>
Con questa mail intendiamo protestare contro la rimozione
forzata del sito
<br>
http://autistici.org/zenmai23/trenitalia
<br>
e contro l'azione legale intrapresa da Trenitalia s.p.a.
ai danni dell'associazione Investici, che ospitava lo
spazio sul proprio server.
</i>
L'ennesimo cliente insoddisfatto.
<br/>
[end]
</p>

<hr>

<p>
<a name="banner"></a>
<h4>
Banner on your website
</h4>

Do you have a website? Publish one of this banner:
<ul>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/trenitalia.gif">http://autistici.org/ai/trenitalia/materiali/trenitalia.gif</a> (468x60 animated)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai.png">http://autistici.org/ai/trenitalia/materiali/banner_ai.png</a> (468x60)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai_half.png">http://autistici.org/ai/trenitalia/materiali/banner_ai_half.png</a> (234x60)

</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai_square.png">http://autistici.org/ai/trenitalia/materiali/banner_ai_square.png</a> (160x160)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai_button.png">http://autistici.org/ai/trenitalia/materiali/banner_ai_button.png</a> (88x31)
</li>
</ul>
</p>

<hr>

<p>
<a name="guestbook"></a>
<h4>Sign the guestbook</h4>

Write something on our <a href="http://autistici.org/ai/trenitalia/guest">guestbook</a>.

<hr>

<p>
<a name="stampa"></a>
<h4>
Print ( only italian :_! )
</h4>

Print and spread!<br>
<a href="http://autistici.org/ai/trenitalia/trenitalia-01092004.pdf">
http://autistici.org/ai/trenitalia/trenitalia-01092004.pdf
</a>
</p>

<hr>

<p>
<a name="signature"></a>
<h4>
Signature
</h4>
Use this ASCII signature!
</p>
</small>

<pre>
-.          .-----.          .-----.        ----------         .--.
#|   o======|#####|   o======|#####|          |#  #|     .--.  |##|
---.   .------------.   .------------.   |----+####+-----+##+--+##|
o)o ) ( (o)o(o)o(o)o ) ( (o)o(o)o(o)o )  |###aAAb###aAAb###aAAb###|
=http://www.autistici.org/ai/trenitalia=-|--(doob)-(doob)-(doob)--#
 o ))  ( o ))   ( o ))  ( o ))   ( o ))      `uu'   `uu'   `uu'
</pre>

<hr>
<a name="donate"></a>
<h4>
donate:   	  
</h4>
<p>
<small>
If you value radical alternatives to the corporate internet,
please pay for what you use in order to keep this service alive.
<br>
Please see our <a href="http://autistici.org/en/donate.html">
donation page</a> for more information. 
</small>
</p>
<br/>

</body>
</html>
