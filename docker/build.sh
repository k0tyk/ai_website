#!/bin/sh
#
# Install script for git.autistici.org/ai/website
# inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

APACHE_MODULES_ENABLE="
	headers
	rewrite
	negotiation
	proxy
	proxy_http
"

APACHE_MODULES_DISABLE="
	deflate
"

set -e
umask 022

# Setup Apache.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2dismod -f -q ${APACHE_MODULES_DISABLE}

# Make sure that files are readable.
chmod -R a+rX /var/lib/sitesearch
chmod -R a+rX /var/www/autistici.org
